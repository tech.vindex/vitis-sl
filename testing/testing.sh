#!/bin/bash

################################################################################
# TEST MODE: 'auto', 'manual' or 'full'
################################################################################

TEST_MODE='auto'
NET_TESTS=off
if [[ "$1" == "--net" ]]; then
    NET_TESTS=on
fi

################################################################################
# CONSTANTS
################################################################################

#AUTOCATEGORIES=( NetworkBookmarks Fragments Format Формат Formato \
#Type Тип Tipo Extension Расширение Dosiersufikso )
AUTOCATEGORIES=( NetworkBookmarks Fragments Format Type Extension )

################################################################################
# COUNTERS
################################################################################

declare -i good_cases=0
declare -i bad_cases=0


################################################################################
# TEXT DECORATION
################################################################################

BOLD='\e[1m'
UNDERLINED='\e[4m'

LBLUE='\e[1;34m'
LGREEN='\e[92m'
LRED='\e[1;31m'
CYAN='\e[96m'

NORMAL='\e[0m' #font reset

title() {
    echo -e "\n""${UNDERLINED}${BOLD}$@${NORMAL}"
}

procedure_title() {
    echo -e "\n${BOLD}$@${NORMAL}\n"
}

subtitle() {
    echo -e ${BOLD}${LGREEN}">" $@${NORMAL}
}

fail() {
    let "bad_cases=bad_cases+1"
    echo -e "  ${LRED}× fail${NORMAL}"":" "$1"
}

pass() {
    let "good_cases=good_cases+1"
    echo -e "  ${LBLUE}✓ pass${NORMAL}"":" "$1"
}

get_result() {
    if [[ "$2" == "$3" ]]; then
        pass "$1"
    else
        fail "$1"
    fi
}

show_results() {
    echo -e "${BOLD}${CYAN}"
    echo "Good cases: $good_cases"
    echo "Bad cases:  $bad_cases"
    echo -e "$NORMAL"
}

################################################################################
# PATH OF TESTING PROGRAM
################################################################################

vitis=./vitis-sl
if [ ! -e $vitis ] ; then
    echo "vitis not found"
    exit 1
fi


################################################################################
# TEST ENVIRONMENT
################################################################################

OPTCONF="--conf vitis.1.conf"
VTS="/tmp/Vitis"

FILESDIR=/tmp/Files

FSPACE1=/tmp/Filespace1
FSPACE2=/tmp/Filespace2

FILE1=$FILESDIR/file1.txt
FILE2=$FILESDIR/file2.txt
FILE3=$FILESDIR/file3.txt
FILE4=$FILESDIR/file4.txt
FILE5=$FILESDIR/file5.txt

_10bytes="0123456789" #line is 10 bytes in size
_16bytes="0123456789ABCDEF"
_30bytes=$_10bytes$_10bytes$_10bytes
_31bytes=$_10bytes$_10bytes$_10bytes"0"

yeats="William Butler Yeats
He Wishes for the Cloths of Heaven

Had I the heaven's embroidered cloths,
Enwrought with golden and silver light,
The blue and the dim of the dark cloths
Of night and light and the half-light,
I would spread the cloths under your feet:
But I being poor have only my dreams;
I have spread my dreams under your feet;
Tread softly, because you tread on my dreams.
"
YEATS="${FILESDIR}/yeats.txt"

lorem="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
sunt in culpa qui officia deserunt mollit anim id est laborum."
BIGTEXT="${FILESDIR}/bigtext.txt"

ENTRY_CONTENT="[Desktop Entry]
Encoding=UTF-8
Name=Home - D Programming Language
Type=Link
URL=http://dlang.org
Comment=html
Icon=text-html"
ENTRY_BASEDIR="${VTS}/__vitis/link_entries"
ENTRY_HTMLDIR="${ENTRY_BASEDIR}/html"
ENTRY_PATH="${ENTRY_HTMLDIR}/D Programming Language.desktop"
ENTRY_IPATH="http://dlang.org"
BOOKMARKS_DIR="${VTS}/__auto/N/NetworkBookmarks"
ENTRY_SYMLINK="$BOOKMARKS_DIR/D Programming Language.desktop"

FP_CONTENT="\
Type: text
Path: /tmp/Files/yeats.txt
Start: 9,1
Finish: "

# experimantal link to ISO for saving
ISO=debian-hurd-2019-i386-DVD-1.iso
DEBIANCD=https://cdimage.debian.org/cdimage/
DEBIANHURD="${DEBIANCD}/ports/current-hurd-i386/iso-dvd/${ISO}"

FP_NAME="But_I_being_poor.fragpointer"
FP_BASEDIR="${VTS}/__vitis/fragpointers"
FP_PATH="${FP_BASEDIR}/${FP_NAME}"
FPCAT_DIR="${VTS}/__auto/FP/Fragments"
FP_SYMLINK="${FPCAT_DIR}/${FP_NAME}"

create_addit_file_env() {
    mkdir -p $ENTRY_HTMLDIR
    mkdir -p "$BOOKMARKS_DIR"
    echo -n "${ENTRY_CONTENT}" > "${ENTRY_PATH}"
    ln -s "$ENTRY_PATH" "$ENTRY_SYMLINK"

    mkdir -p $FP_BASEDIR
    mkdir -p "$FPCAT_DIR"
    echo -n "${FP_CONTENT}" > "${FP_PATH}"
    ln -s "$FP_PATH" "$FP_SYMLINK"
}

create_file_env() {
    mkdir $FILESDIR
    touch $FILE1 $FILE2 $FILE3 $FILE4 $FILE5
    cd $FILESDIR
    echo -n $_31bytes > march.txt
    echo -n $_30bytes > april.txt
    echo -n $_31bytes > may.txt
    echo -n "$yeats"  > yeats.txt
    echo -n $lorem    > bigtext.txt
    echo -n ""        > spring_file

    mkdir ExpDir
    echo -n ""        > ExpDir/.file1
    echo -n ""        > ExpDir/.file2
    echo -n ""        > ExpDir/visibleFile
    echo -n ""        > visibleFile # repeated name

    touch -m --date="1970-01-01 00:00:00" spring_file
    touch -m --date="2019-03-16 10:07:00" march.txt
    touch -m --date="2019-04-16 00:00:01" april.txt 
    touch -m --date="2019-05-16 03:50:09" may.txt

    touch -a --date="1985-02-02 00:00:00" spring_file
    touch -a --date="2019-06-16 01:00:00" may.txt
    touch -a --date="2019-06-16 02:00:00" april.txt 
    touch -a --date="2019-06-16 03:00:00" march.txt

    cd - > /dev/null
}

destroy_file_env() {
    rm -rf $FILESDIR
    rm -rf ${FSPACE1} ${FSPACE2}
}

COPY_MADE=FALSE
create_category_env() {
    if [[ $COPY_MADE == TRUE ]]; then
        restore_copy_of_category_env
        return 0
    fi
    vitis="$PWD/vitis-sl"
    OPTCONF="--conf $PWD/vitis.1.conf"
    cd $FILESDIR

    $vitis create Poetry Spring Text/Fiction Empty $OPTCONF
    $vitis create HiddenFiles .HiddenCategory $OPTCONF

    $vitis assign Poetry -f yeats.txt $OPTCONF    
    $vitis assign Text/Fiction -f yeats.txt bigtext.txt $OPTCONF 
    $vitis assign Text -f visibleFile march.txt april.txt may.txt $OPTCONF
    $vitis assign Spring -f march.txt april.txt may.txt spring_file $OPTCONF
    $vitis assign HiddenFiles -f ExpDir/.file1 ExpDir/.file2 \
                                 ExpDir/visibleFile $OPTCONF
    $vitis assign .HiddenCategory -f ExpDir/.file1 ExpDir/.file2 \
                                     ExpDir/visibleFile $OPTCONF
    $vitis assign RepeatedFiles -f visibleFile ExpDir/visibleFile --yes $OPTCONF
    $vitis assign Text/Fiction -a Literature $OPTCONF
    cd - > /dev/null
    vitis=./vitis-sl
    OPTCONF="--conf vitis.1.conf"
    make_copy_of_category_env
    COPY_MADE=TRUE
}

make_copy_of_category_env() {
    cp -R ${VTS} ${VTS}.1
}

restore_copy_of_category_env() {
    rm -rf ${VTS}
    cp -R ${VTS}.1 ${VTS}
}

delete_copy_of_category_env() {
    rm -rf ${VTS}.1
}

################################################################################
# BASE TESTS
################################################################################

VTSHOME=$HOME/Vitis.test
FILES=$HOME/Files.test

CONF_DIR="$HOME/.config/vitis-sl"
CONF_PATH="${CONF_DIR}/vitis-sl.conf"
conf_dir_existed=FALSE
if [[ -e "$CONF_DIR" ]]; then
    conf_dir_existed=TRUE
fi

evacuate_vitis_conf() {
    if [[ -e "$CONF_PATH" ]] ; then
        mv "$CONF_PATH" "${CONF_PATH}.original"
    fi
}
return_vitis_conf() {
    if [[ -e "${CONF_PATH}.original" ]] ; then
        mv "${CONF_PATH}.original" "$CONF_PATH"
    fi
}
set_base_env() {
    evacuate_vitis_conf
    mkdir -p $VTSHOME $FILES
    mkdir -p "$CONF_DIR"
    cp vitis.1.conf "${CONF_PATH}"
}
destroy_base_env() {
    rm "${CONF_PATH}"
    return_vitis_conf
    rm -rf $VTSHOME $FILES
    if [[ $conf_dir_existed == FALSE ]]; then
        rm -rf "$CONF_DIR"
    fi
}
version() {
    CMD="--version"
    output=`$vitis $CMD`
    expected=`cat ../source/version`
    get_result "version" "$output" "$expected"
} # REQ 1.3


start_of_use() {
    evacuate_vitis_conf

    statement="'vitis --version' doesn't create vitis.conf"
    $vitis --version > /dev/null
    if [[ ! -e "$CONF_PATH" ]]; then
        pass "$statement"
    else
        fail "$statement"
        rm "$CONF_PATH"
    fi

    statement="Use of '--conf' doesn't create default vitis.conf"
    $vitis show D --conf=vitis.1.conf 2> /dev/null
    if [[ ! -e "$CONF_PATH" ]]; then
        pass "$statement"
    else
        fail "$statement"
        rm "$CONF_PATH"
    fi

    statement="Normal creates default vitis.conf"
    if [[ -e $HOME/Vitis ]]; then
        mv $HOME/Vitis $HOME/Vitis.temp.orig
    fi
    [[ -d $HOME/Vitis ]] && home_vitis_exists=TRUE || home_vitis_exists=FALSE
    $vitis show D 2> /dev/null
    if [[ -e "$CONF_PATH" ]]; then
        pass "$statement"
        rm "$CONF_PATH"
    else
        fail "$statement"
    fi
    if [[ $home_vitis_exists == FALSE ]]; then
        rm -rf $HOME/Vitis
    fi

    return_vitis_conf
    rm -rf /tmp/Vitis/
    if [[ -e $HOME/Vitis.temp.orig && ! -e $HOME/Vitis ]]; then
        mv $HOME/Vitis.temp.orig $HOME/Vitis
    fi
} # REQ 3.1


bad_vitis_path() {
    $vitis service set path /dev/null/Vitis $OPTCONF
    CMD="$vitis show D"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) /dev/null/Vitis/: this path is not available."
    if [[ "$output" == "$expected" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo -n "${output}"
    fi
    $vitis service set path /tmp/Vitis $OPTCONF # back to default
}

procedure_base_tests() {
    procedure_title "Base tests"
    version
    start_of_use
    bad_vitis_path
}


################################################################################
# vitis create
################################################################################

case_1_1_1() {
    subtitle "Case 1: Creation of one category with '-c'"
    CMD="$vitis create -c Probe"
    $CMD $OPTCONF
    [[ -d ${VTS}/Probe ]] && pass "${CMD}" || fail "${CMD}"
    rm -rf "${VTS}"
} # REQS 6.1.0, 3.2, 3.4.1

case_1_1_2() {
    subtitle "Case 2: Creation of one category without '-c'"
    CMD="$vitis create Probe"
    $CMD $OPTCONF
    [[ -d ${VTS}/Probe ]] && pass "${CMD}" || fail "${CMD}"
    rm -rf "${VTS}"
} # REQS 6.1.0, 3.2, 3.4.1



case_1_1_3() {
    subtitle "Case 3: Create two categories with '-c'"
    CMD="$vitis create -c Phobos Deimos"
    $CMD $OPTCONF
    [[ -d ${VTS}/Phobos && -d ${VTS}/Deimos ]] && pass "${CMD}" || fail "${CMD}"
    rm -rf "${VTS}"
}

case_1_1_4() {
    subtitle "Case 4: Create two categories without '-c'"
    CMD="$vitis create Phobos Deimos"
    $CMD $OPTCONF
    [[ -d ${VTS}/Phobos && -d ${VTS}/Deimos ]] && pass "${CMD}" || fail "${CMD}"
    rm -rf "${VTS}"
}

case_1_1_5() {
    subtitle "Case 5: Create 10 categories"
    arr=( "Io" "Europa" "Ganymede" "Callisto" "Amalthea" "Himalia" "Elara" \
        "Pasiphae" "Sinope" "Lysithea" )
    CMD="$vitis create ${arr[@]}"
    $CMD $OPTCONF
    status=TRUE
    for moon in ${arr[@]}; do
        if [[ ! -d ${VTS}/${moon} ]]; then
            ls ${VTS}
            status=FALSE
            break
        fi
    done
    [[ ${status} == TRUE ]] && pass "${CMD}" || fail "${CMD}"
    rm -rf "${VTS}"
}

case_1_1_6() {
    subtitle "Case 6: Nested to existing"
    category=Solar_System
    subcategory=Eris
    $vitis create "${category}" $OPTCONF
    CMD="$vitis create ${category}/${subcategory}"
    $CMD $OPTCONF
    if [[ -d "${VTS}/${category}/${subcategory}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_1_1_7() {
    subtitle "Case 7: Nested to nonexisting"
    category=Solar_System
    subcategory=Sedna
    CMD="$vitis create ${category}/${subcategory}"
    $CMD $OPTCONF
    if [[ -d "${VTS}/${category}/${subcategory}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

procedure_1_1() {
    procedure_title "PROCEDURE 1.1"
    case_1_1_1
    case_1_1_2
    case_1_1_3
    case_1_1_4
    case_1_1_5
    case_1_1_6
    case_1_1_7
}

case_1_2_1() {
    subtitle "Case 1: No categories"
    CMD="$vitis create -c"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'create'.
See: vitis-sl --help"
    get_result "${CMD}" "${output}" "${expected}"
    rm -rf "${VTS}"
}

case_1_2_2() {
    subtitle "Case 2: No categories (without '-c')"
    CMD="$vitis create"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'create'.
See: vitis-sl --help"
    get_result "${CMD}" "${output}" "${expected}"
    rm -rf "${VTS}"
}

case_1_2_3() {
    subtitle "Case 3: Creating of existent category"
    category="The_Land_of_Crimson_Clouds"
    CMD="$vitis create $category"
    $CMD $OPTCONF
    output=$($CMD $OPTCONF 2>&1)
    expected="$category: this category already exists. Skipped."
    get_result "${CMD}" "${output}" "${expected}"
    rm -rf "${VTS}"
}

case_1_2_4() {
    subtitle "Case 4: Creating of 2 existent categories and 5 nonexistent"
    arr=( "Mimas" "Enceladus" "Tethys" "Dione" "Rhea" "Titan" "Iapetus" )
    $vitis create Tethys Titan $OPTCONF
    CMD="$vitis create ${arr[@]}"
    output=$($CMD $OPTCONF 2>&1)
    expected="Tethys: this category already exists. Skipped.
Titan: this category already exists. Skipped."
    status=TRUE
    for moon in ${arr[@]}; do
        if [[ ! -d ${VTS}/${moon} ]]; then
            ls ${VTS}
            status=FALSE
            break
        fi
    done
    if [[ ${status} == TRUE && "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_1_2_5() {
    subtitle "Case 5: wrong flag"
    CMD="$vitis create -c X --nibiru"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Unknown option: --nibiru
See: vitis-sl --help"
    if [[ "${output}" == "${expected}" && ! -d ${VTS}/X ]]; then
        pass "$CMD"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}


procedure_1_2() {
    procedure_title "PROCEDURE 1.2"
    case_1_2_1
    case_1_2_2
    case_1_2_3
    case_1_2_4
    case_1_2_5
}

case_1_3_1() {
    subtitle "Case 1: trying of creating autocategories"
    CMD="$vitis create ${AUTOCATEGORIES[@]}"
    output=$($CMD $OPTCONF 2>&1)
    expected=""
    for cat in ${AUTOCATEGORIES[@]}; do
        expected+="$cat: creation of this category is not available. Skipped."
        expected+='
'
    done
    expected=${expected:0:${#expected}-1} # remove last symbol "\n"
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo ${output}
    fi
    rm -rf "${VTS}"
}

case_1_3_2() {
    subtitle "Case 2: subcategories of autocategories"
    declare -a subcategories
    declare -i index=0
    for cat in ${AUTOCATEGORIES[@]}; do
        subcategories[index]=${cat}/some_subcategory
        index=$index+1
    done
    CMD="$vitis create ${subcategories[@]}"
    output=$($CMD $OPTCONF 2>&1)
    expected=""
    for cat in ${subcategories[@]}; do
        expected+="$cat: creation of this category is not available. Skipped."
        expected+='
'
    done
    expected=${expected:0:${#expected}-1} # remove last symbol "\n"
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo ${output}
    fi
    rm -rf "${VTS}"
}

procedure_1_3() {
    procedure_title "PROCEDURE 1.3"
    case_1_3_1
    case_1_3_2
}

tests_create() {
    title "TESTING 'VITIS CREATE'"
    procedure_1_1
    procedure_1_2
    procedure_1_3
}

################################################################################
# vitis assign
################################################################################

case_2_1_1() {
    subtitle "Case 1: Simple alias (without '-c')"
    $vitis create Moon $OPTCONF
    CMD="$vitis assign Moon -a Luna"
    $CMD $OPTCONF
    if [[ -L ${VTS}/Luna && `readlink ${VTS}/Luna` == ${VTS}/Moon ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
} # REQ 7.2.1

case_2_1_2() {
    subtitle "Case 2: Simple alias (with '-c')"
    $vitis create Moon $OPTCONF
    CMD="$vitis assign -c Moon -a Luna"
    $CMD $OPTCONF
    if [[ -L ${VTS}/Luna && `readlink ${VTS}/Luna` == ${VTS}/Moon ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_3() {
    subtitle "Case 3: Three aliases (with '-c')"
    $vitis create Moon $OPTCONF
    CMD="$vitis assign -c Moon -a Luna Hologram Nazi_Base" # ;-)
    $CMD $OPTCONF
    if [[ -L ${VTS}/Luna && -L ${VTS}/Hologram && -L ${VTS}/Nazi_Base
      && `readlink ${VTS}/Luna` == ${VTS}/Moon
      && `readlink ${VTS}/Hologram` == ${VTS}/Moon
      && `readlink ${VTS}/Nazi_Base` == ${VTS}/Moon ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_4() {
    subtitle "Case 4: Three aliases (without '-c')"
    $vitis create Moon $OPTCONF
    CMD="$vitis assign Moon -a Luna Hologram Nazi_Base" # ;-)
    $CMD $OPTCONF
    if [[ -L ${VTS}/Luna && -L ${VTS}/Hologram && -L ${VTS}/Nazi_Base
      && `readlink ${VTS}/Luna` == ${VTS}/Moon
      && `readlink ${VTS}/Hologram` == ${VTS}/Moon
      && `readlink ${VTS}/Nazi_Base` == ${VTS}/Moon ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_5() {
    subtitle "Case 5: Wrong using: a lot of categories (one alias)"
    $vitis create Venus Mercury $OPTCONF
    CMD="$vitis assign Venus Mercury -a Planets"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'assign'.
See: vitis-sl --help"
    if [[ ! -e ${VTS}/Planets && "$output" == "$expected" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_6() {
    subtitle "Case 6: Wrong using: a lot of categories (two aliases)"
    $vitis create Venus Mercury $OPTCONF
    CMD="$vitis assign Venus Mercury -a Planets Space_Objects"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'assign'.
See: vitis-sl --help"
    if [[ ! -e ${VTS}/Planets && ! -e ${VTS}/Space_Objects
      && "$output" == "$expected" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_7() {
    subtitle "Case 7: Alias for nonexistent category (with --yes)"
    CMD="$vitis assign Milky_Way -a Our_Galaxy --yes"
    $CMD $OPTCONF
    alias_path="${VTS}/Our_Galaxy"
    if [[ -d ${VTS}/Milky_Way && -L "$alias_path"
      && `readlink "$alias_path"` == ${VTS}/Milky_Way ]]
    then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_8() {
    subtitle "Case 8: Alias for nonexistent category (with --no)"
    CMD="$vitis assign Milky_Way -a Our_Galaxy --no"
    output=$($CMD $OPTCONF 2>&1)
    alias_path="${VTS}/Our_Galaxy"
    if [[ ! -d ${VTS}/Milky_Way && ! -L "$alias_path" ]]
    then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_9() {
    subtitle "Case 9: Existent alias"
    CMD="$vitis assign Moon -a Luna --no"
    $vitis create Moon Luna $OPTCONF
    output=$($CMD $OPTCONF 2>&1)
    expected="Luna: this category already exists.
The request is skipped."
    if [[ "${output}" == "${expected}" && ! -L ${VTS}/Luna ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_10() {
    subtitle "Case 10: Alias for autocategories"
    CMD="$vitis assign NetworkBookmarks -a Entries"
    category_path=${VTS}/__auto/N/NetworkBookmarks
    alias_path=${VTS}/Entries
    $CMD $OPTCONF
    if [[ -L "${alias_path}"
      && `readlink "${alias_path}"` == "${category_path}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_11() {
    subtitle "Case 11: Alias in autocategories"
    CMD="$vitis assign X -a NetworkBookmarks/X --yes"
    output=$($CMD $OPTCONF 2>&1)
    msg=": creation of this alias is not available. Skipped."
    expected="NetworkBookmarks/X${msg}"
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_1_12() {
    subtitle "Case 12: Combining"
    create_file_env
    $vitis create Synthwave Retrowave $OPTCONF
    $vitis assign Synthwave -s 1 $OPTCONF
    $vitis assign Synthwave -f $FILE1 $OPTCONF
    $vitis assign Synthwave/1 -f $FILE2 $OPTCONF
    CMD="$vitis assign Retrowave -a Synthwave --yes"
    output=$($CMD $OPTCONF 2>&1)
    if [[ -L "${VTS}/Synthwave" && -L "${VTS}/Synthwave/file1.txt"
      && `readlink "${VTS}/Synthwave"` == "${VTS}/Retrowave"
      && `readlink "${VTS}/Retrowave/file1.txt"` == $FILE1
      && -L "${VTS}/Retrowave/1/file2.txt"
      && `readlink "${VTS}/Retrowave/1/file2.txt"` == $FILE2
      && `readlink "${VTS}/1"` == "${VTS}/Retrowave/1" ]]
    then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

procedure_2_1() {
    procedure_title "PROCEDURE 2.1 'Aliases'"
    case_2_1_1
    case_2_1_2
    case_2_1_3
    case_2_1_4
    case_2_1_5
    case_2_1_6
    case_2_1_7
    case_2_1_8
    case_2_1_9
    case_2_1_10
    case_2_1_11
    case_2_1_12
}



case_2_2_1() {
    subtitle "Case 1: One category, one file (with -c)"
    create_file_env
    CMD="$vitis assign -c MainCategory -f $FILE1 --yes"
    $CMD $OPTCONF
    local link="${VTS}/MainCategory/file1.txt"
    if [[ -L "${link}" && `readlink "${link}"` == "${FILE1}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_2_2() {
    subtitle "Case 2: One category, one file (without -c)"
    create_file_env
    CMD="$vitis assign MainCategory -f $FILE1 --yes"
    $CMD $OPTCONF
    local link="${VTS}/MainCategory/file1.txt"
    if [[ -L "${link}" && `readlink "${link}"` == "${FILE1}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_2_3() {
    subtitle "Case 3: Two category, one file (with -c)"
    create_file_env
    CMD="$vitis assign -c Cat1 Cat2 -f $FILE1 --yes"
    $CMD $OPTCONF
    local link1="${VTS}/Cat1/file1.txt"
    local link2="${VTS}/Cat2/file1.txt"
    if [[ -L "${link1}" && -L "${link2}"
      && `readlink "${link1}"` == "${FILE1}" 
      && `readlink "${link2}"` == "${FILE1}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_2_4() {
    subtitle "Case 4: Two category, one file (without -c)"
    create_file_env
    CMD="$vitis assign Cat1 Cat2 -f $FILE1 --yes"
    $CMD $OPTCONF
    local link1="${VTS}/Cat1/file1.txt"
    local link2="${VTS}/Cat2/file1.txt"
    if [[ -L "${link1}" && -L "${link2}"
      && `readlink "${link1}"` == "${FILE1}" 
      && `readlink "${link2}"` == "${FILE1}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_2_5() {
    subtitle "Case 5: One category, two files"
    create_file_env
    CMD="$vitis assign Cat1 -f $FILE1 $FILE2 --yes"
    $CMD $OPTCONF
    local link1="${VTS}/Cat1/file1.txt"
    local link2="${VTS}/Cat1/file2.txt"
    if [[ -L "${link1}" && -L "${link2}"
      && `readlink "${link1}"` == "${FILE1}" 
      && `readlink "${link2}"` == "${FILE2}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_2_6() {
    subtitle "Case 6: Two category, two files"
    create_file_env
    CMD="$vitis assign Cat1 Cat2 -f $FILE1 $FILE2 --yes"
    $CMD $OPTCONF
    local link1="${VTS}/Cat1/file1.txt"
    local link2="${VTS}/Cat1/file2.txt"
    local link3="${VTS}/Cat2/file1.txt"
    local link4="${VTS}/Cat2/file2.txt"
    if [[ -L "${link1}" && -L "${link2}" && -L "${link3}" && -L "${link4}"
      && `readlink "${link1}"` == "${FILE1}" 
      && `readlink "${link2}"` == "${FILE2}"
      && `readlink "${link3}"` == "${FILE1}" 
      && `readlink "${link4}"` == "${FILE2}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_2_7() {
    subtitle "Case 7: no user categories, only autocategories"
    create_file_env
    CMD="$vitis assign -f $FILE1"
    $CMD $OPTCONF
    local link1="${VTS}/__auto/F/Format/TXT/file1.txt"
    local link2="${VTS}/__auto/T/Type/Text/file1.txt"
    local link3="${VTS}/__auto/E/Extension/txt/file1.txt"
    if [[ -L "${link1}" && -L "${link2}" && -L "${link3}" 
      && `readlink "${link1}"` == "${FILE1}" 
      && `readlink "${link2}"` == "${FILE1}"
      && `readlink "${link3}"` == "${FILE1}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

OPTCONF_NOAUTO="--conf vitis.noauto.conf"

case_2_2_8() {
    subtitle "Case 8: no categories (wrong using)"
    create_file_env
    CMD="$vitis assign -f $FILE1"
    output=$($CMD $OPTCONF_NOAUTO 2>&1)
    expected="(Error) Wrong using of command 'assign'.
See: vitis-sl --help"
    get_result "${CMD}" "${output}" "${expected}"
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_2_9() {
    subtitle "Case 9: category for nonexistent file"
    local FILE=nonexistent_file
    CMD="$vitis assign Category -f $FILE --yes"
    output=$($CMD $OPTCONF_NOAUTO 2>&1)
    msg=": file or directory not found. The request is skipped."
    expected="nonexistent_file${msg}"
    if [[ ! -e "${VTS}/Category" && "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_2_10() {
    subtitle "Case 10: demonstration of removing unnecessary links"
    create_file_env
    $vitis create Category/Subcategory $OPTCONF
    $vitis assign Category -f $FILE1 $OPTCONF
    CMD="$vitis assign Category/Subcategory -f $FILE1"
    output=$($CMD $OPTCONF 2>&1)
    if [[ ! -e ${VTS}/Category/file1.txt
      && `readlink ${VTS}/Category/Subcategory/file1.txt` == $FILE1 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

procedure_2_2() {
    procedure_title "PROCEDURE 2.2 'Assigning categories to files'"
    case_2_2_1
    case_2_2_2
    case_2_2_3
    case_2_2_4
    case_2_2_5
    case_2_2_6
    case_2_2_7
    case_2_2_8
    case_2_2_9
    case_2_2_10
}

OPTCONF_FSPACES="--conf vitis.filespaces.conf"

case_2_3_1() {
    subtitle "Case 1: simple save"
    create_file_env
    CMD="$vitis assign Category -f ${FILE1} --save --yes"
    $CMD $OPTCONF_FSPACES
    local newpath="${FSPACE1}/file1.txt"
    local link=${VTS}/Category/file1.txt
    if [[ -L "${link}" && `readlink "${link}"` == "${newpath}"
      && ! -L "${newpath}" && -f "${newpath}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_3_2() {
    subtitle "Case 2: save without file spaces (wrong using)"
    create_file_env
    CMD="$vitis assign Category -f ${FILE1} --save --yes"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) File spaces are not configured."
    if [[ "${output}" == "${expected}" && ! -e "${VTS}/Category" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_3_3() {
    subtitle "Case 3: save to selected file space"
    create_file_env
    CMD="$vitis assign Category -f ${FILE1} --to ${FSPACE2} --yes"
    $CMD $OPTCONF_FSPACES
    local newpath="${FSPACE2}/file1.txt"
    local link=${VTS}/Category/file1.txt
    if [[ -L "${link}" && `readlink "${link}"` == "${newpath}"
      && ! -L "${newpath}" && -f "${newpath}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_3_4() {
    subtitle "Case 4: save to wrong file space (wrong using)"
    create_file_env
    CMD="$vitis assign Category -f ${FILE1} --to /tmp/Filespace3 --yes"
    output=$($CMD $OPTCONF_FSPACES 2>&1)
    expected="(Error) /tmp/Filespace3: There is no file space."
    local newpath="/tmp/Filespace3/file1.txt"
    local link=${VTS}/Category/file1.txt
    if [[ ! -L "${link}" && ! -e "${newpath}"
      && "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_3_5() {
    subtitle "Case 5: save five files"
    create_file_env
    declare -a files=( ${FILE1} ${FILE2} ${FILE3} ${FILE4} ${FILE5} )
    CMD="$vitis assign Category -f ${files[@]} --save --yes"
    $CMD $OPTCONF_FSPACES
    status=TRUE
    for i in 1 2 3 4 5 ; do
        local newpath="${FSPACE1}/file${i}.txt"
        local link=${VTS}/Category/file${i}.txt
        if [[ ! -e "${newpath}"
          || ! -L "${link}" || `readlink "${link}"` != "${newpath}" ]]; then
            status=FALSE
            break
        fi
    done
    if [[ $status == TRUE ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_3_6() {
    subtitle "Case 6: save five files, but two are nonexistent"
    create_file_env && rm ${FILE2} && rm ${FILE4}
    declare -a files=( ${FILE1} ${FILE2} ${FILE3} ${FILE4} ${FILE5} )
    CMD="$vitis assign Category -f ${files[@]} --save --yes"
    output=$($CMD $OPTCONF_FSPACES 2>&1)
    expected="\
/tmp/Files/file2.txt: file or directory not found. The request is skipped.
/tmp/Files/file4.txt: file or directory not found. The request is skipped."
    status=TRUE
    for i in 1 2 3 4 5 ; do
        local newpath="${FSPACE1}/file${i}.txt"
        local link=${VTS}/Category/file${i}.txt
        if [[ ! -e "${newpath}"
          || ! -L "${link}" || `readlink "${link}"` != "${newpath}" ]]; then
            if [[ $i == 2 || $i == 4 ]]; then continue; fi
            status=FALSE
            break
        fi
    done
    if [[ $status == TRUE && "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

OPTCONF_AUTOSAVE="--conf vitis.autosave.conf"

case_2_3_7() {
    subtitle "Case 7: case five files with autosave to file space"
    create_file_env
    declare -a files=( ${FILE1} ${FILE2} ${FILE3} ${FILE4} ${FILE5} )
    CMD="$vitis assign Category -f ${files[@]} --yes"
    $CMD $OPTCONF_AUTOSAVE
    status=TRUE
    for i in 1 2 3 4 5 ; do
        local newpath="${FSPACE1}/file${i}.txt"
        local link=${VTS}/Category/file${i}.txt
        if [[ ! -e "${newpath}"
          || ! -L "${link}" || `readlink "${link}"` != "${newpath}" ]]; then
            status=FALSE
            break
        fi
    done
    if [[ $status == TRUE ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_3_8() {
    subtitle "Case 8: repeated copying in file space"
    create_file_env
    $vitis create Category $OPTCONF_AUTOSAVE
    CMD="$vitis assign Category -f ${FILE1}"
    $CMD $OPTCONF_AUTOSAVE
    $CMD $OPTCONF_AUTOSAVE --yes
    $CMD $OPTCONF_AUTOSAVE --yes
    local link1="${VTS}/Category/file1.txt"
    local link2="${VTS}/Category/__repeated_names/2/file1.txt"
    local link3="${VTS}/Category/__repeated_names/3/file1.txt"
    local path1="${FSPACE1}/file1.txt"
    local path2="${FSPACE1}/__repeated_names/2/file1.txt"
    local path3="${FSPACE1}/__repeated_names/3/file1.txt"
    if [[ -L ${link1} && `readlink "${link1}"` == "${path1}"
       && -L ${link2} && `readlink "${link2}"` == "${path2}"
       && -L ${link3} && `readlink "${link3}"` == "${path3}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

procedure_2_3() {
    procedure_title "PROCEDURE 2.3 'Copying to file space'"
    case_2_3_1
    case_2_3_2
    case_2_3_3
    case_2_3_4
    case_2_3_5
    case_2_3_6
    case_2_3_7
    case_2_3_8
}

case_2_4_1() {
    subtitle "Case 1: simple text fragment (full file in fragment)"
    create_file_env
    $vitis create Poetry $OPTCONF
    CMD="$vitis assign Poetry -f ${YEATS} --fragname Verse"
    $CMD $OPTCONF
    local expected_fragpointer="\
Type: text
Path: /tmp/Files/yeats.txt
Start: 
Finish: "
    local link=${VTS}/Poetry/Verse.fragpointer
    local autolink=${VTS}/__auto/FP/Fragments/Verse.fragpointer
    local path=${VTS}/__vitis/fragpointers/Verse.fragpointer
    if [[ -L "${link}" && `readlink ${link}` == "${path}"
      && -L "${autolink}" && `readlink ${autolink}` == "${path}"
      && ! -L "${path}" && -f "${path}"
      && `cat ${path}` == "${expected_fragpointer}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_2() {
    subtitle "Case 2: simple text fragment (with --start)"
    create_file_env
    $vitis create Poetry $OPTCONF
    FRAGOPTS="--start 9,1 --fragname But_I_being_poor"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS}"
    $CMD $OPTCONF
    local expected_fragpointer="\
Type: text
Path: /tmp/Files/yeats.txt
Start: 9,1
Finish: "
    local link=${VTS}/Poetry/But_I_being_poor.fragpointer
    local autolink=${VTS}/__auto/FP/Fragments/But_I_being_poor.fragpointer
    local path=${VTS}/__vitis/fragpointers/But_I_being_poor.fragpointer

    if [[ -L "${link}" && `readlink ${link}` == "${path}"
      && -L "${autolink}" && `readlink ${autolink}` == "${path}"
      && ! -L "${path}" && -f "${path}"
      && `cat ${path}` == "${expected_fragpointer}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_3() {
    subtitle "Case 3: simple text fragment (with --finish)"
    create_file_env
    $vitis create Poetry $OPTCONF
    FRAGOPTS="--finish 1,20 --fragname Author"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS}"
    $CMD $OPTCONF
    local expected_fragpointer="\
Type: text
Path: /tmp/Files/yeats.txt
Start: 
Finish: 1,20"
    local link=${VTS}/Poetry/Author.fragpointer
    local autolink=${VTS}/__auto/FP/Fragments/Author.fragpointer
    local path=${VTS}/__vitis/fragpointers/Author.fragpointer

    if [[ -L "${link}" && `readlink ${link}` == "${path}"
      && -L "${autolink}" && `readlink ${autolink}` == "${path}"
      && ! -L "${path}" && -f "${path}"
      && `cat ${path}` == "${expected_fragpointer}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_4() {
    subtitle "Case 4: simple text fragment (with --start and --finish)"
    create_file_env
    $vitis create Poetry $OPTCONF
    FRAGOPTS="--start 9,1 --finish 9,36 --fragname One_line"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS}"
    $CMD $OPTCONF
    local expected_fragpointer="\
Type: text
Path: /tmp/Files/yeats.txt
Start: 9,1
Finish: 9,36"
    local link=${VTS}/Poetry/One_line.fragpointer
    local autolink=${VTS}/__auto/FP/Fragments/One_line.fragpointer
    local path=${VTS}/__vitis/fragpointers/One_line.fragpointer

    if [[ -L "${link}" && `readlink ${link}` == "${path}"
      && -L "${autolink}" && `readlink ${autolink}` == "${path}"
      && ! -L "${path}" && -f "${path}"
      && `cat ${path}` == "${expected_fragpointer}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_5() {
    subtitle "Case 5: without --fragpointer (wrong using)"
    create_file_env
    $vitis create Poetry $OPTCONF
    FRAGOPTS="--start 9,1 --finish 9,36"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS}"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'assign'.
See: vitis-sl --help"
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_6() {
    subtitle "Case 6: wrong value for --start"
    create_file_env
    $vitis create Poetry $OPTCONF
    FRAGOPTS="--start -9,1 --fragname But_I_being_poor"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS}"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Invalid format of the fragment pointer.
See: vitis-sl --help"
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_7() {
    subtitle "Case 7: simple text fragment with --save"
    create_file_env
    $vitis create Poetry $OPTCONF_FSPACES
    FRAGOPTS="--start 9,1 --fragname But_I_being_poor"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS} --save"
    $CMD $OPTCONF_FSPACES
    local expected_fragpointer="\
Type: text
Path: /tmp/Filespace1/yeats.txt
Start: 9,1
Finish: "
    local link=${VTS}/Poetry/But_I_being_poor.fragpointer
    local autolink=${VTS}/__auto/FP/Fragments/But_I_being_poor.fragpointer
    local path=${VTS}/__vitis/fragpointers/But_I_being_poor.fragpointer

    if [[ -L "${link}" && `readlink ${link}` == "${path}"
      && -L "${autolink}" && `readlink ${autolink}` == "${path}"
      && ! -L "${path}" && -f "${path}"
      && `cat ${path}` == "${expected_fragpointer}"
      && -f ${FSPACE1}/yeats.txt && ! -e ${VTS}/Poetry/yeats.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_8() {
    subtitle "Case 8: simple text fragment with autosave"
    create_file_env
    $vitis create Poetry $OPTCONF_AUTOSAVE
    FRAGOPTS="--start 9,1 --fragname But_I_being_poor"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS}"
    $CMD $OPTCONF_AUTOSAVE
    local expected_fragpointer="\
Type: text
Path: /tmp/Filespace1/yeats.txt
Start: 9,1
Finish: "
    local link=${VTS}/Poetry/But_I_being_poor.fragpointer
    local autolink=${VTS}/__auto/FP/Fragments/But_I_being_poor.fragpointer
    local path=${VTS}/__vitis/fragpointers/But_I_being_poor.fragpointer

    if [[ -L "${link}" && `readlink ${link}` == "${path}"
      && -L "${autolink}" && `readlink ${autolink}` == "${path}"
      && ! -L "${path}" && -f "${path}"
      && `cat ${path}` == "${expected_fragpointer}"
      && -f ${FSPACE1}/yeats.txt && ! -e ${VTS}/Poetry/yeats.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_4_9() {
    subtitle "Case 9: nonexistent file (wrong using)"
    $vitis create Poetry $OPTCONF
    FRAGOPTS="--start -9,1 --fragname But_I_being_poor"
    CMD="$vitis assign Poetry -f ${YEATS} ${FRAGOPTS}"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) ${YEATS}: file or directory not found."
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        echo "$output"
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_4_10() {
    subtitle "Case 10: a lot of files (wrong using)"
    create_file_env
    $vitis create Category $OPTCONF
    FRAGOPTS="--start -9,1 --fragname Some_fragment"
    CMD="$vitis assign Category -f ${YEATS} ${BIGTEXT} ${FRAGOPTS}"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'assign'.
See: vitis-sl --help"
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

procedure_2_4() {
    procedure_title "PROCEDURE 2.4 'Creating pointers to file fragments'"
    case_2_4_1
    case_2_4_2
    case_2_4_3
    case_2_4_4
    case_2_4_5
    case_2_4_6
    case_2_4_7
    case_2_4_8
    case_2_4_9
    case_2_4_10
}

case_2_5_1() {
    subtitle "Case 1: simple assigning from Vitis (one file, one category)"
    create_file_env
    $vitis assign -c Category1 -f $FILE1 --yes $OPTCONF
    CMD="$vitis assign Category2 -v Category1/file1.txt --yes"
    $CMD $OPTCONF
    if [[ -L ${VTS}/Category2/file1.txt
      && `readlink ${VTS}/Category2/file1.txt` == $FILE1 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_5_2() {
    subtitle "Case 2: simple assigning from Vitis (one file, two categories)"
    create_file_env
    $vitis assign -c Category1 -f $FILE1 --yes $OPTCONF
    CMD="$vitis assign Category2 Category3 -v Category1/file1.txt --yes"
    $CMD $OPTCONF
    if [[ -L ${VTS}/Category2/file1.txt && -L ${VTS}/Category3/file1.txt
      && `readlink ${VTS}/Category2/file1.txt` == $FILE1
      && `readlink ${VTS}/Category3/file1.txt` == $FILE1 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_5_3() {
    subtitle "Case 3: simple assigning from Vitis (two files, two categories)"
    create_file_env
    $vitis assign -c C1 -f $FILE1 $FILE2 --yes $OPTCONF
    CMD="$vitis assign C2 C3 -v C1/file1.txt C1/file2.txt --yes"
    $CMD $OPTCONF
    if [[ -L ${VTS}/C2/file1.txt && -L ${VTS}/C3/file1.txt
      && -L ${VTS}/C2/file2.txt && -L ${VTS}/C3/file2.txt
      && `readlink ${VTS}/C2/file1.txt` == $FILE1
      && `readlink ${VTS}/C3/file1.txt` == $FILE1
      && `readlink ${VTS}/C2/file2.txt` == $FILE2
      && `readlink ${VTS}/C3/file2.txt` == $FILE2 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_5_4() {
    subtitle "Case 4: simple assigning from Vitis (two files, one category)"
    create_file_env
    $vitis assign -c C1 -f $FILE1 $FILE2 --yes $OPTCONF
    CMD="$vitis assign C2 -v C1/file1.txt C1/file2.txt --yes"
    $CMD $OPTCONF
    if [[ -L ${VTS}/C2/file1.txt && -L ${VTS}/C2/file2.txt
      && `readlink ${VTS}/C2/file1.txt` == $FILE1
      && `readlink ${VTS}/C2/file2.txt` == $FILE2 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_5_5() {
    subtitle "Case 5: repeated name and --number"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign -c Category -f /tmp/A/file1.txt --yes $OPTCONF
    $vitis create -c Category2 $OPTCONF
    # by alphabet, /tmp/A/file1.txt is 1, /tmp/Files/file1.txt is 2
    CMD="$vitis assign -c Category2 -v Category/file1.txt --number 1"
    $CMD $OPTCONF
    if [[ -L ${VTS}/Category2/file1.txt
      && `readlink ${VTS}/Category2/file1.txt` == /tmp/A/file1.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}" /tmp/A/
    destroy_file_env
}

case_2_5_6() {
    subtitle "Case 6: repeated name and no --number (wrong using)"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign -c Category -f /tmp/A/file1.txt --yes $OPTCONF
    $vitis create -c Category2 $OPTCONF
    CMD="$vitis assign -c Category2 -v Category/file1.txt"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Specify the number of the required file \
using the '--number' flag."
    if [[ "$expected" == "$output" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo "$output"
    fi
    rm -rf "${VTS}" /tmp/A/
    destroy_file_env
}

case_2_5_7() {
    subtitle "Case 7: no repeated names and --number (wrong using)"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    $vitis create -c Category2 $OPTCONF
    CMD="$vitis assign -c Category2 -v Category/file1.txt --number 2"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) File with this number not found."
    if [[ "$output" == "$expected" && ! -e ${VTS}/Category2/file1.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_5_8() {
    subtitle "Case 8: repeated names and --number out of range (wrong using)"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign -c Category -f /tmp/A/file1.txt --yes $OPTCONF
    $vitis create -c Category2 $OPTCONF
    CMD="$vitis assign -c Category2 -v Category/file1.txt --number 3"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) File with this number not found."
    if [[ "$output" == "$expected" && ! -e ${VTS}/Category2/file1.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}" /tmp/A/
    destroy_file_env
}

case_2_5_9() {
    subtitle "Case 9: --number with invalid value (wrong using)"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign -c Category -f /tmp/A/file1.txt --yes $OPTCONF
    $vitis create -c Category2 $OPTCONF
    CMD="$vitis assign -c Category2 -v Category/file1.txt --number X"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'assign'.
See: vitis-sl --help"
    if [[ "$output" == "$expected" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}" /tmp/A/
    destroy_file_env
}

case_2_5_10() {
    subtitle "Case 10: reappointment"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    CMD="$vitis assign Category -v Category/file1.txt --yes"
    output=$($CMD $OPTCONF 2>&1)
    if [[ -L ${VTS}/Category/file1.txt
      && `readlink ${VTS}/Category/file1.txt` == $FILE1 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo "$output"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_5_11() {
    subtitle "Case 11: pointer to fragment"
    create_file_env
    $vitis create Poetry $OPTCONF
    $vitis assign Poetry -f ${YEATS} $OPTCONF
    FRAGOPTS="--start 9,1 --fragname But_I_being_poor"
    CMD="$vitis assign Poetry -v Poetry/yeats.txt ${FRAGOPTS}"
    $CMD $OPTCONF
    local expected_fragpointer="\
Type: text
Path: /tmp/Files/yeats.txt
Start: 9,1
Finish: "
    local link=${VTS}/Poetry/But_I_being_poor.fragpointer
    local autolink=${VTS}/__auto/FP/Fragments/But_I_being_poor.fragpointer
    local path=${VTS}/__vitis/fragpointers/But_I_being_poor.fragpointer

    if [[ -L "${link}" && `readlink ${link}` == "${path}"
      && -L "${autolink}" && `readlink ${autolink}` == "${path}"
      && ! -L "${path}" && -f "${path}"
      && `cat ${path}` == "${expected_fragpointer}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

procedure_2_5() {
    procedure_title "PROCEDURE 2.5 'Assigning categories for Vitis-files'"
    case_2_5_1
    case_2_5_2
    # case_2_5_3 # no bug, frozen for future
    # case_2_5_4 # no bug, frozen for future
    case_2_5_5
    case_2_5_6
    case_2_5_7
    case_2_5_8
    case_2_5_9
    case_2_5_10 #behavior is not bad, but really behavior not defined
    case_2_5_11
}

COLLECTED_WORKS=/tmp/Collected
DIR=$COLLECTED_WORKS
UNPUBLISHED=${DIR}/unpublished.txt
DIR60=${DIR}/60s
BEST=${DIR60}/Best
DIR70=${DIR}/70s

create_directory_hierarchy() {
    mkdir $DIR
        touch ${DIR}/unpublished.txt
        mkdir ${DIR}/60s
            touch ${DIR}/60s/thing.txt
            touch ${DIR}/60s/other.txt
            mkdir ${DIR}/60s/Best/
                touch ${DIR}/60s/Best/note.txt
        mkdir ${DIR}/70s
            touch ${DIR}/70s/last.txt
}

destroy_directory_hierarchy() {
    rm -rf $DIR
}

case_2_6_1() {
    subtitle "Case 1: simple directory transfer to Vitis"
    create_directory_hierarchy
    CMD="$vitis assign -d $DIR"
    $CMD $OPTCONF
    if [[ -d ${VTS}/Collected
      && -L ${VTS}/Collected/unpublished.txt
      && `readlink ${VTS}/Collected/unpublished.txt` == $UNPUBLISHED
      && -d ${VTS}/Collected/60s
      && -L ${VTS}/Collected/60s/thing.txt
      && `readlink ${VTS}/Collected/60s/thing.txt` == ${DIR60}/thing.txt
      && -L ${VTS}/Collected/60s/other.txt
      && `readlink ${VTS}/Collected/60s/other.txt` == ${DIR60}/other.txt
      && -d ${VTS}/Collected/60s/Best
      && -L ${VTS}/Collected/60s/Best/note.txt
      && `readlink ${VTS}/Collected/60s/Best/note.txt` == ${BEST}/note.txt
      && -d ${VTS}/Collected/70s
      && -L ${VTS}/Collected/70s/last.txt
      && `readlink ${VTS}/Collected/70s/last.txt` == ${DIR70}/last.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_directory_hierarchy
}

case_2_6_2() {
    subtitle "Case 2: directory transfer to Vitis with --as"
    create_directory_hierarchy
    CMD="$vitis assign -d $DIR --as Works"
    $CMD $OPTCONF
    if [[ -d ${VTS}/Works
      && -L ${VTS}/Works/unpublished.txt
      && `readlink ${VTS}/Works/unpublished.txt` == $UNPUBLISHED
      && -d ${VTS}/Works/60s
      && -L ${VTS}/Works/60s/thing.txt
      && `readlink ${VTS}/Works/60s/thing.txt` == ${DIR60}/thing.txt
      && -L ${VTS}/Works/60s/other.txt
      && `readlink ${VTS}/Works/60s/other.txt` == ${DIR60}/other.txt
      && -d ${VTS}/Works/60s/Best
      && -L ${VTS}/Works/60s/Best/note.txt
      && `readlink ${VTS}/Works/60s/Best/note.txt` == ${BEST}/note.txt
      && -d ${VTS}/Works/70s
      && -L ${VTS}/Works/70s/last.txt
      && `readlink ${VTS}/Works/70s/last.txt` == ${DIR70}/last.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_directory_hierarchy
}

case_2_6_3() {
    subtitle "Case 3: directory transfer to Vitis with --as and -c"
    create_directory_hierarchy
    CMD="$vitis assign -d $DIR -c Books --as Works --yes"
    $CMD $OPTCONF
    if [[ -d ${VTS}/Works
      && -L ${VTS}/Works/unpublished.txt
      && `readlink ${VTS}/Works/unpublished.txt` == $UNPUBLISHED
      && `readlink ${VTS}/Books/unpublished.txt` == $UNPUBLISHED
      && -d ${VTS}/Works/60s
      && -L ${VTS}/Works/60s/thing.txt
      && `readlink ${VTS}/Works/60s/thing.txt` == ${DIR60}/thing.txt
      && `readlink ${VTS}/Books/thing.txt` == ${DIR60}/thing.txt
      && -L ${VTS}/Works/60s/other.txt
      && `readlink ${VTS}/Works/60s/other.txt` == ${DIR60}/other.txt
      && `readlink ${VTS}/Books/other.txt` == ${DIR60}/other.txt
      && -d ${VTS}/Works/60s/Best
      && -L ${VTS}/Works/60s/Best/note.txt
      && `readlink ${VTS}/Works/60s/Best/note.txt` == ${BEST}/note.txt
      && `readlink ${VTS}/Books/note.txt` == ${BEST}/note.txt
      && -d ${VTS}/Works/70s
      && -L ${VTS}/Works/70s/last.txt
      && `readlink ${VTS}/Works/70s/last.txt` == ${DIR70}/last.txt
      && `readlink ${VTS}/Books/last.txt` == ${DIR70}/last.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_directory_hierarchy
}

case_2_6_4() {
    subtitle "Case 4: directory transfer to Vitis with --as and --save"
    create_directory_hierarchy
    CMD="$vitis assign -d $DIR --as Works --save"
    $CMD $OPTCONF_FSPACES
    if [[ -d ${VTS}/Works
      && -L ${VTS}/Works/unpublished.txt
      && `readlink ${VTS}/Works/unpublished.txt` == \
          ${FSPACE1}/Collected/unpublished.txt 
      && -d ${VTS}/Works/60s
      && -L ${VTS}/Works/60s/thing.txt
      && `readlink ${VTS}/Works/60s/thing.txt` == \
          ${FSPACE1}/Collected/60s/thing.txt
      && -L ${VTS}/Works/60s/other.txt
      && `readlink ${VTS}/Works/60s/other.txt` == \
          ${FSPACE1}/Collected/60s/other.txt
      && -d ${VTS}/Works/60s/Best
      && -L ${VTS}/Works/60s/Best/note.txt
      && `readlink ${VTS}/Works/60s/Best/note.txt` == \
          ${FSPACE1}/Collected/60s/Best/note.txt
      && -d ${VTS}/Works/70s
      && -L ${VTS}/Works/70s/last.txt
      && `readlink ${VTS}/Works/70s/last.txt` == \
          ${FSPACE1}/Collected/70s/last.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    rm -rf $FSPACE1
    destroy_directory_hierarchy
}

case_2_6_5() {
    subtitle "Case 5: autosave"
    create_directory_hierarchy
    CMD="$vitis assign -d $DIR --as Works"
    $CMD $OPTCONF_AUTOSAVE
    if [[ -d ${VTS}/Works
      && -L ${VTS}/Works/unpublished.txt
      && `readlink ${VTS}/Works/unpublished.txt` == \
          ${FSPACE1}/Collected/unpublished.txt 
      && -d ${VTS}/Works/60s
      && -L ${VTS}/Works/60s/thing.txt
      && `readlink ${VTS}/Works/60s/thing.txt` == \
          ${FSPACE1}/Collected/60s/thing.txt
      && -L ${VTS}/Works/60s/other.txt
      && `readlink ${VTS}/Works/60s/other.txt` == \
          ${FSPACE1}/Collected/60s/other.txt
      && -d ${VTS}/Works/60s/Best
      && -L ${VTS}/Works/60s/Best/note.txt
      && `readlink ${VTS}/Works/60s/Best/note.txt` == \
          ${FSPACE1}/Collected/60s/Best/note.txt
      && -d ${VTS}/Works/70s
      && -L ${VTS}/Works/70s/last.txt
      && `readlink ${VTS}/Works/70s/last.txt` == \
          ${FSPACE1}/Collected/70s/last.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    rm -rf $FSPACE1
    destroy_directory_hierarchy
}

procedure_2_6() {
    procedure_title "PROCEDURE 2.6 'Assigning categories for directories'"
    case_2_6_1
    case_2_6_2
    case_2_6_3
    case_2_6_4
    case_2_6_5
}

case_2_7_1() {
    subtitle "Case 1: simple expression"
    $vitis create Winter $OPTCONF
    touch /tmp/snow /tmp/coldness
    $vitis assign Winter -f /tmp/snow /tmp/coldness $OPTCONF
    CMD="$vitis assign January -e Winter --yes"
    $CMD $OPTCONF
    if [[ -L ${VTS}/January/snow
      && `readlink ${VTS}/January/snow` == /tmp/snow
      && -L ${VTS}/January/coldness
      && `readlink ${VTS}/January/coldness` == /tmp/coldness ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/snow /tmp/coldness
    rm -rf "${VTS}"
}

case_2_7_2() {
    subtitle "Case 2: complex expression"
    $vitis create December January $OPTCONF
    touch /tmp/snow /tmp/coldness
    $vitis assign December -f /tmp/snow $OPTCONF
    $vitis assign January -f /tmp/coldness $OPTCONF
    CMD="$vitis assign Winter -e December u: January --yes"
    $CMD $OPTCONF
    if [[ -L ${VTS}/Winter/snow
      && `readlink ${VTS}/Winter/snow` == /tmp/snow
      && -L ${VTS}/Winter/coldness
      && `readlink ${VTS}/Winter/coldness` == /tmp/coldness ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/snow /tmp/coldness
    rm -rf "${VTS}"
}

case_2_7_3() {
    subtitle "Case 3: complex expression (interesting version)"
    $vitis create December January $OPTCONF
    touch /tmp/snow /tmp/coldness
    $vitis assign December -f /tmp/snow $OPTCONF
    $vitis assign January -f /tmp/coldness $OPTCONF
    CMD="$vitis assign January -e December u: January"
    $CMD $OPTCONF
    if [[ -L ${VTS}/January/snow
      && `readlink ${VTS}/January/snow` == /tmp/snow
      && -L ${VTS}/January/coldness
      && `readlink ${VTS}/January/coldness` == /tmp/coldness ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/snow /tmp/coldness
    rm -rf "${VTS}"
}

case_2_7_4() {
    subtitle "Case 4: wrong expression"
    $vitis create Winter $OPTCONF
    touch /tmp/snow /tmp/coldness
    $vitis assign Winter -f /tmp/snow /tmp/coldness $OPTCONF
    CMD="$vitis assign January -e Winter i: u: Winter --yes"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong expression.
See: vitis-sl --help"
    if [[ "${output}" == "${expected}"  ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/snow /tmp/coldness
    rm -rf "${VTS}"
}

procedure_2_7() {
    procedure_title "PROCEDURE 2.7 'Assigning categories by expression'"
    case_2_7_1
    case_2_7_2
    case_2_7_3
    case_2_7_4
}

case_2_8_1() {
    subtitle "Case 1: one network bookmark"
    CMD="$vitis assign -i dlang.org"
    $CMD $OPTCONF
    link_entries_html=${ENTRY_BASEDIR}/html
    entry="${link_entries_html}/Home - D Programming Language.desktop"
    expected_content="[Desktop Entry]
Encoding=UTF-8
Name=Home - D Programming Language
Type=Link
URL=http://dlang.org
Comment=html
Icon=text-html"
    real_content=`cat "$entry"`
    autocategory=${VTS}/__auto/N/NetworkBookmarks
    link="${autocategory}/Home - D Programming Language.desktop"
    if [[ -f "$entry" && "${expected_content}" == "${real_content}"
      && -L "$link" && `readlink "$link"` == "$entry" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_8_2() {
    subtitle "Case 2: two network bookmarks"
    DLANG="https://dlang.org"
    CMD="$vitis assign -i $DLANG $DEBIANHURD"
    $CMD $OPTCONF
    link_entries_html=${VTS}/__vitis/link_entries/html
    link_entries_non_html=${VTS}/__vitis/link_entries/non-html
    entry_1="${link_entries_html}/Home - D Programming Language.desktop"
    entry_2="${link_entries_non_html}/${ISO}.desktop"
    expected_content_1="[Desktop Entry]
Encoding=UTF-8
Name=Home - D Programming Language
Type=Link
URL=$DLANG
Comment=html
Icon=text-html"
    expected_content_2="[Desktop Entry]
Encoding=UTF-8
Name=$ISO
Type=Link
URL=$DEBIANHURD
Comment=non-html
Icon=text-html"
    real_content_1=`cat "$entry_1"`
    real_content_2=`cat "$entry_2"`
    autocategory=${VTS}/__auto/N/NetworkBookmarks
    link_1="${autocategory}/Home - D Programming Language.desktop"
    link_2="${autocategory}/${ISO}.desktop"
    if [[ -f "$entry_1" && "${expected_content_1}" == "${real_content_1}"
      && -L "$link_1" && `readlink "$link_1"` == "$entry_1"
      && -f "$entry_2" && "${expected_content_2}" == "${real_content_2}"
      && -L "$link_2" && `readlink "$link_2"` == "$entry_2" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_8_3() {
    subtitle "Case 3: one network bookmark and one category by -c"
    CMD="$vitis assign -i dlang.org -c D --yes"
    $CMD $OPTCONF
    link_entries_html=${VTS}/__vitis/link_entries/html
    entry="${link_entries_html}/Home - D Programming Language.desktop"
    expected_content="[Desktop Entry]
Encoding=UTF-8
Name=Home - D Programming Language
Type=Link
URL=http://dlang.org
Comment=html
Icon=text-html"
    real_content=`cat "$entry"`
    autocategory=${VTS}/__auto/N/NetworkBookmarks
    autolink="${autocategory}/Home - D Programming Language.desktop"
    dlink="${VTS}/D/Home - D Programming Language.desktop"
    if [[ -f "$entry" && "${expected_content}" == "${real_content}"
      && -L "$autolink" && `readlink "$autolink"` == "$entry"
      && -L "$dlink" && `readlink "$dlink"` == "$entry" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_8_4() {
    subtitle "Case 4: two network bookmarks and -c"
    DLANG="https://dlang.org"
    DTITLE="Home - D Programming Language"
    CMD="$vitis assign -i $DLANG $DEBIANHURD -c Favorites --yes"
    $CMD $OPTCONF
    link_entries_html=${VTS}/__vitis/link_entries/html
    link_entries_non_html=${VTS}/__vitis/link_entries/non-html
    entry_1="${link_entries_html}/${DTITLE}.desktop"
    entry_2="${link_entries_non_html}/${ISO}.desktop"
    expected_content_1="[Desktop Entry]
Encoding=UTF-8
Name=$DTITLE
Type=Link
URL=$DLANG
Comment=html
Icon=text-html"
    expected_content_2="[Desktop Entry]
Encoding=UTF-8
Name=$ISO
Type=Link
URL=$DEBIANHURD
Comment=non-html
Icon=text-html"
    real_content_1=`cat "$entry_1"`
    real_content_2=`cat "$entry_2"`
    autocategory=${VTS}/__auto/N/NetworkBookmarks
    link_1="${autocategory}/Home - D Programming Language.desktop"
    link_2="${autocategory}/${ISO}.desktop"
    if [[ -f "$entry_1" && "${expected_content_1}" == "${real_content_1}"
      && -L "$link_1" && `readlink "$link_1"` == "$entry_1"
      && -L "${VTS}/Favorites/${DTITLE}.desktop"
      && `readlink "${VTS}/Favorites/${DTITLE}.desktop"` == "$entry_1"
      && -f "$entry_2" && "${expected_content_2}" == "${real_content_2}"
      && -L "$link_2" && `readlink "$link_2"` == "$entry_2"
      && -L "${VTS}/Favorites/${ISO}.desktop"
      && `readlink "${VTS}/Favorites/${ISO}.desktop"` == "$entry_2"
      ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo "1"
        ls /tmp/Vitis/__vitis/link_entries/
        echo "2"
        ls /tmp/Vitis/__vitis/link_entries/non-html
        echo "3"
        ls /tmp/Vitis/__vitis/link_entries/html
    fi
    rm -rf "${VTS}"
}

case_2_8_5() {
    subtitle "Case 5: one network bookmark and --save"
    URL="https://dlang.org/phobos/std_format.html"
    TITLE="std.format - D Programming Language"
    CMD="$vitis assign -i $URL --save"
    $CMD $OPTCONF
    link_entries_html=${VTS}/__vitis/link_entries/html
    entry="${link_entries_html}/${TITLE}.desktop"
    expected_content="[Desktop Entry]
Encoding=UTF-8
Name=$TITLE
Type=Link
URL=$URL
Comment=html
Icon=text-html"
    real_content=`cat "$entry"`
    autocategory=${VTS}/__auto/N/NetworkBookmarks
    link="${autocategory}/${TITLE}.desktop"
    if [[ -f "$entry" && "${expected_content}" == "${real_content}"
      && -L "$link" && `readlink "$link"` == "$entry"
      && -f "${link_entries_html}/${TITLE}.desktop" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

procedure_2_8() {
    [[ $NET_TESTS == off ]] && return
    procedure_title "PROCEDURE 2.8 'Network Bookmarks'"
    case_2_8_1
    case_2_8_2
    case_2_8_3
    case_2_8_4
    case_2_8_5
}

case_2_9_1() {
    subtitle "Case 1: simple renaming"
    $vitis create Militia $OPTCONF
    touch /tmp/officer
    $vitis assign Militia -f /tmp/officer $OPTCONF
    CMD="$vitis assign Militia -n Police"
    $CMD $OPTCONF
    if [[ ! -d ${VTS}/Militia && -d ${VTS}/Police
      && -L ${VTS}/Police/officer
      && `readlink ${VTS}/Police/officer` == /tmp/officer ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/officer
    rm -rf "${VTS}"
}

case_2_9_2() {
    subtitle "Case 2: simple renaming of alias"
    $vitis create Category $OPTCONF
    touch /tmp/officer
    $vitis assign Category -f /tmp/officer $OPTCONF
    $vitis assign Category -a Alias $OPTCONF
    CMD="$vitis assign Alias -n NewAlias"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/Alias && -L ${VTS}/NewAlias
      && -L ${VTS}/NewAlias/officer
      && `readlink ${VTS}/NewAlias/officer` == /tmp/officer ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/officer
    rm -rf "${VTS}"
}

case_2_9_3() {
    subtitle "Case 3: wrong renaming to existent category"
    $vitis create Category Tag $OPTCONF
    touch /tmp/officer
    $vitis assign Category -f /tmp/officer $OPTCONF
    CMD="$vitis assign Category -n Tag"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Tag: this category already exists."
    if [[ "${output}" == "${expected}"
      && -e "${VTS}/Category/officer" && ! -e "${VTS}/Tag/officer" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/officer
    rm -rf "${VTS}"
}

case_2_9_4() {
    subtitle "Case 4: simple renaming"
    $vitis create Militia FBI $OPTCONF
    CMD="$vitis assign Militia FBI -n Police"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'assign'.
See: vitis-sl --help"
    if [[ ! -d ${VTS}/Police && -d ${VTS}/Militia && -d ${VTS}/FBI
      && "$output" == "$expected" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_9_5() {
    subtitle "Case 5: renaming to subcategory"
    $vitis create D Programming $OPTCONF
    touch /tmp/dbook.txt
    $vitis assign D Programming -f /tmp/dbook.txt $OPTCONF
    CMD="$vitis assign D -n Programming/D"
    output=$($CMD $OPTCONF 2>&1)
    if [[ `readlink ${VTS}/Programming/D/dbook.txt` == /tmp/dbook.txt
      && ! -e ${VTS}/D
      && ! -e ${VTS}/Programming/dbook.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/dbook.txt
    rm -rf "${VTS}"
}

case_2_9_6() {
    subtitle "Case 6: renaming to subcategory of non-existent category"
    $vitis create D $OPTCONF
    touch /tmp/dbook.txt
    $vitis assign D -f /tmp/dbook.txt $OPTCONF
    CMD="$vitis assign D -n Programming/D"
    output=$($CMD $OPTCONF 2>&1)
    local path=`readlink ${VTS}/Programming/D/dbook.txt`
    if [[ "$path" == /tmp/dbook.txt
      && ! -e ${VTS}/D
      && ! -e ${VTS}/Programming/dbook.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo "$output"
    fi
    rm /tmp/dbook.txt
    rm -rf "${VTS}"
}

case_2_9_7() {
    subtitle "Case 7: renaming to autocategory (wrong using)"
    $vitis create D $OPTCONF
    CMD="$vitis assign D -n NetworkBookmarks"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) NetworkBookmarks: this renaming is not allowed."
    get_result "$CMD" "$output" "$expected"
    rm -rf "${VTS}"
}

case_2_9_8() {
    subtitle "Case 8: renaming to subcategory of autocategory (wrong using)"
    $vitis create D $OPTCONF
    CMD="$vitis assign D -n NetworkBookmarks/subcat"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) NetworkBookmarks/subcat: this renaming is not allowed."
    get_result "$CMD" "$output" "$expected"
    rm -rf "${VTS}"
}



procedure_2_9() {
    procedure_title "PROCEDURE 2.9 'Renaming category'"
    case_2_9_1
    case_2_9_2
    case_2_9_3
    case_2_9_4
    case_2_9_5
    case_2_9_6
    case_2_9_7
    case_2_9_8
}

case_2_10_1() {
    subtitle "Case 1: simple renaming file"
    create_file_env
    $vitis create C1 C2 C2/Sub $OPTCONF
    $vitis assign C1 C2/Sub -f $FILE1 $FILE2 $OPTCONF
    CMD="$vitis assign -v C1/file1.txt -n superfile"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1/file1.txt && ! -e ${VTS}/C2/Sub/file1.txt
      && -L ${VTS}/C1/superfile && -L ${VTS}/C2/Sub/superfile
      && `readlink ${VTS}/C1/superfile` == $FILE1
      && `readlink ${VTS}/C2/Sub/superfile` == $FILE1
      && `readlink ${VTS}/C1/file2.txt` == $FILE2
      && `readlink ${VTS}/C2/Sub/file2.txt` == $FILE2 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_10_2() {
    subtitle "Case 2: renaming file, wrong name"
    create_file_env
    $vitis create C1 C2 C2/Sub $OPTCONF
    $vitis assign C1 C2/Sub -f $FILE1 $FILE2 $OPTCONF
    CMD="$vitis assign -v C1/fileX.txt -n superfile"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) C1/fileX.txt: file in Vitis is not found."
    get_result "$CMD" "$output" "$expected"
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_10_3() {
    subtitle "Case 3: renaming to existent name"
    create_file_env
    $vitis create C1 C2 C2/Sub $OPTCONF
    $vitis assign C1 C2/Sub -f $FILE1 $FILE2 $OPTCONF
    CMD="$vitis assign -v C1/file1.txt -n file2.txt"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1/file1.txt && ! -e ${VTS}/C2/Sub/file1.txt
      && `readlink ${VTS}/C1/__repeated_names/2/file2.txt` == $FILE1
      && `readlink ${VTS}/C2/Sub/__repeated_names/2/file2.txt` == $FILE1
      && `readlink ${VTS}/C1/file2.txt` == $FILE2
      && `readlink ${VTS}/C2/Sub/file2.txt` == $FILE2 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_10_4() {
    subtitle "Case 4: repeated names and using --number"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign -c Category -f /tmp/A/file1.txt --yes $OPTCONF
    # by alphabet, /tmp/A/file1.txt is 1, /tmp/Files/file1.txt is 2
    CMD="$vitis assign -v Category/file1.txt -n fileOne.txt --number 1"
    $CMD $OPTCONF
    if [[ -L ${VTS}/Category/fileOne.txt 
      && `readlink ${VTS}/Category/fileOne.txt` == /tmp/A/file1.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}" /tmp/A/
    destroy_file_env
}

case_2_10_5() {
    subtitle "Case 5: repeated names and no --number (error)"
    create_file_env
    $vitis assign -c Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign -c Category -f /tmp/A/file1.txt --yes $OPTCONF
    # by alphabet, /tmp/A/file1.txt is 1, /tmp/Files/file1.txt is 2
    CMD="$vitis assign -v Category/file1.txt -n fileOne.txt"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Specify the number of the required file \
using the '--number' flag."
    if [[ ! -L ${VTS}/Category/fileOne.txt
      && "$output" == "$expected" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}" /tmp/A/
    destroy_file_env
}

procedure_2_10() {
    procedure_title "PROCEDURE 2.10 'Renaming file'"
    case_2_10_1
    case_2_10_2
    case_2_10_3
    case_2_10_4
    case_2_10_5
}

case_2_11_1() {
    subtitle "Case 1: simple renaming file"
    create_file_env
    $vitis create C1 C2 C2/Sub $OPTCONF
    $vitis assign C1 C2/Sub -f $FILE1 $FILE2 $OPTCONF
    CMD="$vitis assign -f $FILE1 -n superfile"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1/file1.txt && ! -e ${VTS}/C2/Sub/file1.txt
      && -L ${VTS}/C1/superfile && -L ${VTS}/C2/Sub/superfile
      && `readlink ${VTS}/C1/superfile` == $FILE1
      && `readlink ${VTS}/C2/Sub/superfile` == $FILE1
      && `readlink ${VTS}/C1/file2.txt` == $FILE2
      && `readlink ${VTS}/C2/Sub/file2.txt` == $FILE2 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_11_2() {
    subtitle "Case 2: renaming file, nonexistent file (wrong using)"
    create_file_env
    $vitis create C1 C2 C2/Sub $OPTCONF
    $vitis assign C1 C2/Sub -f $FILE1 $FILE2 $OPTCONF
    CMD="$vitis assign -f fileX.txt -n superfile"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) fileX.txt: file not found."
    get_result "$CMD" "$output" "$expected"
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_11_3() {
    subtitle "Case 3: name not found in Vitis (wrong using)"
    create_file_env
    $vitis create C1 C2 C2/Sub $OPTCONF
    $vitis assign C1 C2/Sub -f $FILE1 $FILE2 $OPTCONF
    CMD="$vitis assign -f $FILE3 -n superfile"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) /tmp/Files/file3.txt: file in Vitis is not found."
    get_result "$CMD" "$output" "$expected"
    rm -rf "${VTS}"
    destroy_file_env
}

case_2_11_4() {
    subtitle "Case 4: renaming to existent name"
    create_file_env
    $vitis create C1 C2 C2/Sub $OPTCONF
    $vitis assign C1 C2/Sub -f $FILE1 $FILE2 $OPTCONF
    CMD="$vitis assign -f $FILE1 -n file2.txt"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1/file1.txt && ! -e ${VTS}/C2/Sub/file1.txt
      && `readlink ${VTS}/C1/__repeated_names/2/file2.txt` == $FILE1
      && `readlink ${VTS}/C2/Sub/__repeated_names/2/file2.txt` == $FILE1
      && `readlink ${VTS}/C1/file2.txt` == $FILE2
      && `readlink ${VTS}/C2/Sub/file2.txt` == $FILE2 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
    destroy_file_env
}

procedure_2_11() {
    procedure_title "PROCEDURE 2.11 'Renaming file. Second way'"
    case_2_11_1
    case_2_11_2
    case_2_11_3
    case_2_11_4
}

case_2_12_1() {
    subtitle "Case 1: global subcategory"
    $vitis create Audiofiles $OPTCONF
    CMD="$vitis assign Audiofiles -s Music"
    $CMD $OPTCONF
    if [[ `readlink ${VTS}/Music` == ${VTS}/Audiofiles/Music
      && -d ${VTS}/Audiofiles/Music ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_12_2() {
    subtitle "Case 2: global subcategory and other alias as obstruction"
    $vitis create Audiofiles $OPTCONF
    $vitis create Audioclips $OPTCONF
    $vitis assign Audioclips -a Music $OPTCONF
    CMD="$vitis assign Audiofiles -s Music --no"
    output=$($CMD $OPTCONF 2>&1)
    expected="Music: this category already exists.
/tmp/Vitis/Music -> /tmp/Vitis/Audioclips
The request is skipped."
    if [[ "$expected" == "$output" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
        echo -e "$output"
    fi
    rm -rf "${VTS}"
}

case_2_12_3() {
    subtitle "Case 3: global subcategory and other subcategory as obstruction"
    $vitis create Audiofiles/Music $OPTCONF
    CMD="$vitis assign Audiofiles -s Music"
    output=$($CMD $OPTCONF 2>&1)
    expected="Audiofiles/Music: this subcategory already exists. \
The request is skipped."
    if [[ "$expected" == "$output" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_12_4() {
    subtitle "Case 4: subcategory for autocategory (wrong using)"
    CMD="$vitis assign NetworkBookmarks -s Pages"
    output=$($CMD $OPTCONF 2>&1)
    msg=": this category is not available for this use. Skipped."
    expected="NetworkBookmarks${msg}"
    if [[ "$expected" == "$output" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_12_5() {
    subtitle "Case 5: local subcategory"
    $vitis create Audiofiles $OPTCONF
    CMD="$vitis assign Audiofiles -s Music --local"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/Music && -d ${VTS}/Audiofiles/Music ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_2_12_6() {
    subtitle "Case 6: transfer category to subcategory"
    $vitis create Audiofiles Music $OPTCONF
    touch /tmp/track.mp3
    $vitis assign Music -f /tmp/track.mp3 $OPTCONF
    CMD="$vitis assign Audiofiles -s Music"
    $CMD $OPTCONF
    if [[ `readlink ${VTS}/Audiofiles/Music/track.mp3` == /tmp/track.mp3
      && `readlink ${VTS}/Music` == ${VTS}/Audiofiles/Music ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/track.mp3
    rm -rf "${VTS}"
}

procedure_2_12() {
    procedure_title "PROCEDURE 2.12 'Global and local subcategories'"
    case_2_12_1
    case_2_12_2
    case_2_12_3
    case_2_12_4
    case_2_12_5
    case_2_12_6
}

tests_assign() {
    procedure_2_1
    procedure_2_2
    procedure_2_3
    procedure_2_4
    procedure_2_5
    procedure_2_6
    procedure_2_7
    procedure_2_8
    procedure_2_9
    procedure_2_10
    procedure_2_11
    procedure_2_12
}


################################################################################
# vitis delete
################################################################################

case_3_1_1() {
    subtitle "Case 1: deleting one category with alias"
    $vitis create Category $OPTCONF
    $vitis assign Category -a Alias $OPTCONF
    CMD="$vitis delete Category"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/Category && ! -e ${VTS}/Alias ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_3_1_2() {
    subtitle "Case 2: deleting two categories"
    $vitis create C1 C2 C3 $OPTCONF
    CMD="$vitis delete C1 C2"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1 && ! -e ${VTS}/C2 && -e ${VTS}/C3 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_3_1_3() {
    subtitle "Case 3: deleting nonexistent category"
    CMD="$vitis delete Category"
    output=$($CMD $OPTCONF 2>&1)
    expected="Category: this category doesn't exist. The request is skipped."
    get_result "${CMD}" "${output}" "${expected}"
    rm -rf "${VTS}"
}

case_3_1_4() {
    subtitle "Case 4: deleting four nonexistent and five existent categories"
    $vitis create C2 C5 C6 C7 C8 $OPTCONF
    CMD="$vitis delete C1 C2 C3 C4 C5 C6 C7 C8 C9"
    output=$($CMD $OPTCONF 2>&1)
    expected="C1: this category doesn't exist. The request is skipped.
C3: this category doesn't exist. The request is skipped.
C4: this category doesn't exist. The request is skipped.
C9: this category doesn't exist. The request is skipped."
    if [[ "${output}" == "${expected}" && ! -e ${VTS}/C* ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

case_3_1_5() {
    subtitle "Case 5: deleting alias"
    $vitis create Category $OPTCONF
    touch /tmp/simple_file
    $vitis assign Category -f /tmp/simple_file $OPTCONF
    $vitis assign Category -a Alias $OPTCONF
    CMD="$vitis delete Alias"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/Alias && -L ${VTS}/Category/simple_file ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/simple_file
    rm -rf "${VTS}"
}

case_3_1_6() {
    subtitle "Case 6: deleting alias with --force"
    $vitis create Category $OPTCONF
    touch /tmp/simple_file
    $vitis assign Category -f /tmp/simple_file $OPTCONF
    $vitis assign Category -a Alias $OPTCONF
    CMD="$vitis delete Alias --force"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/Alias && ! -e ${VTS}/Category ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm /tmp/simple_file
    rm -rf "${VTS}"
}

case_3_1_7() {
    subtitle "Case 7: deleting autocategory"
    CMD="$vitis delete NetworkBookmarks"
    output=$($CMD $OPTCONF 2>&1)
    msg=": deletion of this category is prohibited. Skipped."
    expected="NetworkBookmarks${msg}"
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf "${VTS}"
}

procedure_3_1() {
    procedure_title "PROCEDURE 3.1 'Deleting categories'"
    case_3_1_1
    case_3_1_2
    case_3_1_3
    case_3_1_4
    case_3_1_5
    case_3_1_6
    case_3_1_7
}

case_3_2_1() {
    subtitle "Case 1: simple deleting"
    create_file_env
    $vitis assign Category -f $FILE1 $FILE2 --yes $OPTCONF
    CMD="$vitis delete -c Category -f file1.txt"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/Category/file1.txt && -d ${VTS}/Category ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_2_2() {
    subtitle "Case 2: two links"
    create_file_env
    $vitis assign Category -f $FILE1 $FILE2 $FILE3 --yes $OPTCONF
    CMD="$vitis delete -c Category -f file1.txt file2.txt"
    link_1=${VTS}/Category/file1.txt
    link_2=${VTS}/Category/file2.txt
    link_3=${VTS}/Category/file3.txt
    $CMD $OPTCONF
    if [[ ! -e $link_1  && ! -e $link_2 && -e $link_3 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_2_3() {
    subtitle "Case 3: deleting nonexistent link"
    create_file_env
    $vitis assign Category -f $FILE1 --yes $OPTCONF
    CMD="$vitis delete -c Category -f file99.txt"
    output=$($CMD $OPTCONF 2>&1)
    expected="Category/file99.txt: file in Vitis is not found. \
The request is skipped."
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_2_4() {
    subtitle "Case 4: repeated names"
    create_file_env
    $vitis assign Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign Category -f /tmp/A/file1.txt --yes $OPTCONF
    CMD="$vitis delete Category -f file1.txt --number 1"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/Category/__repeated_names/2/file1.txt
      && `readlink ${VTS}/Category/file1.txt` == $FILE1 ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf /tmp/A
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_2_5() {
    subtitle "Case 5: repeated names without --number (wrong using)"
    create_file_env
    $vitis assign Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign Category -f /tmp/A/file1.txt --yes $OPTCONF
    CMD="$vitis delete Category -f file1.txt"
    output=$($CMD $OPTCONF 2>&1)
    msg="Specify the number of the required file using the '--number' flag."
    expected="$msg The request is skipped."
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf /tmp/A/
    destroy_file_env
    rm -rf "${VTS}"
}

procedure_3_2() {
    procedure_title "PROCEDURE 3.2 'Deleting files'"
    case_3_2_1
    case_3_2_2
    case_3_2_3
    case_3_2_4
    case_3_2_5
}

case_3_3_1() {
    subtitle "Case 1: simple deletion of all links by name and category"
    create_file_env
    $vitis assign C1 C2 -f $FILE1 --yes $OPTCONF
    CMD="$vitis delete -v C2/file1.txt"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1/file1.txt && ! -e ${VTS}/C2/file2.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_3_2() {
    subtitle "Case 2: simple deletion of all links by name only"
    create_file_env
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign C1 C2 -f $FILE1 --yes $OPTCONF
    $vitis assign C3 -f /tmp/A/file1.txt --yes $OPTCONF
    CMD="$vitis delete -v file1.txt"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1/file1.txt && ! -e ${VTS}/C2/file2.txt
      && ! -e ${VTS}/C3/file1.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf /tmp/A/
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_3_3() {
    subtitle "Case 3: nonexistent link"
    create_file_env
    CMD="$vitis delete -v file1.txt"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) file1.txt: file in Vitis is not found."
    if [[ "${output}" == "${expected}" ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_3_4() {
    subtitle "Case 4: repeated name and --number"
    create_file_env
    $vitis assign C1 -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign C1 C2 -f /tmp/A/file1.txt --yes $OPTCONF
    CMD="$vitis delete -v C1/file1.txt --number 1"
    $CMD $OPTCONF
    if [[ ! -e ${VTS}/C1/__repeated_names/2/file1.txt
      && ! -e ${VTS}/C2/file1.txt
      && -L ${VTS}/C1/file1.txt && `readlink ${VTS}/C1/file1.txt` == $FILE1 ]]
    then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf /tmp/A
    destroy_file_env
    rm -rf "${VTS}"
}

case_3_3_5() {
    subtitle "Case 5: repeated names without --number (wrong using)"
    create_file_env
    $vitis assign Category -f $FILE1 --yes $OPTCONF
    mkdir /tmp/A/
    touch /tmp/A/file1.txt
    $vitis assign Category -f /tmp/A/file1.txt --yes $OPTCONF
    CMD="$vitis delete -v Category/file1.txt"
    output=$($CMD $OPTCONF 2>&1)
    msg="Specify the number of the required file using the '--number' flag."
    expected="(Error) $msg"
    if [[ "${output}" == "${expected}"
        && -L ${VTS}/Category/file1.txt
        && -L ${VTS}/Category/__repeated_names/2/file1.txt ]]; then
        pass "${CMD}"
    else
        fail "${CMD}"
    fi
    rm -rf /tmp/A/
    destroy_file_env
    rm -rf "${VTS}"
}

procedure_3_3() {
    procedure_title "PROCEDURE 3.3 'Deleting files in whole Vitis-system'"
    case_3_3_1
    case_3_3_2
    case_3_3_3
    case_3_3_4
    case_3_3_5
}

tests_delete() {
    procedure_3_1
    procedure_3_2
    procedure_3_3
}


################################################################################
# vitis show
################################################################################

case_4_1_1() {
    subtitle "Case 1: show one category"
    create_file_env && create_category_env
    CMD="$vitis show Text"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] april.txt
[2] bigtext.txt
[3] march.txt
[4] may.txt
[5] visibleFile
[6] yeats.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_2() {
    subtitle "Case 2: show subcategory"
    create_file_env && create_category_env
    CMD="$vitis show Text/Fiction"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] bigtext.txt
[2] yeats.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_3() {
    subtitle "Case 3: show union"
    create_file_env && create_category_env
    CMD="$vitis show Poetry u: Spring"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] april.txt
[2] march.txt
[3] may.txt
[4] spring_file
[5] yeats.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_4() {
    subtitle "Case 4: show intersection"
    create_file_env && create_category_env
    CMD="$vitis show Text i: Spring"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] april.txt
[2] march.txt
[3] may.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_5() {
    subtitle "Case 5: show complement"
    create_file_env && create_category_env
    CMD="$vitis show Text/Fiction \\ Poetry"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] bigtext.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_6() {
    subtitle "Case 6: complex expression"
    create_file_env && create_category_env
    CMD="$vitis show { Poetry u: Spring } i: Text \\ Text/Fiction"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] april.txt
[2] march.txt
[3] may.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_7() {
    subtitle "Case 7: nonexistent category"
    create_file_env && create_category_env
    CMD="$vitis show FairyTail"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) FairyTail: this category doesn't exist."
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_8() {
    subtitle "Case 8: flag --categories"
    create_file_env && create_category_env
    CMD="$vitis show Spring --categories"
    output=$($CMD $OPTCONF 2>&1)
    expected="[1] april.txt
\"Spring\" \"Text\" \"Extension/txt\" \"Format/TXT\" \"Type/Text\"
[2] march.txt
\"Spring\" \"Text\" \"Extension/txt\" \"Format/TXT\" \"Type/Text\"
[3] may.txt
\"Spring\" \"Text\" \"Extension/txt\" \"Format/TXT\" \"Type/Text\"
[4] spring_file
\"Spring\" \"Format/TXT\" \"Type/Text\""
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_9() {
    subtitle "Case 9: flag --details"
    create_file_env && create_category_env
    CMD="$vitis show Spring --details"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] april.txt
Modified: 2019-04-16 00:00:01	Permissions: rw-r--r--
Accessed: 2019-06-16 02:00:00	Size: 30 B (30 B)
[2] march.txt
Modified: 2019-03-16 10:07:00	Permissions: rw-r--r--
Accessed: 2019-06-16 03:00:00	Size: 31 B (31 B)
[3] may.txt
Modified: 2019-05-16 03:50:09	Permissions: rw-r--r--
Accessed: 2019-06-16 01:00:00	Size: 31 B (31 B)
[4] spring_file
Modified: 1970-01-01 00:00:00	Permissions: rw-r--r--
Accessed: 1985-02-02 00:00:00	Size: 0 B (0 B)"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_10() {
    subtitle "Case 10: flag --paths"
    create_file_env && create_category_env
    CMD="$vitis show Spring --paths"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] april.txt
/tmp/Files/april.txt
[2] march.txt
/tmp/Files/march.txt
[3] may.txt
/tmp/Files/may.txt
[4] spring_file
/tmp/Files/spring_file"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_11() {
    subtitle "Case 11: flag --ipaths"
    create_file_env && create_category_env
    create_addit_file_env
    CMD="$vitis show NetworkBookmarks --ipaths"
    output=$($CMD $OPTCONF 2>&1)
    expected="[1] D Programming Language.desktop
URL: http://dlang.org"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_12() {
    subtitle "Case 12: flag --fragment-info"
    create_file_env && create_category_env
    create_addit_file_env
    CMD="$vitis show Fragments --fragment-info"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] But_I_being_poor.fragpointer
Type:     text
Start:    9,1
Finish:   -
Original: /tmp/Files/yeats.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_13() {
    subtitle "Case 13: show one category with numbers (--no-numbers)"
    create_file_env && create_category_env
    CMD="$vitis show Text --no-numbers"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
april.txt
bigtext.txt
march.txt
may.txt
visibleFile
yeats.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_14() {
    subtitle "Case 14: show one category - only 2, 3 and 5"
    create_file_env && create_category_env
    CMD="$vitis show Text -n 2-3,5"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[2] bigtext.txt
[3] march.txt
[5] visibleFile"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_15() {
    subtitle "Case 15: sort by size"
    create_file_env && create_category_env
    CMD="$vitis show Spring u: Text/Fiction --sort=size"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] spring_file
[2] april.txt
[3] march.txt
[4] may.txt
[5] yeats.txt
[6] bigtext.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_16() {
    subtitle "Case 16: sort by modification time"
    create_file_env && create_category_env
    CMD="$vitis show Spring --sort=time"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] spring_file
[2] march.txt
[3] april.txt
[4] may.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_17() {
    subtitle "Case 17: sort by access time"
    create_file_env && create_category_env
    CMD="$vitis show Spring --sort=atime"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] spring_file
[2] may.txt
[3] april.txt
[4] march.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_18() {
    subtitle "Case 18: sort by access time with --reverse"
    create_file_env && create_category_env
    CMD="$vitis show Spring --sort=atime --reverse"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[4] march.txt
[3] april.txt
[2] may.txt
[1] spring_file"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_19() {
    subtitle "Case 19: hidden files without --hidden"
    create_file_env && create_category_env
    CMD="$vitis show HiddenFiles"
    output=$($CMD $OPTCONF 2>&1)
    expected="[1] visibleFile"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_20() {
    subtitle "Case 20: hidden files and --hidden"
    create_file_env && create_category_env
    CMD="$vitis show HiddenFiles --hidden"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] .file1
[2] .file2
[3] visibleFile"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_1_21() {
    subtitle "Case 21: repeated names"
    create_file_env && create_category_env
    CMD="$vitis show Text u: HiddenFiles --paths"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
[1] april.txt
${FILESDIR}/april.txt
[2] bigtext.txt
${FILESDIR}/bigtext.txt
[3] march.txt
${FILESDIR}/march.txt
[4] may.txt
${FILESDIR}/may.txt
[5] 1/visibleFile
${FILESDIR}/ExpDir/visibleFile
[6] 2/visibleFile
${FILESDIR}/visibleFile
[7] yeats.txt
${FILESDIR}/yeats.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

procedure_4_1() {
    procedure_title "PROCEDURE 4.1 'Requests by expression'"
    case_4_1_1
    case_4_1_2
    case_4_1_3
    case_4_1_4
    case_4_1_5
    case_4_1_6
    case_4_1_7
    case_4_1_8
    case_4_1_9
    case_4_1_10
    case_4_1_11
    case_4_1_12
    case_4_1_13
    case_4_1_14
    case_4_1_15
    case_4_1_16
    case_4_1_17
    case_4_1_18
    case_4_1_19
    case_4_1_20
    case_4_1_21
}

case_4_2_1() {
    subtitle "Case 1: simple request"
    create_file_env && create_category_env
    CMD="$vitis show -v Spring/march.txt"
    output=$($CMD $OPTCONF 2>&1)
    expected="[1] march.txt"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_2_2() {
    subtitle "Case 2: request without category (wrong using)"
    create_file_env && create_category_env
    CMD="$vitis show -v visibleFile"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Wrong using of command 'show'.
See: vitis-sl --help"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_2_3() {
    subtitle "Case 3: request by supercategory, no subcategory"
    create_file_env && create_category_env
    CMD="$vitis show -v Text/yeats.txt --categories"
    output=$($CMD $OPTCONF 2>&1)
    expected="[1] yeats.txt
\"Poetry\" \"Text/Fiction\" \"Extension/txt\" \"Format/TXT\" \"Type/Text\""
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_2_4() {
    subtitle "Case 4: repeated names without --number (wrong using)"
    create_file_env && create_category_env
    CMD="$vitis show -v RepeatedFiles/visibleFile"
    output=$($CMD $OPTCONF 2>&1)
    expected="(Error) Specify the number of the required file \
using the '--number' flag."
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_2_5() {
    subtitle "Case 5: repeated name"
    create_file_env && create_category_env
    CMD="$vitis show -v RepeatedFiles/visibleFile --number 2 --paths"
    output=$($CMD $OPTCONF 2>&1)
    expected="[1] visibleFile
/tmp/Files/visibleFile"
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

procedure_4_2() {
    procedure_title "PROCEDURE 4.2 'Requests by file from Vitis'"
    case_4_2_1
    case_4_2_2
    case_4_2_3
    case_4_2_4
    case_4_2_5
}

case_4_3_1() {
    subtitle "Case 1: simple request for category list"
    create_file_env && create_category_env
    CMD="$vitis show --all-categories"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
\"Empty\"
\"HiddenFiles\"
\"Literature\" -> \"Text/Fiction\"
\"Poetry\"
\"RepeatedFiles\"
\"Spring\"
\"Text\"
\"Text/Fiction\""
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_3_2() {
    subtitle "Case 2: empty list"
    CMD="$vitis show --all-categories"
    output=$($CMD $OPTCONF 2>&1)
    expected=""
    get_result "${CMD}" "${output}" "${expected}"
    rm -rf "${VTS}"
}

case_4_3_3() {
    subtitle "Case 3: hidden category"
    create_file_env && create_category_env
    CMD="$vitis show --all-categories --hidden"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
\".HiddenCategory\"
\"Empty\"
\"HiddenFiles\"
\"Literature\" -> \"Text/Fiction\"
\"Poetry\"
\"RepeatedFiles\"
\"Spring\"
\"Text\"
\"Text/Fiction\""
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

case_4_3_4() {
    
    subtitle "Case 4: autocategories"
    create_file_env
    create_category_env
    CMD="$vitis show --all-categories --auto"
    output=$($CMD $OPTCONF 2>&1)
    expected="\
\"Empty\"
\"HiddenFiles\"
\"Literature\" -> \"Text/Fiction\"
\"Poetry\"
\"RepeatedFiles\"
\"Spring\"
\"Text\"
\"Text/Fiction\"
\"Extension\"
\"Extension/txt\"
\"Format\"
\"Format/TXT\"
\"Type\"
\"Type/Text\""
    get_result "${CMD}" "${output}" "${expected}"
    destroy_file_env && rm -rf "${VTS}"
}

procedure_4_3() {
    procedure_title "PROCEDURE 4.3 'All categories'"
    case_4_3_1
    case_4_3_2
    case_4_3_3
    case_4_3_4
}

tests_show() {
    procedure_4_1
    procedure_4_2
    procedure_4_3
    delete_copy_of_category_env
}



################################################################################
# RUNNING
################################################################################

automatic_tests() {
    procedure_base_tests
    tests_create
    tests_assign
    tests_delete
    tests_show
}

manual_tests() {
    echo ""
}


main() {
    LANG=en_US.UTF8
    if [[ $1 == "--manual" ]]; then TEST_MODE="manual"
    elif [[ $1 == "--full" ]]; then TEST_MODE="full"
    fi

    if [[ $TEST_MODE != "manual" ]]; then
        automatic_tests
    fi
    if [[ $TEST_MODE != "auto" ]]; then
        manual_tests
    fi

    show_results
}
main $@

if [[ $bad_cases -ne 0 ]]; then
    exit 1
fi
