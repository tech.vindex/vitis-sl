#!/usr/bin/rdmd --compiler=ldc2
module langtool;
import std.algorithm, std.csv, std.file,
       std.path, std.stdio, std.conv;


void main(string[] args) {
    if (args.length == 2 && args[1].among("--help", "help", "-h")
     || args.length > 3) {
        printHelp();
        return;
    }

    string csvfile = "locales.csv",
           dtxtfile = "locales.dtxt";
    if (args.length == 3) {
        csvfile = args[1];
        dtxtfile = args[2];
    } else if (args.length == 2) {
        csvfile = args[1];
        dtxtfile = baseName(csvfile, ".csv") ~ ".dtxt";
    }

    try {
        string textFromCSV = readText(csvfile);
        string textWith2DArray;
        foreach(record; csvReader(textFromCSV)) {
            textWith2DArray ~= record.to!string ~ ",\n";
        }
        std.file.write(dtxtfile, "[\n" ~ textWith2DArray ~ "]");
    } catch(Exception e) {
        stderr.writeln(e.msg);
    }
}


void printHelp() {
    writeln("Using:");
    writeln(">: localesconvert file.csv");
    writeln("Parameters 'file.csv' are optional.");
    writeln("Default: file.csv is locales.csv.");
}
