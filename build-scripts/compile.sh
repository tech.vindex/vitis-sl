#!/bin/bash -e
source build-scripts/env.sh

SRCDIR=source
SRC="${SRCDIR}/*.d"

BINVERSION=$1        # release or debug
DC=$2                # dmd, ldc2
PHOBOS_LINKING=$3    # dynamic or static
AMALTHEA_LINKING=$4  # dynamic or static
LDC2_STATIC_OPTS="-defaultlib=:libphobos2-ldc.a,:libdruntime-ldc.a,:libz.a"
LDC2_STATIC_OPTS+=" -link-defaultlib-shared=false"
GDC_STATIC_OPTS="-static-libphobos"
DMD_STATIC_OPTS=""

STATIC_OPTS=""
if [[ ${PHOBOS_LINKING} == static ]]; then
    STATIC_OPTS=$(eval echo \$${DC^^}_STATIC_OPTS)
fi

if [[ ${DC} == dmd ]]; then
    DEBUG="-debug -g"
    RELEASE="-O -release"
    OTHER="-preview=rvaluerefparam"
    OUT="-of"
elif [[ ${DC} == ldc2 ]]; then
    DEBUG="-d-debug -gc"
    RELEASE="-O -release"
    OTHER="-preview=rvaluerefparam"
    OUT="-of"
elif [[ ${DC} == gdc ]]; then
    DEBUG="-fdebug"
    RELEASE="-O2 -frelease"
    OTHER="-fpreview=rvaluerefparam"
    OUT="-o "
fi

set -x

LPATH=/usr/lib/${DEB_HOST_MULTIARCH}
[[ ${AMALTHEA_LINKING} == dynamic ]] && LIBEXT=so || LIBEXT=a
if [[ ${DC} != gdc ]]; then
    AMALTHEA_OPTS="-L-L${LPATH} -L-l:libamalthea-${DC}.${LIBEXT}"
elif [[ ${AMALTHEA_LINKING} == dynamic ]]; then
    AMALTHEA_OPTS="-lamalthea-${DC}"
elif [[ ${AMALTHEA_LINKING} == static ]]; then
    AMALTHEA_OPTS="${LPATH}/libamalthea-${DC}.a"
fi

LOCALES_CSV=resources/locales.csv
LOCALES_DTXT=source/locales.dtxt
${DC} ${BS}/localesconverter.script.d ${OUT}localesconverter
./localesconverter "${LOCALES_CSV}" "${LOCALES_DTXT}"
rm -f localesconverter localesconverter.o

mkdir -p ${BINPATH_BASEDIR}
ARGS="-Jsource/ ${STATIC_OPTS} ${AMALTHEA_OPTS}"
if [[ $BINVERSION == debug ]]; then
    ${DC} ${SRC} ${OUT}${BINPATH} ${ARGS} ${DEBUG} ${OTHER}
else
    ${DC} ${SRC} ${OUT}${BINPATH} ${ARGS} ${RELEASE} ${OTHER}
    objcopy --strip-debug --strip-unneeded ${BINPATH} ${BINPATH}
fi
pushd "${BINPATH_BASEDIR}"
chrpath -d ${APP}
chmod 755 ${APP}
popd

set +x

