#!/bin/bash
PROJECT=vitis-sl
APP=$PROJECT

ALL="changelog COPYING copyright Makefile README.md summary \
build-scripts/ help/ source/ testing/ resources/ doc/"

readonly VERSION=`cat source/version`

readonly BINPATH_BASEDIR=build/bin
BINPATH="${BINPATH_BASEDIR}/${APP}"

readonly SUMMARY=`cat summary`

if [[ -e /usr/bin/dpkg-architecture ]]; then
    DEB_HOST_MULTIARCH=`dpkg-architecture -qDEB_HOST_MULTIARCH`
fi

BS=build-scripts
