BS=build-scripts
PREFIX=/usr/local
DESTDIR=

DEFAULT_PLANG=D
PLANG=$(DEFAULT_PLANG)
MULTILINGUAL=FALSE
ifeq ($(MULTILINGUAL),TRUE)
	PLANG_DIRNAME=lang_$(PLANG)/
else
	PLANG_DIRNAME=
endif

ifeq ($(PLANG),D)
	DC=ldc2  # dmd, gdc or ldc2
	PHOBOS_LINKING=dynamic   # static or dynamic
	AMALTHEA_LINKING=dynamic # static or dynamic
	RELEASE_ARGS=release $(DC) $(PHOBOS_LINKING) $(AMALTHEA_LINKING)
	DEBUG_ARGS=debug $(DC) $(PHOBOS_LINKING) $(AMALTHEA_LINKING)
endif

debug:
	$(BS)/$(PLANG_DIRNAME)compile.sh $(DEBUG_ARGS)

bin:
	$(BS)/$(PLANG_DIRNAME)compile.sh $(RELEASE_ARGS)

NET_TESTS=off  # for checking network functional
test:
	$(BS)/run_tests.sh $(NET_TESTS)

doc: doc/req-ru/requirements.tex
	cd doc/req-ru/; pdflatex requirements.tex
	cd doc/req-ru/; pdflatex requirements.tex
	cd doc/req-ru/; rm -f *aux *log *out *toc

install:
	$(BS)/install.sh --install $(DESTDIR)$(PREFIX)

uninstall:
	$(BS)/install.sh --uninstall $(DESTDIR)$(PREFIX)

tarball: clean 
	$(BS)/build_tarball.sh

clean:
	rm -rf build/
	cd doc/req-ru; rm -f *.pdf *.log *.aux *.out *.toc
	rm -f source/locales.dtxt

