vitis-sl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

---

## vitis-sl

vitis-sl is a deprecated category manager for files.
It allows to move from a hierarchical organization of files to a semantic one.
Categories are stored as directories (in ~/Vitis by default).
These directories contain links to file on your file system.
The vitis-sl settings are stored in the file '~/.config/vitis-sl/vitis-sl.conf'.


See details:
```
$ vitis-sl --help
```

vitis-sl is a new name for vitis of versions **0.13.X**.
Versions **0.13.X** are last versions for vitis using
**s**ymbolic **l**inks for semantic file system.

New project [vitis 0.14+](https://gitlab.com/os-18/vitis)
breaks the backward compatibility.


:--

## Build from source

### Preparing

Before assembling, you need to install a compiler for D
(this project supports [ldc](https://github.com/ldc-developers/ldc/) only)
and [chrpath](https://directory.fsf.org/wiki/Chrpath).

For example, in Debian-based distributions,
you can install required packages as follows:

```
# apt install ldc chrpath
```

Similarly, in Fedora:

```
# dnf install ldc chrpath
```

This project is assembled with a dynamic linking (by default)
to the [Amalthea library](https://gitlab.com/os-18/amalthea).
So if you want to build this project from source, you also need
to build and install Amalthea. Then return to this instruction.


### Compilation and installation


Executable bin-file:

```
$ make
```


Installation (by default, to /usr/local/):

```
# make install
```

After that, the application is ready for use.

Uninstall:

```
# make uninstall
```

---

## Start

First, create your first category:

```
$ vitis-sl create Music
```

Now assign this category to a file:

```
$ vitis-sl assign Music -f "/home/$USER/Downloads/Josh Woodward - Swansong.ogg"
```

The quotation marks are used when the path to the file contains spaces.

Command 'show' can show you all the files from the category:

```
$ vitis-sl show Music
```

You can view the contents of the category with all the details about the files:

```
$ vitis-sl show Music --paths --details --categories
```

You can open files by default application<br>(for best result, it's recommended to install [ufo](https://bitbucket.org/vindexbit/ufo)):

```
$ vitis-sl open Music
```

If you want to eliminate the category from the file, use 'delete':

```
$ vitis-sl delete Music -f "Josh Woodward - Swansong.ogg"
```

The main advantages of this program are the ability to assign multiple categories to one file and use mathematical expressions with operations on sets to select files.

Example:

```
$ vitis-sl assign Music 2010s -f "/home/$USER/Downloads/Josh Woodward - Swansong.ogg"
```

```
$ vitis-sl open Music i: 2010s
```

*Note: **'i:'** is intersection*.

---

**vitis-sl** has a large number of different functions, see the details in the help:

```
$ vitis-sl --help
```

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
