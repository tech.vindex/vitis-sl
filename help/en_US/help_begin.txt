vitis-sl is a category manager for files.
It allows to move from a hierarchical organization of files to a semantic one.
Categories are stored as directories (in ~/vitis-sl by default).
These directories contain links to file on your file system.
The vitis-sl settings are stored in the file '~/.config/vitis-sl/vitis-sl.conf'.

--------------------------------------------------------------------------------
As first argument, vitis-sl takes one of following commands:
    assign    — category assigning for files and some other operations;
    copy      — copying files from categories to the designated place;
    create    — creation of new categories;
    delete    — deleting of categories and deleting of links from categories;
    open      — opening of files;
    run       — launching files as programs;
    service   — settings, service information, analysis of system data;
    show      — output lists of files and categories on the screen.
Additional information:
    --help    — displaying of this help;
    --version — displaying of version of the program.

For each command, you can add '--help' (or '-h') to display help about command.
Example: 'vitis-sl show --help'.

Also, the option '--conf=<path_to_configuration_file> is applied to each
command, which allows to use an alternative configuration file
at the specific time of the vitis-sl call. For details on the settings,
see help of the service command.

See the help of specific commands for more information:
    vitis-sl assign --help
    vitis-sl copy --help 
    vitis-sl create --help
    vitis-sl delete --help
    vitis-sl open --help
    vitis-sl run --help
    vitis-sl service --help
    vitis-sl show --help
