Общий формат для команды create:
    vitis-sl create [-c] <категория> ...
    Используется для создания новой категории (категорий),
    если категория вложенная, то указывается полный путь.
    Флаг '-c' необязателен.
