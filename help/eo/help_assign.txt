Formato por komando "assign":
    vitis-sl assign [[-c] <kategorio>] [-ago] <dateno> [[--yes][--no]]

Nenepran flagon "--yes" aŭ "--no" oni uzas por aŭtomata respondo
al eblaj petoj de la programo, ekzemple, al kreo de kategorio.

vitis-sl assign [-c] <kategorio> -a <pseŭdonimo> ...
    Por kategorio kreiĝas ĝia(j) pseŭdonimo(j).

vitis-sl assign [[-c] <kategorio> ... ] -f <dosiero> ... [--save] [--to <vojo>]
    Indikataj kategorioj doniĝas al indikataj dosieroj.
    Kategoria listo povas esti lasita, se aŭtokategorioj estas agordataj;
    tiam la dosieroj ricevas nur aŭtokategoriojn.
    
    Kroma flago "--save" igas dosierojn kopiiĝi en dosieran spacon
    (montru: vitis-sl service --help), ĉi tio estas grava, se aŭtokonservo
    ne estas agordata.
    Kroma flago "--to" donas eblon elekti dosierujon el listo de dosierspacoj
    kiel loko por kopiado.

vitis-sl assign [[-c] <kategorio> ...] -f <dosiero> ... [--save] [--to <vojo>] \
             [--start <starto>] [--finish <fino>] --fragname <nomo>
    Vi povas doni kategoriojn al fragmentoj de videaĵoj, aŭdioj kaj tekstoj,
    se "mediafragmenter" estas instalita.
    Por ĉi tiu celo oni uzas opciojn "--fragname", "--start" kaj "--finish".
    Flago "--fragname" estas nepra kaj indikas al nomo de
    kreanta fragmenta indiko.
    Por videaĵoj kaj videoaĵoj limoj de fragmentoj estas tempaj markoj
    je aranĝo "HH:MM:SS". Por tekstoj limo estas numero de linio kaj
    numero de signo de la linio, dividitaj per komo (sen blankoj).
    Ekzemploj:
        vitis-sl assign -f Filmo.mkv --start 02:03:29 --finish 02:03:56 \
                                  --fragname "Kulta sceno"
        vitis-sl assign -c Intro -f s01s01.avi --finish 00:00:59 \
                                            --fragname "Duck Tales intro"
        vitis-sl assign -f Notoj.txt --start 45,1 --finish 59,31 \
                                  --fragname "Citaĵo"
    Dum uzaso de la komando kreiĝas indikoj al dosierfragmentoj;
    reala tranĉo de fragmentoj ne okazas.
    Detalojn:
        mediafragmenter --help

vitis-sl assign -d <dosierujo> [--as <kategorio>] [-c <kategorio> ...] \
             [--save] [--to <vojo>]
    Enhavo de dosierujo ricevas kategorion nomigita per nomo de la dosierujo.
    Vi povas elekti alian nomo de nova kategorio per "--as".
    De enmetaj dosierujoj formiĝas lokaj subkategorioj.
    Priskribojn de opcioj "--save" kaj "--to" montru supre.
    Per opcio "-c" vi povas indiki kromajn kategoriojn, kiuj estas
    fiksataj por ĉiuj dosieroj el la dosierujo.

vitis-sl assign [-c] <kategorio> -e <esprimo>
    Indikata kategorio fiksiĝas al ĉiuj dosierujoj el dosieroj laŭ esprimo.
    Informon pro esprimoj montru en helpo de la komando "show":
        vitis-sl show --help

vitis-sl assign [-c] <kategorio> -v <[kategorio/]dosiero> [--number <numero>] \
             [[--start <starto>] [--finish <fino>] --fragname <nomo>]
    La komando donas kategoriojn al dosiero el vitis-sl-sistemo.
    Se kategorio ne estas indikita, la dosiero serĉiĝas
    en la tuta vitis-sl-sistemo.
    Ĉi tie ankaŭ eblas kreado de indikoj al dosierfragmentoj,
    detaloj estas supre.
    Por solvo de situacioj kiam kategorio havas kelkajn dosierojn kun
    ripetaj nomoj, oni uzas opcion "--number".
    Numeroj klariĝas per "vitis-sl show". Detaloj: vitis-sl show --help

vitis-sl assign [[-c] <kategorio> ...] -i <reta ligilo> ... [--save]
    Al reta ligilo doniĝas kategorio(j).
    Kategoria listo povas esti lasita, se aŭtokategoriigo estas agordita.
    Ekzemplo:
        vitis-sl assign GitLab -i https://gitlab.com/
    
    Opcio "--save" donas eblon konservi kopion de HTML-paĝo.
    Ĉi tiu opcio estas eksperimenta.

vitis-sl assign [-c] <kategorio> -n <nova nomo>
    Kategorio renomiĝas.
    Aŭtomataj kategorioj ne povas renomiĝi.

vitis-sl assign -v <[kategorio/]dosiero> -n <nova_nomo> [--number <numero>]
    Dosiero renomiĝas por ĉiuj kategorioj, kun kiuj ĝi estis ligata.
    Por plisimpligo de identigo vi povas indiki iun ligitan kun la dosiero
    kategorion. Se kategorio ne estas indikita, la dosiero serĉas en la tuta
    vitis-sl-sistemo.
    Por solvo de ripetaj nomoj de dosieroj, oni uzas flagon "--number".

vitis-sl assign -f <vojo/al/dosiero> -n <nova_nomo>
    Renomiĝas ĉiuj simbolaj ligiloj al indikita dosiero
    por ĉiuj kategorioj, kun kiuj la dosiero estas ligita.
    Oni indikas absolutan aŭ relativan vojon al la dosiero en dosiera sistemo.

vitis-sl assign [-c] <kategorio> -s <subkategorio> [[--local][--global]]
    Kategorio ekhavas subkategorion.
    Defaŭlte, kreiĝas malloka pseŭdonimo al kreata subkategorio:
    per tio subkategorio povas esti uzata kiel ordinara kategorio.
    Ekzemplo:
    vitis-sl assign C++ -s Boost --yes
        Kreiĝas subkategorio "C++/Boost" kaj pseŭdonimo-ligilo "Boost".
        Se "Boost" pli frue ne ekzistis, ĝi kreiĝas aŭtomate.
        Se kategorio "Boost" ekzistis, ĝia enhavo transloĝiĝas en "C++/Boost".
        Ĉi tiu komando estas ekvivalenta al:
            vitis-sl create C++/Boost
            vitis-sl assign C++/Boost -a Boost
    Se flago "--local" estas uzita anstataŭ "--global" (defaŭlta opcio),
    kategorio kreiĝas kiel loka.
    Ekzemplo:
    vitis-sl assign BigProject -s resources --local --yes
        Kreiĝas subkategorio "BigProject/resources".
        Ĉi tiu komando estas ekvivalenta al "vitis-sl create BigProject/resources".
