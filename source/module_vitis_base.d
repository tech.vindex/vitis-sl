/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021, 2025 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module module_vitis_base;
public alias moduleBase = module_vitis_base;

public import module_conf, module_output;
public import amalthea.fs,
              amalthea.langlocal,
              amalthea.net,
              amalthea.sys;
public import amalthea.dataprocessing : extractOptionValue, extractOptionRange;
public import std.array,
              std.algorithm,
              std.datetime,
              std.file,
              std.format,
              std.getopt,
              std.path,
              std.process,
              std.string,
              std.typecons;

import std.base64, std.regex;

import std.algorithm.mutation : stripRight;
import std.file : exists, isDir, isSymlink;

/*******************************************************************************
 * Valid commands for vitis.
 */
enum CommandEnum {
    e_show    = "show",
    e_open    = "open",
    e_run     = "run",
    e_create  = "create",
    e_assign  = "assign",
    e_delete  = "delete",
    e_service = "service",
    e_copy    = "copy",
    e_help    = "help",
};
alias CommandEnum CE;


/*******************************************************************************
 * Share directory
 */
immutable defaultShareDir = "/usr/share/help/";
static string shareDir = defaultShareDir;

string getShareDir() {
    return shareDir;
}
void setShareDir(string dirPath) {
    shareDir = dirPath;
}


/*******************************************************************************
 * The function returns path to main Vitis directory.
 */
string getVitisPath() {
    string vitisPath = VitisConf.getPath() ~ "/";
    if (vitisPath.exists && vitisPath.isFile) {
        auto msg = "(Error)"._s ~ " " ~ "file 'Vitis' blocks work."._s;
        throw new FileException(msg);
    } else if (!vitisPath.exists) {
        try {
            mkdirRecurse(vitisPath);
        } catch(std.file.FileException e) {
            auto err = vitisPath ~ ": this path is not available."._s;
            throw new FileWarning(err);
        }
    }
    return vitisPath;
}


/*******************************************************************************
 * The function returns path to directory with desktop-files
 * (for Internet links) from Vitis.
 */
string getLinkEntriesPath() {
    auto vitisDesktopFilesPath = getVitisPath() ~ "__vitis/link_entries/";
    if (!exists(vitisDesktopFilesPath)) {
        mkdirRecurse(vitisDesktopFilesPath);
    }
    return vitisDesktopFilesPath;
}
string getHTMLLinkEntriesPath() {
    string path = getLinkEntriesPath ~ "html/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string getNonHTMLLinkEntriesPath() {
    string path = getLinkEntriesPath ~ "non-html/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}


string getFragPointersPath() {
    auto vitisFragPointersPath = getVitisPath() ~ "__vitis/fragpointers/";
    if (!exists(vitisFragPointersPath)) {
        mkdirRecurse(vitisFragPointersPath);
    }
    return vitisFragPointersPath;
}


string getAutoCategoriesPath() {
    return getVitisPath() ~ "__auto/";
}

string networkBookmarksCategory = "NetworkBookmarks";

string getRootNetworkBookmarksCategoryPath() {
    string path = getAutoCategoriesPath() ~ "N/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string getNetworkBookmarksCategoryPath() {
    string path = getRootNetworkBookmarksCategoryPath()
                ~ networkBookmarksCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string fragPointersCategory() {
    return "Fragments"/*._s*/;
}

string getRootFragPointersCategoryPath() {
    string path = getAutoCategoriesPath() ~ "FP/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string getFragPointersCategoryPath() {
    string path = getRootFragPointersCategoryPath ~ fragPointersCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string getRootFormatCategoryPath() {
    string path = getAutoCategoriesPath() ~ "F/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string formatCategory() { return "Format"/*._s*/; }
string getFormatCategoryPath() {
    string path = getRootFormatCategoryPath() ~ formatCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string getRootTypeCategoryPath() {
    string path = getAutoCategoriesPath() ~ "T/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string typeCategory() { return "Type"/*._s*/; }
string getTypeCategoryPath() {
    string path = getRootTypeCategoryPath() ~ typeCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string getRootExtensionCategoryPath() {
    string path = getAutoCategoriesPath() ~ "E/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string extensionCategory() {
    return "Extension"/*._s*/;
}

string getExtensionCategoryPath() {
    string path = getRootExtensionCategoryPath() ~ extensionCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string[] getUsedAutoCategoriesPaths() {
    string[] modes = VitisConf.getAutocategorization().split(";").array;
    string[] paths;
    foreach(mode; modes) {
        if (mode == "none") break;
        if (mode.empty) continue;
        if (mode == "format") {
            paths ~= getFormatCategoryPath();
            paths ~= getTypeCategoryPath();
        } else if (mode == "extension") {
            paths ~= getExtensionCategoryPath();
        }
    }
    return paths;
}


bool isAutocategoryName(string name) {
    string topCategory = name.split('/')[0];
    if ("" == getPathIfAutocategory(topCategory)) {
        return false;
    }
    return true;
}


/*******************************************************************************
 * The function returns full path to directory of the automatic category.
 */
string getPathIfAutocategory(string category) {
    category = category.stripRight('/');
    string lowerCategory = category.toLower;

    if (VitisConf.getCaseSensitivity == "yes") {
        if (category == networkBookmarksCategory) {
            return getNetworkBookmarksCategoryPath;
        }
        if (category == fragPointersCategory) {
            return getFragPointersCategoryPath;
        }
    } else {
        if (lowerCategory == networkBookmarksCategory.toLower) {
            return getNetworkBookmarksCategoryPath;
        }
        if (lowerCategory == fragPointersCategory.toLower) {
            return getFragPointersCategoryPath;
        }
        {
            auto tempLang = amalthea.langlocal.getCurrentLanguage();
            scope(exit) amalthea.langlocal.chooseLanguage(tempLang);
            string[] languages = ["en_US", "ru_RU", "eo"];
            foreach(lang; languages) {
                amalthea.langlocal.chooseLanguage(lang);
                if (lowerCategory == formatCategory.toLower) {
                    return getFormatCategoryPath();
                }
                if (lowerCategory == typeCategory.toLower) {
                    return getTypeCategoryPath();
                }
                if (lowerCategory == extensionCategory.toLower) {
                    return getExtensionCategoryPath();
                }
            }
        }
    }

    string tryToFind(string autoRootDir) {
        string estimatedPath = autoRootDir ~ category;
        if (VitisConf.getCaseSensitivity == "yes") {
            if (estimatedPath.exists) {
                return estimatedPath;
            }
        } else {
            string[] dirsInAutoDir = getDirListRecurse(autoRootDir);
            foreach(p; dirsInAutoDir) {
                if (estimatedPath.toLower == p.toLower) {
                    return p;
                }
            }
        }
        return "";
    }

    string[] autoCatMode = VitisConf.getAC().split(';').array;
    string path;
    string autoDir;
    if (canFind(autoCatMode, "format")) {
        //autocategory by format
        autoDir = getRootFormatCategoryPath();
        path = tryToFind(autoDir);
        if (path != "") return path;
        //autocategory by type
        autoDir = getRootTypeCategoryPath();
        path = tryToFind(autoDir);
        if (path != "") return path;
    } else if (canFind(autoCatMode, "extension")) {
        //autocategory by extension
        autoDir = getRootExtensionCategoryPath();
        path = tryToFind(autoDir);
        if (path != "") return path;
    }

    return "";
}


/*******************************************************************************
 * The function returns full path to directory of the category.
 */
string getCatDirPath(string category) {
    category = category.stripRight('/');
    string estimatedPath = getVitisPath ~ category ~ '/';
    if (exists(estimatedPath)) {
        return estimatedPath;
    }
    if (VitisConf.getCaseSensitivity == "no") {
        string lowerCategory = category.toLower;
        string[] allPathsToCategories
            = getDirListRecurse(getVitisPath)
                .filter!(a => !a.startsWith(getVitisPath ~ "__"))
                .array;
        foreach(p; allPathsToCategories) {
            if (lowerCategory == baseName(p).toLower) {
                return p ~ '/';
            }
        }
    }
    return getPathIfAutocategory(category);
}
deprecated alias getCategoryPath = getCatDirPath;


/*******************************************************************************
 * The function checks whether the argument is subcategory (of other category)
 * with global scope.
 */
bool isGlobalSubCategory(string thing) {
    thing = std.algorithm.strip(thing, '/');
    if (!thing.canFind("/")) { //only: cat/subcat
        return false;
    }
    auto fullPath = getCatDirPath(thing).stripRight('/');
    auto globPath = getCatDirPath(thing.baseName).stripRight('/');
    if (fullPath.empty || globPath.empty) return false;
    if (globPath.isSymlink && globPath.readLink.stripRight('/') == fullPath) {
        return true;
    }
    return false;
}


void changeSymlink(string link, string newPath) {
    std.file.remove(link);
    symlink(newPath, link);
}


string[] getTopLevelCategories(Flag!"fullPath" fullPath = Yes.fullPath) {
    auto categories = getTopLevelCategoriesAndAliases()
            .filter!(a => !a.isSymlink)
            .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; categories) {
            cat = cat[l .. $];
        }
    }
    return categories;
}


string[] getUserTopLevelCategories(Flag!"fullPath" fullPath = Yes.fullPath) {
    string[] usercategories = getDirList(getVitisPath)
        .filter!(a => !a.canFind("/__") && a.isDir)
        .filter!(a => !a.isSymlink)
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; usercategories) {
            cat = cat[l .. $];
        }
    }
    return usercategories;
}


string[] getTopLevelCategoriesAndAliases(
    Flag!"fullPath" fullPath = Yes.fullPath
) {
    string[] usercategories = getDirList(getVitisPath)
        .filter!(a => !a.canFind("/__") && a.isDir)
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; usercategories) {
            cat = cat[l .. $];
        }
    }
    string[] autocategories = getTopLevelAutoCategories(fullPath);
    return usercategories ~ autocategories;
}

// without autocategories and without aliases
string[] getNoAutoCategories(Flag!"fullPath" fullPath = No.fullPath) {
    auto categories = getDirListRecurse(getVitisPath)
        .filter!(a => !a.baseName.startsWith("__"))
        .filter!(a => !a.dirName.startsWith("__"))
        .filter!(a => !a.isSymlink)
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; categories) {
            cat = cat[l .. $];
        }
    }
    return categories;
}

string[] getAllCategoriesAndAliases(Flag!"fullPath" fullPath = No.fullPath) {
    //exclude unnecessary catalogies for repeared files and service catalogies
    auto categories = getDirListRecurse(getVitisPath)
        .filter!(a => !a.baseName.startsWith("__"))
        .filter!(a => !a.dirName.startsWith("__"))
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; categories) {
            cat = cat[l .. $];
        }
    }
    return categories ~ getAllAutoCategories(fullPath);
}


string[] getAliases(Flag!"fullPath" fullPath = No.fullPath) {
    //exclude unnecessary catalogies for repeared files and service catalogies
    auto categories = getDirListRecurse(getVitisPath)
        .filter!(a => !a.baseName.startsWith("__"))
        .filter!(a => !a.dirName.startsWith("__"))
        .filter!(a => a.isSymlink)
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; categories) {
            cat = cat[l .. $];
        }
    }
    return categories;
}


string[] getTopLevelAutoCategories(Flag!"fullPath" fullPath = No.fullPath) {
    if (!getAutoCategoriesPath().exists) return null;
    auto autoCategories = getDirList(getAutoCategoriesPath);
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; autoCategories) {
            cat = cat[l .. $];
        }
    }
    return autoCategories;
}

string[] getAllAutoCategories(Flag!"fullPath" fullPath = Yes.fullPath) {
    string[] autoCategories;
    void subfn(string autoDirPath) {
        if (!autoDirPath.exists) return;
        autoCategories ~= autoDirPath;
        string[] categories = getDirListRecurse(autoDirPath)
            .filter!(a => !a.baseName.startsWith("__"))
            .filter!(a => !a.dirName.startsWith("__"))
            .array;
        autoCategories ~= categories;
    }
    subfn(getFormatCategoryPath);
    subfn(getTypeCategoryPath);
    subfn(getExtensionCategoryPath);
    if (fullPath) {
        return autoCategories;
    }
    size_t l = getAutoCategoriesPath().length;
    foreach(ref cat; autoCategories) {
        cat = cat[l+2 .. $]; //remove parts of paths 'F/', 'E/', 'T/'
    }
    return autoCategories;
}


/*******************************************************************************
 * The function returns the real name of the category by its alias
 */
string getCategoryByAlias(string catAlias)
in {
    assert(getCatDirPath(catAlias).stripRight('/').isSymlink);
}
do {
    string aliasPath = getCatDirPath(catAlias).stripRight('/');
    string categoryPath = readLink(aliasPath);
    return categoryPath[getVitisPath.length .. $];
}


/*******************************************************************************
 * Returns opening application depending on the desktop environment
 */
deprecated string chooseOpenerByDefault() {
    string vitisDefaultTool = "ufo";
    if (amalthea.sys.getAppPath(vitisDefaultTool)) {
        return vitisDefaultTool;
    }
    return "";
}

bool stringIsUint(string value) {
    import std.uni : isNumber;
    foreach(c; value) if (!(isNumber(to!dchar(c)))) return false;
    return true;
}

/*******************************************************************************
 * Function returns chopped (rarefied) array
 * by numbers like "1-4,6,8-11,9" or simple number
 */
string[] getRarefiedArrayByNumbers(string[] arr, string numbers) {
    import std.regex;
    string[] resultArray;
    resultArray.length = arr.length;
    if (stringIsUint(numbers)) {
        ssize_t n = to!ssize_t(numbers)-1;
        if (n >= 0 && n < resultArray.length) resultArray[n] = arr[n];
        return resultArray;
    }

    //for valid expression, for example: [1-5,7,12,14-19]
    auto r = ctRegex!(r"^\[[0-9]+(-[0-9]+)?(,+[0-9]+(-[0-9]+)?)*\]$");
    numbers = std.string.replace(numbers, ",", ",,");
    if (!numbers.startsWith("[") && !numbers.endsWith("]")) {
        numbers = "[" ~ numbers ~ "]"; //legacy
    }
    auto c = numbers.matchFirst(r);
    if (0 != c.length) {
        immutable
            strRegExpForRanges = `((?P<index1>[0-9]+)-(?P<index2>[0-9]+))`,
            strRegExpForSimpleNumber = `([,\[](?P<index>[0-9]+)[,\]])`;
        auto sharedRegExp
            = ctRegex!(strRegExpForRanges ~ "|" ~ strRegExpForSimpleNumber);

        foreach (el; matchAll(c[0], sharedRegExp)) {
            try {
                for (auto i = el["index1"].to!size_t-1;
                     i < el["index2"].to!size_t;
                     ++i) {
                    if (i >= 0 && i < resultArray.length)
                        resultArray[i] = arr[i];
                }
            }
            //if el["index1"] is invalid
            catch(std.conv.ConvException e) {
                auto index = el["index"].to!size_t-1;
                resultArray[index] = arr[index];
            }
        }
    } else {
        throw new ValidException("Incorrect use of option '-n'."._s);
    }
    return resultArray;
}


/*******************************************************************************
 * The function checks whether the word is an option (flag)
 */
bool isOption(string line) {
    return line.startsWith("-");
}


string[string] getSymlinksInfo(string catalog,
                               bool showHiddenData = true,
                               bool brokenLinks = false,
                               bool recursively = true) {
    catalog = catalog.stripRight('/');
    string[string] allLinks;
    string[] filelist, dirlist;

    auto files = dirEntries(catalog, SpanMode.shallow);

    if (brokenLinks) {
        foreach(f; files) {
            if (f.isSymlink && (!f.readLink.exists || f.isFile)) {
                filelist ~= baseName(f.name);
            } else if (f.isDir) {
                dirlist ~= baseName(f.name);
            }
        }
    } else {
        foreach(f; files) {
            if (f.isSymlink && !f.readLink.exists) {
                continue;
            } else if (f.isSymlink && f.isFile) {
                filelist ~= baseName(f.name);
            } else if (f.isDir) {
                dirlist ~= baseName(f.name);
            }
        }
    }

    if (showHiddenData == false) {
        filelist = filelist.filter!(a => !a.startsWith(".")).array;
        dirlist  =  dirlist.filter!(a => !a.startsWith(".")).array;
    }
    foreach(string link; filelist) {
        allLinks[absolutePath(catalog)~"/"~link] = readLink(catalog~'/'~link);
    }
    if (recursively)
        foreach(string d; dirlist) {
            string[string] temp = getSymlinksInfo(catalog~'/'~d,
                                                  showHiddenData,
                                                  brokenLinks,
                                                  recursively);
            foreach(pathToLink, pathToFile; temp) {
                allLinks[pathToLink] = pathToFile;
            }
        }
    return allLinks;
}


string[string] getLinksFromCategory(string category) {
    auto dir = getCatDirPath(category);
    return getSymlinksInfo(dir);
}


/*******************************************************************************
 * The function returns an associative array with full paths to links (as keys)
 * and file paths (as values) from the Vitis
 */
string[string] getAllLinks() {
    string[] allTopCategories = getTopLevelCategories();
    string[string] allLinks;
    foreach(category; allTopCategories) {
        auto temp = getSymlinksInfo(category); //recursively
        foreach(k, v; temp) allLinks[k] = v;
    }
    return allLinks;
}

string[string] getAllNoAutoLinks() {
    string[] allTopCategories = getUserTopLevelCategories();
    string[string] allLinks;
    foreach(category; allTopCategories) {
        auto temp = getSymlinksInfo(category); //recursively
        foreach(k, v; temp) allLinks[k] = v;
    }
    return allLinks;
}

string[string] getAllLinksFromCache() {
    static bool firstFunctionCall = true;

    static string[string] allLinks;
    if (!firstFunctionCall) return allLinks;

    string[] allTopCategories = getTopLevelCategories();
    foreach(category; allTopCategories) {
        auto temp = getSymlinksInfo(category); //recursively
        foreach(k, v; temp) allLinks[k] = v;
    }
    firstFunctionCall = false;
    return allLinks;
}


deprecated string[] findFilesInVitis(string file) {
    string[] categories = getTopLevelCategories(No.fullPath);
    string[] filepaths;
    foreach(c; categories) {
        string vitisFile = c ~ "/" ~ file;
        writeln("vitisFile = ", vitisFile);
        try filepaths ~= findFileInVitis(vitisFile)[0];
        catch (Exception e) {writeln(e.msg);}
    }
    return filepaths;
}

auto findFileInVitis(string vitisFile,
                     size_t number = 0) {
    string finalFilepath;
    string finalLinkpath;
    string category = dirName(vitisFile).stripRight('/');
    enforce(!category.empty);
    string linkName = baseName(vitisFile);

    string[string] files;
    files = getFileListByExpression([category], Yes.fullLinkPath);
    size_t counter;
    string filepath, linkpath;
    foreach(f, l; files) {
        if (l.baseName == linkName) {
            counter++;
            filepath = f;
            linkpath = l;
        }
    }
    if (0 == counter) {
        auto msg = category~"/"~linkName~": file in Vitis is not found."._s;
        throw new FileWarning(msg);
    } else if (1 == counter) {
        if (number != 0 && number != 1) {
            auto msg = "File with this number not found."._s;
            throw new FileWarning(msg);
        }
        finalFilepath = filepath;
        finalLinkpath = linkpath;
    } else {
        if (number == 0) {
            auto msg = (s_("Specify the number of the required file " ~
                        "using the '--number' flag."));
            throw new FileWarning(msg);
        }
        size_t[string] numbersOfRepeatedFiles =
            groupByFilePathWithNumbers(files);
        foreach(lpath, n; numbersOfRepeatedFiles) {
            if (n == number && lpath.baseName == linkName) {
                finalFilepath = lpath.readLink;
                finalLinkpath = lpath;
                break;
            }
        }
        if (finalFilepath.empty) {
            auto msg = "File with this number not found."._s;
            throw new FileWarning(msg);
        }
    }
    Tuple!(string, "filepath", string, "linkpath") result;
    result.filepath = finalFilepath;
    result.linkpath = finalLinkpath;
    return result;
}


/*******************************************************************************
 * The function returns link list (keys are linked files, values are links)
 */
string[string] getFileListByExpression(
                    string[] expression,
                    Flag!"fullLinkPath" fullLinkPath = No.fullLinkPath) {
    //remove unwanted slashes
    for (size_t i; i < expression.length; i++) {
        expression[i] = expression[i].strip('/');
    }
    import module_RPN;
    string[string] rawfiles = module_RPN.calculateExpression(expression);
    string[string] files;

    if (fullLinkPath) {
        foreach(fpath, lpath; rawfiles) {
            files[fpath] = lpath; //copying?
        }
    } else {
        foreach(fpath, lpath; rawfiles) {
            files[fpath] = baseName(lpath);
        }
    }
    return files;
}


size_t[string] groupByFilePathWithNumbers(string[string] filesWithLinks) {
    string[] files, links;
    size_t[] numbers;
    size_t[string] numbersOfRepeatedNames;
    orderAssociativeArrayByKeys(filesWithLinks, files, links);
    numbers.length = links.length;

    size_t[string] repeatedNamesCounter;
    foreach(i; 0 .. numbers.length) {
        auto lpath = links[i];
        repeatedNamesCounter[lpath.baseName]++;
    }
    foreach(name, n; repeatedNamesCounter) {
        size_t internalCounter;
        foreach(i, fpath; files) {
            auto lpath = links[i];
            if (lpath.baseName == name) {
                if (n == 1) {
                    numbersOfRepeatedNames[lpath] = 0;
                    break;
                } else {
                    internalCounter++;
                    numbersOfRepeatedNames[lpath] = internalCounter;
                }
            }
        }
    }
    return numbersOfRepeatedNames;
}


deprecated size_t getNumberOfRepeatedFile(string linkPath) {
    string[] pathParts = linkPath.split('/').array;
    if (pathParts[$-3] == "__repeated_names") {
        return pathParts[$-2].to!size_t;
    }
    return 1;
}


/*******************************************************************************
 * Decorative border for use in user interface
 */
string getBorder() {
    char[] arr;
    arr.length = 80;
    arr[] = '-';
    string border = to!string(arr) ~ "\n";
    return border;
}


/*******************************************************************************
 * Interactive element of UI: getting an answer (yes/no) to a question
 */
bool getAnswerToTheQuestion(string question = "") {
    "simple".tprint(question);
    string ans = readln;
    ans = std.string.strip(ans).toLower;
    if (ans.among("y"._s, "yes"._s, "y", "yes")) {
        return true;
    }
    return false;
}


/*******************************************************************************
 * The function is used in the beginning of unit test,
 * denotes the lower bound of the test.
 */
void utBegin(string place) {
    writeln("========================================");
    writeln(">: unittesting in ", place, "\n");
}


/*******************************************************************************
 * The function is used in the end of unit test,
 * denotes the upper bound of the test.
 */
void utEnd(string place) {
    writeln("\n>: end of unittesting in ", place);
    writeln("========================================\n");
}


/*******************************************************************************
 * The function checks condition and throws exception if the condition is false
 */
void checkCondition(alias pred)(bool thing,
                                string file = __FILE__,
                                size_t line = __LINE__)
  if (is(typeof(pred) == string))
{
    if (!thing) {
        //stderr.writeln(file, ": ", line);
        throw new ValidException(pred, file, line);
    }
}


/*******************************************************************************
 * The function deletes duplicate slashes in a string.
 */
void removeDuplicateSlashes(ref string s) {
    while(s.canFind("//")) {
        s = s.replace("//", "/");
    }
}
unittest {
    auto line = "/home/user//Documents///file.odt";
    line.removeDuplicateSlashes();
    assert(line == "/home/user/Documents/file.odt");
}


string[] getDirList(string catalog,
                    bool baseNames = false,
                    bool hidden = true,
                    bool recurse = false) {
    string absoluteCatalog = getAbsolutePath(catalog);
    string[] dirs = std.file.dirEntries(absoluteCatalog, SpanMode.shallow)
        .filter!(a => a.isDir)
        .map!(a => a.name)
        .array;
    if (false == hidden) {
         dirs = dirs.filter!(a => !baseName(a).startsWith(".")).array;
    }
    if (baseNames) dirs = dirs.map!(a => std.path.baseName(a)).array;
    if (recurse) {
        string[] additDirs;
        foreach(dir; dirs) {
            string[] newAdditDirs;

            newAdditDirs = getDirList(
                baseNames ? catalog~"/"~dir : dir,
                baseNames,
                hidden,
                recurse
            );
            if (baseNames) foreach(ref additDir; newAdditDirs) {
                additDir =  dir ~ "/" ~ additDir;
            }
            additDirs ~= newAdditDirs;
        }
        dirs ~= additDirs;
    }
    return dirs;
}


string[] getDirListRecurse(string catalog,
                           bool baseNames = false,
                           bool hidden = true) {
    return getDirList(catalog, baseNames, hidden, true);
}


string[] getFileList(string catalog,
                     bool baseNames = false,
                     bool hidden = true,
                     bool recursive = false) {
    catalog = getAbsolutePath(catalog);
    string[] filelist, dirlist;
    filelist = std.file.dirEntries(catalog, SpanMode.shallow)
        .filter!(a => a.isFile || a.isSymlink)
        .map!(a => a.name)
        .array;
    dirlist = std.file.dirEntries(catalog, SpanMode.shallow)
        .filter!(a => a.isDir)
        .map!(a => a.name)
        .array;
    if (false == hidden) {
        filelist = filelist.filter!(a => !baseName(a).startsWith(".")).array;
        dirlist  =  dirlist.filter!(a => !baseName(a).startsWith(".")).array;
    }
    if (recursive) foreach(string d; dirlist) {
        string[] temp = getFileList(d, false, hidden, true);
        filelist ~= temp;
    }
    if (baseNames) foreach(ref f; filelist) {
        f = f.baseName;
    }
    return filelist;
}

string createTempFile() {
    string tempfile;
    do {
        tempfile = std.file.tempDir ~ "/tempfile"
                 ~ Clock.currTime.toISOExtString();
    } while(std.file.exists(tempfile));
    std.file.write(tempfile, "");
    return tempfile;
}


string getContentTypeByURL(in char[] url) {
    import std.net.curl : HTTP, options;
    auto http = HTTP(url);
    http.verifyPeer = false;
    http.perform;
    auto header = http.responseHeaders;
    string contentType;
    foreach(key, value; header) {
        if (key.toLower == "content-type") {
            contentType = value;
            break;
        }
    }
    return contentType;
}


bool isHTML(string url) {
    string contentType;
    try {
        contentType = getContentTypeByURL(url);
        if (contentType.canFind("text/html")) {
            return true;
        }
    } catch(Exception e) {
        stderr.writeln(e.msg);
        return false;
    }
    return false;
}


alias jv = amalthea;

/*******************************************************************************
 * Function for saving HTML-pages
 * with resources and conversion of links,
 * returns title of HTML-page.
 * Cut as deprecated function from the Amalthea library.
 */
string savePage(string url, string targetDir = ".", bool monolithic = true) {
    if (!isHTML(url)) {
        throw new Exception("This URL does not lead to HTML-page.");
    }
    auto title = url.getHTMLPageTitle.replace("/", "|");
    auto pageDir = targetDir.stripRight('/') ~ "/" ~ title;
    auto contentDir = pageDir ~ "/content/";
    auto baseAddress = getBaseAddressFromURL(url);
    if (!pageDir.exists) mkdirRecurse(pageDir);
    if (!monolithic && !contentDir.exists) mkdir(contentDir);
    string page = "<!-- " ~ url ~ " -->\n";
    page ~= jv.net.getPage(url).toString;

    replaceEncodingLabelToUnicode(page);
    auto srcValues = page.matchAll(ctRegex!(`[sS][rR][cC]="([^"]*)`));
    auto hrefValues = page.matchAll(ctRegex!(`[hH][rR][eE][fF]="([^"]*)`));
    string[] resourceAddresses;
    foreach(el; srcValues)  resourceAddresses ~= el[1];
    foreach(el; hrefValues) resourceAddresses ~= el[1];

    auto cwd = getcwd();
    cd(pageDir);
    foreach(addr; resourceAddresses) {
        if (handleIfPicture(page, url, addr, monolithic))    continue;
        if (handleIfSourceCode(page, url, addr, monolithic)) continue;
        //if (addr.startsWith("//")) page = page.replace(addr, addr[2 .. $]);
    }
    cd(cwd);
    std.file.write(pageDir ~ "/index.html", page);
    return title;
}


private auto getBaseAddressFromURL(string url) {
    auto lastSlashIndex = url.lastIndexOf('/');
    string filename = url[lastSlashIndex+1 .. $];
    string baseAddress = url[0 .. lastSlashIndex+1];
    return baseAddress;
}


private ref string replaceEncodingLabelToUnicode(return ref string html) {
    foreach(label; encodingLabels) {
        html = html.replace(`charset="`~label~`"`, `charset=UTF-8`)
                   .replace(`charset='`~label~`'`, `charset=UTF-8`)
                   .replace(`charset=`~label~``,   `charset=UTF-8`);
    }
    return html;
}


private immutable picExtensions = [
    ".bmp", ".gif", ".jp2", ".jpg", ".jpeg", ".png", ".tif", ".tiff"
];


private immutable srcExtensions = [".css", ".js"];


private bool handleIfPicture(ref string page,
                             string pageURL,
                             string resourceAddr,
                             bool monolithic) {
    alias url = pageURL;
    alias addr = resourceAddr;
    bool urlIsPicture;
    auto lowerAddr = addr.toLower;
    foreach(ext; picExtensions) {
        if (lowerAddr.endsWith(ext) || lowerAddr.indexOf(ext~"?") != -1) {
            urlIsPicture = true;
            break;
        }
    }
    if (!urlIsPicture) return false;
    auto fsaddress = "content/" ~ getFileNameFromURL(addr);
    string picURL;
    if (addr.startsWith("//")) {
        picURL = addr[2 .. $];
    } else if (addr.startsWith("http")) {
        picURL = addr;
    } else {
        picURL = getSiteAddressFromURL(url) ~ addr;
    }
    ubyte[] pic;
    try {
        pic = jv.net.getRaw(picURL);
    } catch (StdNetException e) {
        debug stderr.writeln("Warning: ", e.msg, ": ", picURL);
        return true;
    }
    if (!monolithic) {
        std.file.write(fsaddress, pic);
        page = page.replace(addr, "./"~fsaddress);
    } else {
        string encoded = Base64.encode(pic);
        string extension = addr.split('.')[$-1].split('?')[0];
        string source = "data:image/" ~ extension ~ ";base64," ~ encoded;
        page = page.replace(addr, source);
    }
    return true;
}


private bool handleIfSourceCode(ref string page,
                                string pageURL,
                                string resourceAddr,
                                bool monolithic) {
    alias url = pageURL;
    alias addr = resourceAddr;
    bool urlIsJS;
    bool urlIsCSS;
    auto lowerAddr = addr.toLower;
    if (lowerAddr.endsWith(".js"))             urlIsJS = true;
    else if (-1 != lowerAddr.indexOf(".js?"))  urlIsJS = true;
    else if (lowerAddr.endsWith(".css"))       urlIsCSS = true;
    else if (-1 != lowerAddr.indexOf(".css?")) urlIsCSS = true;

    bool urlIsSourceCode = urlIsJS || urlIsCSS;
    if (!urlIsSourceCode) return false;
    auto fsaddress = "content/" ~ getFileNameFromURL(addr);
    string resURL;
    if (addr.startsWith("//")) {
        resURL = addr[2 .. $];
    } else if (addr.startsWith("http")) {
        resURL = addr;
    } else {
        resURL = getSiteAddressFromURL(url) ~ addr;
    }
    string resource;
    try {
        resource = jv.net.getPage(resURL).toString;
    } catch (StdNetException e) {
        debug stderr.writeln("Warning: ", e.msg, ": ", resURL);
        return true;
    }

    if (!monolithic) {
        std.file.write(fsaddress, resource);
        page = page.replace(addr, "./"~fsaddress);
    } else {
        ssize_t index = indexOf(page, addr);
        if (urlIsJS) {
            resource = handleClosedTagCases(resource);
            index = index + page[index .. $].indexOf("</script>");
            index = index + page[index .. $].indexOf(">") + 1;
            resource =
                "\n<!-- begin "~addr~"--><script type='text/javascript'>\n"
                ~ resource ~
                "\n</script><!-- end -->\n";
        }
        else
            index = index + page[index .. $].indexOf(">") + 1;
        if (urlIsCSS) {
            resource =
                "\n" ~
                `<style type="text/css">`
                ~ "\n" ~ resource ~ "\n</style>\n";
        }

        page = page[0 .. index] ~ resource ~ page[index .. $];
        page = page.replace(`"`~addr~`"`, `""`);
        page = page.replace(`'`~addr~`'`, `""`);
        page = page.replace(`=`~addr, `=""`);
    }
    return true;
}


private auto getSiteAddressFromURL(string url) {
    auto temp = url.findSplitAfter("//");
    string protocol = temp[0];
    string address = temp[1];
    auto firstSlashIndex = address.indexOf('/');
    string site;
    if (firstSlashIndex == -1) {
        site = address;
    } else {
        site = address[0 .. firstSlashIndex+1];
    }
    return site;
}

private auto getFileNameFromURL(string url) {
    auto temp = url.findSplitAfter("//");
    string protocol = temp[0];
    string address = temp[1];
    auto lastSlashIndex = address.lastIndexOf('/');
    string filename = address[lastSlashIndex+1 .. $];
    if (filename.empty) {
        filename = "index.html";
        url ~= filename;
    }
    filename = filename.split("?")[0];
    filename = filename.split("#")[0];
    return filename;
}


private string handleClosedTagCases(string resource) {
    string result = resource.idup;
    char q = '"';
    auto index = result.indexOf("</");
    while(index != -1) {
        foreach_reverse(i, ch; result[0 .. index]) {
            char prev_ch = result[i-1];
            if (ch == '"') {
                q = '"';
                break;
            } else if (ch == '\'') {
                q = '\'';
                break;
            }
        }
        q = '\''; //temporaly
        result = result.replace(`</`, `<`~q~`+`~q~`/`);
        index = result.indexOf("</");
    }
    return result;
}



//source: https://encoding.spec.whatwg.org/
immutable string[] encodingLabels = [
/*UTF-8*/          "unicode-1-1-utf-8", "utf-8", "utf8",
/*IBM866*/         "866", "cp866", "csibm866", "ibm866",
/*ISO-8859-2*/     "csisolatin2", "iso-8859-2", "iso-ir-101", "iso8859-2",
                   "iso88592", "iso_8859-2", "iso_8859-2:1987", "l2", "latin2",
/*ISO-8859-3*/     "csisolatin3", "iso-8859-3", "iso-ir-109", "iso8859-3",
                   "iso88593", "iso_8859-3", "iso_8859-3:1988", "l3", "latin3",
/*ISO-8859-4*/     "csisolatin4", "iso-8859-4", "iso-ir-110", "iso8859-4",
                   "iso88594", "iso_8859-4", "iso_8859-4:1988", "l4", "latin4",
/*ISO-8859-5*/     "csisolatincyrillic", "cyrillic", "iso-8859-5", "iso-ir-144",
                   "iso8859-5", "iso88595", "iso_8859-5", "iso_8859-5:1988",
/*ISO-8859-6*/     "arabic", "asmo-708", "csiso88596e", "csiso88596i",
                   "csisolatinarabic", "ecma-114", "iso-8859-6", "iso-8859-6-e",
                   "iso-8859-6-i", "iso-ir-127", "iso8859-6", "iso88596",
                   "iso_8859-6", "iso_8859-6:1987",
/*ISO-8859-7*/     "csisolatingreek", "ecma-118", "elot_928", "greek", "greek8",
                   "iso-8859-7", "iso-ir-126", "iso8859-7", "iso88597",
                   "iso_8859-7", "iso_8859-7:1987", "sun_eu_greek",
/*ISO-8859-8*/     "csiso88598e", "csisolatinhebrew", "hebrew", "iso-8859-8",
                   "iso-8859-8-e", "iso-ir-138", "iso8859-8", "iso88598",
                   "iso_8859-8", "iso_8859-8:1988", "visual",
/*ISO-8859-8-I*/   "csiso88598i", "iso-8859-8-i", "logical",
/*ISO-8859-10*/    "csisolatin6", "iso-8859-10", "iso-ir-157", "iso8859-10",
                   "iso885910", "l6", "latin6",
/*ISO-8859-13*/    "iso-8859-13", "iso8859-13", "iso885913",
/*ISO-8859-14*/    "iso-8859-14", "iso8859-14", "iso885914",
/*ISO-8859-15*/    "csisolatin9", "iso-8859-15", "iso8859-15", "iso885915",
                   "iso_8859-15", "l9",
/*ISO-8859-16*/    "iso-8859-16",
/*KOI8-R*/         "cskoi8r", "koi", "koi8", "koi8-r", "koi8_r",
/*KOI8-U*/         "koi8-ru", "koi8-u",
/*macintosh*/      "csmacintosh", "mac", "macintosh", "x-mac-roman",
/*windows-874*/    "dos-874", "iso-8859-11", "iso8859-11", "iso885911",
                   "tis-620", "windows-874",
/*windows-1250*/   "cp1250", "windows-1250", "x-cp1250",
/*windows-1251*/   "cp1251", "windows-1251", "x-cp1251",
/*windows-1252*/   "ansi_x3.4-1968", "ascii", "cp1252", "cp819", "csisolatin1",
                   "ibm819", "iso-8859-1", "iso-ir-100", "iso8859-1",
                   "iso88591", "iso_8859-1", "iso_8859-1:1987", "l1", "latin1",
                   "us-ascii", "windows-1252", "x-cp1252",
/*windows-1253*/   "cp1253", "windows-1253", "x-cp1253",
/*windows-1254*/   "cp1254", "csisolatin5", "iso-8859-9", "iso-ir-148",
                   "iso8859-9", "iso88599", "iso_8859-9", "iso_8859-9:1989",
                   "l5", "latin5", "windows-1254", "x-cp1254",
/*windows-1255*/   "cp1255", "windows-1255", "x-cp1255",
/*windows-1256*/   "cp1256", "windows-1256", "x-cp1256",
/*windows-1257*/   "cp1257", "windows-1257", "x-cp1257",
/*windows-1258*/   "cp1258", "windows-1258", "x-cp1258",
/*x-mac-cyrillic*/ "x-mac-cyrillic", "x-mac-ukrainian",
/*GBK*/            "chinese", "csgb2312", "csiso58gb231280", "gb2312", "gb_2312",
                   "gb_2312-80", "gbk", "iso-ir-58", "x-gbk",
/*gb18030*/        "gb18030",
/*Big5*/           "big5", "big5-hkscs", "cn-big5", "csbig5", "x-x-big5",
/*EUC-JP*/         "cseucpkdfmtjapanese", "euc-jp", "x-euc-jp",
/*ISO-2022-JP*/    "csiso2022jp", "iso-2022-jp",
/*Shift_JIS*/      "csshiftjis", "ms932", "ms_kanji", "shift-jis", "shift_jis",
                   "sjis", "windows-31j", "x-sjis",
/*EUC-KR*/         "cseuckr", "csksc56011987", "euc-kr", "iso-ir-149", "korean",
                   "ks_c_5601-1987", "ks_c_5601-1989", "ksc5601", "ksc_5601",
                   "windows-949",
/*replacement*/    "csiso2022kr", "hz-gb-2312", "iso-2022-cn",
                   "iso-2022-cn-ext", "iso-2022-kr", "replacement",
/*UTF-16BE*/       "utf-16be",
/*UTF-16LE*/       "utf-16", "utf-16le",
/*x-user-defined*/ "x-user-defined"
];



/*******************************************************************************
 * The function converts an expression of the form 'cat1/cat2'
 * to an array of the form ["cat1", "i:", "cat2"].
 */
deprecated string[] convertExpressionWithSlash(string expression) {
    removeDuplicateSlashes(expression);
    expression = expression.replace("/", "/i:/");
    string[] arr = expression.split("/");
    return arr;
}
unittest {
    auto query = "Music/Foreign/80s";
    auto alteredQuery = ["Music", "i:", "Foreign", "i:", "80s"];
    assert(convertExpressionWithSlash(query) == alteredQuery);
}


/// Template for exceptions
private mixin template RealizeException() {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}
/// Exception related to validation.
class ValidException : Exception { mixin RealizeException; }
/// Exception for file system problems.
class FileWarning : Exception { mixin RealizeException; }
/// Exception for file space problems.
class FileSpaceException : Exception { mixin RealizeException; }
/// Exception for nonexistent programs.
class AppNotFound : Exception { mixin RealizeException; }

/*******************************************************************************
 * The function prints error message.
 */
void printErrorMessage(string msg) {
    "error".tprint("(Error)"._s);
    stderr.writeln(" ", msg);
}
deprecated alias exceptionHandler = printErrorMessage;


debug class Timer {
    import std.datetime.stopwatch, core.thread;

    StopWatch sw;
    this() {
        sw = StopWatch(AutoStart.no);
    }
    void start() {
        sw.start();
    }
    void finish(bool noprint = false) {
        sw.stop();
        writeln(sw.peek.total!"usecs", " microseconds");
        sw.reset();
    }
}

/*******************************************************************************
 * This deprecated function arranges the associative array
 * by the required sorting method (string "by" is equal to "values" or "keys"),
 * returns an array of keys and an array of values.
 * Copied from amalthea 1.0.1.
 */
void orderAssociativeArray(alias by, V, K)
                          (in V[K] aarray,
                           out K[] orderedKeys,
                           out V[] orderedValues)
if (by == "values" || by == "keys") {
    if (aarray.length == 0) {
        return;
    }
    if (by == "keys") {
        orderedKeys = aarray.keys.dup;
        orderedKeys.sort;
        orderedValues.length = orderedKeys.length;
        foreach(i, k; orderedKeys) {
            orderedValues[i] = aarray[k];
        }
    } else {
        orderedValues = aarray.values.dup;
        orderedValues.sort;
        orderedKeys.length = orderedValues.length;
        size_t[] passedIndexes;
        foreach(k; aarray.keys) {
            foreach(i, v; orderedValues) {
                if (passedIndexes.canFind(i)) {
                    continue;
                }
                if (aarray[k] == v) {
                    orderedKeys[i] = k;
                    passedIndexes ~= i;
                    break;
                }
            }
        }
    }
}


/*******************************************************************************
 * Thid deprecated function arranges the associative array by keys,
 * returns an array of keys and an array of values.
 * Copied from amalthea 1.0.1.
 */
void orderAssociativeArrayByKeys(V, K)(in V[K] aarray,
                                       out K[] orderedKeys,
                                       out V[] orderedValues) {
    orderAssociativeArray!"keys"(aarray, orderedKeys, orderedValues);
}


/*******************************************************************************
 * This deprecated function arranges the associative array by values,
 * returns an array of keys and an array of values.
 * Copied from amalthea 1.0.1.
 */
void orderAssociativeArrayByValues(V, K)(in V[K] aarray,
                                         out K[] orderedKeys,
                                         out V[] orderedValues) {
    orderAssociativeArray!"values"(aarray, orderedKeys, orderedValues);
}
