/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import module_vitis_base,
       module_help;

public alias moduleCreate = module_create;

/*******************************************************************************
 * Primary function for creating categories
 */
void mainFn(string[] args) {
    string error = "Wrong using of command 'create'."._s;
    enforce(args.length > 1, new ValidException(error));
    if (args[1].among("--help", "-h")) {
        fnHelp("create");
        return;
    }
    if (args[1] == "-c") {
        enforce(args.length > 2, new ValidException(error));
        args = args[2 .. $];
    } else {
        args = args[1 .. $];
    }
    foreach(arg; args) {
        if (!arg.isOption) continue;
        throw new ValidException("Unknown option:"._s ~ " " ~ arg);
    }
    createCategories(args);
}


/*******************************************************************************
 * The function creates categories
 */
string[] createCategories(string[] categoriesForCreating,
                          Flag!"silentMode" silentMode = No.silentMode) {
    string[] categories;
    string msg;
    foreach(c; categoriesForCreating) {
        if (c.startsWith("__") || c.isAutocategoryName) {
            if (!silentMode) {
                msg = c ~ ": creation of this category is not available."._s;
                "warning".tprintln(msg, " ", "Skipped."._s);
            }
        } else if (!getCatDirPath(c).empty) {
            if (!silentMode) {
                msg = c ~ ": this category already exists."._s;
                "warning".tprintln(msg, " ", "Skipped."._s);
            }
        } else {
            categories ~= c;
        }
    }
    foreach(c; categories) {
        mkdirRecurse(getVitisPath ~ c);
    }
    return categories;
}
