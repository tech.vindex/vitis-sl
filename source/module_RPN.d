/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018, 2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module module_RPN; //reverse polish notice
public alias moduleRPN = module_RPN;

import module_vitis_base;

import std.file : exists;
import amalthea.dataprocessing :
    mixAssociativeArrays, calcUnion, calcIntersection, calcComplement;

enum TypeOfExpressionElement {
    LBRACE, RBRACE,
    CATEGORY,
    UNION, INTERSECTION, COMPLEMENT
}
alias ET = TypeOfExpressionElement;
@property ET etype(string line) {
    if      (line == "{")                     return ET.LBRACE;
    else if (line == "}")                     return ET.RBRACE;
    else if (line.among("u:", "+", "∪", "⋃")) return ET.UNION;
    else if (line.among("i:", "∩", "⋂"))      return ET.INTERSECTION;
    else if (line.among("c:", "d:", "\\"))    return ET.COMPLEMENT;
    else                                      return ET.CATEGORY;
}
@property bool isMathSign(ET value) {
    return cast(bool) value.among(ET.UNION, ET.INTERSECTION, ET.COMPLEMENT);
}
@property bool isBrace(ET value) {
    return cast(bool) value.among(ET.LBRACE, ET.RBRACE);
}
@property int prior(string line) {
    if (line.etype.among(ET.LBRACE, ET.RBRACE))    return 1;
    if (line.etype == ET.INTERSECTION)             return 2;
    if (line.etype.among(ET.UNION, ET.COMPLEMENT)) return 3;
    return int.max;
}


/// The function counts the number of opening and closing brackets
void validateBrackets(in string[] expression,
                      in string lbracket="{",
                      in string rbracket="}") {
    size_t lbrackets, rbrackets;
    foreach (element; expression) {
        if (element == lbracket) ++lbrackets;
        if (element == rbracket) ++rbrackets;
        enforce(rbrackets <= lbrackets,
                new ValidException("Incorrect use of brackets."._s));
    }
    enforce(lbrackets == rbrackets,
            new ValidException("Incorrect use of brackets."._s));
}


void validateExpression(in string[] expression) {
    enforce(!expression.empty,
            new ValidException("No expression."._s));

    validateBrackets(expression);

    ET[] elementTypeArray;
    elementTypeArray.length = expression.length;
    foreach(i, ref el; elementTypeArray) {
        el = expression[i].etype;
    }
    if (isMathSign(elementTypeArray[0])) {
        "error".tprint("Expression cannot begin with such sign."._s, "\n");
        throw new ValidException("Wrong expression."._s);
    }
    foreach(i, el; elementTypeArray) {
        bool elementIsLast = true;
        ET nextEl;
        if (elementTypeArray.length > i+1) {
            elementIsLast = false;
            nextEl = elementTypeArray[i+1];
        }
        if (el == ET.LBRACE || el.isMathSign) {
            if (elementIsLast) throw new ValidException("Wrong expression."._s);
            else if (nextEl != ET.LBRACE && nextEl != ET.CATEGORY) {
                    throw new ValidException("Wrong expression."._s);
            }
        }
        if (el.among(ET.RBRACE, ET.CATEGORY) && !elementIsLast
            && !isMathSign(nextEl) && nextEl != ET.RBRACE) {
            throw new ValidException("Wrong expression."._s);
        }
    }
}


static string[] toReversePolishNotation(string[] infixNotation) {
    string[] operationStack;
    string[] exitArray;
    foreach(el; infixNotation) {

        if (el.etype == ET.CATEGORY) {
            exitArray ~= el;
        } else if (el.etype.isMathSign || el.etype.isBrace) {
            if (operationStack.empty) {
                operationStack ~= el;
            } else {
                if ((operationStack[$-1].prior <= el.prior)
                    && !el.etype.isBrace
                    && !operationStack[$-1].etype.isBrace
                   ) {
                    exitArray ~= operationStack[$-1];
                    operationStack[$-1] = el;
                } else if (ET.RBRACE == el.etype) {
                    auto lastindex = cast(ssize_t)(operationStack.length) - 1;
                    for (ssize_t i = lastindex; i >= 0; i--) {
                        if (operationStack[i].etype != ET.LBRACE) {
                            exitArray ~= operationStack[i];
                            operationStack.length--;
                        } else {
                            operationStack.length--;
                            break;
                        }
                    }
                } else {
                    operationStack ~= el;
                }
            }
        }
    }
    for(int i = cast(int)operationStack.length-1; i >= 0; i--) {
        exitArray ~= operationStack[i];
    }
    operationStack.length = 0;
    return exitArray;
}


/*******************************************************************************
 * The function takes a mathematical expression with the operations of union,
 * intersection, subtraction in a special vitis-format,
 * returns an associative array with links (as values) and file paths (as keys)
 */
string[string] calculateExpression(in string[] expr) {
    validateExpression(expr);

    string[] expression = expr.dup;
    alias string[string] AssociativeArray;
    if (1 == expression.length) {
        expression = expression ~ ["u:"] ~ expression;
    }
    string[] RPN = toReversePolishNotation(expression);

    foreach(ref el; RPN) {
        if (el.etype == ET.CATEGORY) {
            string category = getCatDirPath(el);//getVitisPath ~ el;//;
            exists(category).enforce(
                new FileWarning(el ~ ": this category doesn't exist."._s)
            );
            el = category;
        }
    }

    string[][] arrayWithFileLists;
    ET[] arrayWithElementTypes;
    string[string] assArrayWithLinkLists;
    foreach(el; RPN) {
        arrayWithElementTypes ~= el.etype;
        if (ET.CATEGORY == el.etype) {
            auto symlinksInfo = getSymlinksInfo(el);
            assArrayWithLinkLists = mixAssociativeArrays(assArrayWithLinkLists,
                                                         symlinksInfo);
            arrayWithFileLists ~= symlinksInfo.values;
        } else {
            arrayWithFileLists ~= null;
        }
    }

    for(size_t i; i < arrayWithElementTypes.length; ++i) {
        auto el = arrayWithElementTypes[i];
        if (!el.isMathSign) continue;
        auto values1 = arrayWithFileLists[i-2];
        auto values2 = arrayWithFileLists[i-1];
        string[] unitedValues;
        if (ET.UNION == el) {
            unitedValues = calcUnion(values1, values2);
        } else if (ET.INTERSECTION == el) {
            unitedValues = calcIntersection(values1, values2);
        } else if (ET.COMPLEMENT == el) {
            unitedValues = calcComplement(values1, values2);
        }
        arrayWithFileLists = [[""]] ~ [[""]]
                           ~ arrayWithFileLists[0 .. i-2]
                           ~ unitedValues
                           ~ arrayWithFileLists[i+1 .. $];
    }

    for(size_t i; i < arrayWithFileLists.length; ++i) {
        if (arrayWithFileLists[i] == [""]) {
            arrayWithFileLists = arrayWithFileLists[0 .. i]
                               ~ arrayWithFileLists[i+1 .. $];
            i--;
        }
    }
    assert(1 == arrayWithFileLists.length);

    string[string] resultLinks;
    foreach(f; arrayWithFileLists[0]) {
        foreach(k, v; assArrayWithLinkLists) {
            if (f.dup == v.dup) {
                if (k !in resultLinks) resultLinks[v] = k;
            }
        }
    }

    return resultLinks;
}
