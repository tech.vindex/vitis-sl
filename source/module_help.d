/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import module_vitis_base;
public alias moduleHelp = module_help;

import std.file : exists;

/*******************************************************************************
 * The function displays help
 */
void mainFn(string command = "") {
    string lang = VitisConf.getLanguage();
    if (lang == "auto")
        lang = amalthea.langlocal.getSystemLanguage();
    string pathToHelp = getShareDir ~ "/help/" ~ lang ~ "/vitis-sl/";
    if (!exists(pathToHelp)) {
        pathToHelp = getShareDir ~ "/help/en_US/vitis-sl/";
    }
    if (!exists(pathToHelp)) {
        stderr.writeln("Help file does not exist."._s);
        return;
    }
    if ("" == command) {
        writeln(readText(pathToHelp ~ "help_begin.txt"));
    } else {
        writeln(readText(pathToHelp ~ "help_" ~ command ~ ".txt"));
    }
}
alias fnHelp = mainFn;
