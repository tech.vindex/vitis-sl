/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import module_vitis_base;
import
    module_assign,
    module_copy,
    module_create,
    module_delete,
    module_help,
    module_service,
    module_show;

import std.getopt;

int main(string[] args) {
    if (args.length == 1) {
        stderr.writeln("Invalid command."._s);
        stderr.writeln("See: vitis-sl --help"._s);
        return 1;
    }
    if (args[1] == "--version" && args.length == 2) {
        writeln(import("version").strip);
        return 0;
    }

    string configFile = extractOptionValue(args, "--conf");
    if (!configFile.empty) try {
        VitisConf.setVitisConfFile(configFile);
    } catch (FileException e) {
        stderr.writeln(e.msg);
        return 1;
    }
    string language = VitisConf.getLanguage();
    if (language == "auto")
        language = amalthea.langlocal.getSystemLanguage();
    initLocalization(mixin(import("locales.dtxt")), language);

    string currentApp = readLink("/proc/self/exe");
    string shareDir = buildNormalizedPath(dirName(currentApp), "..", "share");
    moduleBase.setShareDir(shareDir);

    try {
        executor(args[1 .. $]);
    } catch (ValidException ve) {
        printErrorMessage(std.string.strip(ve.msg));
        stderr.writeln("See: vitis-sl --help"._s);
        return 1;
    } catch (FileWarning fw) {
        printErrorMessage(fw.msg);
        return 2;
    } catch (FileSpaceException e) {
        printErrorMessage(e.msg);
        return 3;
    } catch (GetOptException e) {
        auto msg = e.msg.replace("Unrecognized option",
                                 "Unknown option:"._s);
        printErrorMessage(msg);
        stderr.writeln("See: vitis-sl --help"._s);
        return 4;
    } catch (AmaltheaNetException e) {
        printErrorMessage(e.msg._s);
        return 5;
    } catch (AppNotFound e) {
        printErrorMessage(e.msg);
        return 6;
    } catch (Exception e) {
        writeln(e);
        return 9;
    }
    return 0;
}


/*******************************************************************************
 * The function implements the launch of the requested command
 * from the command-line argument
 */
void executor(string[] args) {
    enforce(args.length > 0, new ValidException("No arguments."._s));
    if (args[0].among("--help", "-h")) args[0] = "help";
    CE command = cast(CE)args[0];
    switch(command) {
        case CE.e_show:    moduleShow.mainFn(args);    break;
        case CE.e_open:    goto case CE.e_show;        
        case CE.e_run:     goto case CE.e_show;        
        case CE.e_create:  moduleCreate.mainFn(args);  break;
        case CE.e_assign:  moduleAssign.mainFn(args);  break;
        case CE.e_delete:  moduleDelete.mainFn(args);  break;
        case CE.e_service: moduleService.mainFn(args); break;
        case CE.e_copy:    moduleCopy.mainFn(args);    break;
        case CE.e_help:    moduleHelp.mainFn();        break;
        default: throw new ValidException("Invalid command."._s);
    }
}
