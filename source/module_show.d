/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

module module_show;
public alias moduleShow = module_show;

import module_vitis_base,
       module_help,
       module_RPN;

import std.datetime;

import std.algorithm.mutation : stripRight;
import std.file : exists, isDir, isSymlink;

struct MainOptions {
    string numbers;
    SortEnum sortEnum;
    bool reverse;
    bool mix;
    bool hidden;
}
MainOptions extractMainOptions(ref string[] args) {
    MainOptions mainOptions;
    mainOptions.numbers = args.extractOptionValue("-n", "--numbers");
    mainOptions.sortEnum = SortEnum.name;
    getopt(args, config.passThrough, "sort",    &mainOptions.sortEnum,
                                     "reverse", &mainOptions.reverse,
                                     "mix",     &mainOptions.mix,
                                     "hidden",  &mainOptions.hidden);
    return mainOptions;
}


struct ShowingOptions {
    bool categories;
    bool details;
    bool paths;
    bool ipaths;
    bool fragmentInfo;
    bool noColors;
    bool noNumbers;
    bool machineOutput;
}
ShowingOptions extractShowingOptions(ref string[] args) {
    ShowingOptions options;
    if (!stdout.isTTY) options.noColors = true;
    getopt(args, config.passThrough, "categories",    &options.categories,
                                     "details",       &options.details,
                                     "paths",         &options.paths,
                                     "ipaths",        &options.ipaths,
                                     "fragment-info", &options.fragmentInfo,
                                     "no-colors",     &options.noColors,
                                     "no-numbers",    &options.noNumbers,
                                     "m|machine",     &options.machineOutput);
    if (options.machineOutput) {
        options.noNumbers = true;
        options.noColors = true;
    }
    return options;
}


struct OpeningOptions {
    string app;
    bool savedPage;
}
OpeningOptions extractOpeningOptions(ref string[] args) {
    OpeningOptions options;
    getopt(args, config.passThrough, "app",        &options.app,
                                     "saved-page", &options.savedPage);
    return options;
}


struct RunningOptions {
    bool sudo;
    string user;
}
RunningOptions extractRunningOptions(ref string[] args) {
    RunningOptions options;
    getopt(args, config.passThrough, "sudo", &options.sudo,
                                     "user", &options.user);
    return options;
}


/*******************************************************************************
 * The function manages the commands "show", "open", "run"
 */
void mainFn(string[] args) {
    CE command = cast(CE)args[0];
    string err = s_("Wrong using of command '" ~ cast(string)command ~ "'.");
    checkCondition!err(args.length >= 2);
    if (args[1].among("--help", "-h")) {
        fnHelp(cast(string)command);
        return;
    }

    bool modeCategoriesList;
    bool help;
    getopt(
        args, config.passThrough,
        "all-categories", &modeCategoriesList,
        "h|help", &help
    );
    checkCondition!err(help == false);  // here '--help' is wrong option
    if (command == CE.e_show && modeCategoriesList) {
        bool showAutoCategories;
        getopt(args, config.passThrough, "auto", &showAutoCategories);
        bool hidden;
        getopt(args, config.passThrough, "hidden", &hidden);
        checkCondition!err(args.length == 1);
        showAllCategories(hidden, showAutoCategories);
        return;
    }

    if (!args[1].isOption) {
        args = args[0] ~ ["-e"] ~ args[1 .. $];
    }
    bool showingMode, openingMode, runningMode;

    MainOptions mainOptions = extractMainOptions(args);
    ShowingOptions showingOptions;
    OpeningOptions openingOptions;
    RunningOptions runningOptions;
    if (command == CE.e_show) {
        showingOptions = extractShowingOptions(args);
        getopt(args, config.passThrough, "open", &openingMode,
                                         "run",  &runningMode);
        if (openingMode) openingOptions = extractOpeningOptions(args);
        if (runningMode) runningOptions = extractRunningOptions(args);
    } else if (command == CE.e_open) {
        openingOptions = extractOpeningOptions(args);
        getopt(args, config.passThrough, "show", &showingMode);
        if (showingMode) showingOptions = extractShowingOptions(args);
    } else if (command == CE.e_run) {
        runningOptions = extractRunningOptions(args);
        getopt(args, config.passThrough, "show", &showingMode);
        if (showingMode) showingOptions = extractShowingOptions(args);
    }

    string[] expression = args.extractOptionRange("-e");
    if (!expression.empty) {
        checkCondition!err(args.length == 1);
        if (command == CE.e_show) {
            showFilesByExpression(expression, mainOptions, showingOptions);
            if (openingMode) {
                openFilesByExpression(expression, mainOptions, openingOptions);
            } else if (runningMode) {
                runFilesByExpression(expression, mainOptions, runningOptions);
            }
        } else if (command == CE.e_open) {
            openFilesByExpression(expression, mainOptions, openingOptions);
            if (showingMode) {
                showFilesByExpression(expression, mainOptions, showingOptions);
            }
        } else if (command == CE.e_run) {
            runFilesByExpression(expression, mainOptions, runningOptions);
            if (showingMode) {
                showFilesByExpression(expression, mainOptions, showingOptions);
            }
        }
        return;
    }

    string vitisFile = args.extractOptionValue("-v");
    if (!vitisFile.empty) {
        checkCondition!err(dirName(vitisFile) != ".");
        size_t number;
        string strNumber = args.extractOptionValue("--number");
        number = strNumber.empty ? 0 : strNumber.to!size_t;
        checkCondition!err(args.length == 1);
        if (command == CE.e_show) {
            showVitisFile(vitisFile, mainOptions, showingOptions, number);
        } else if (command == CE.e_open) {
            openVitisFile(vitisFile, mainOptions, openingOptions, number);
        } else if (command == CE.e_run) {
            runVitisFile(vitisFile, mainOptions, runningOptions, number);
        }
        return;
    }
    checkCondition!err(false);
}


void showAllCategories(bool flagHidden, bool flagAutocategories) {
    string[] allRootAutoCatPaths = [
        //getAutoCategoriesPath(),
        getRootNetworkBookmarksCategoryPath(),
        getRootFragPointersCategoryPath(),
        getRootFormatCategoryPath(),
        getRootTypeCategoryPath(),
        getRootExtensionCategoryPath()
    ];

    string[] dirList;
    string[] catList;
    string vtspath = getVitisPath();
    auto tempDirList = getDirListRecurse(vtspath).array;
    tempDirList = std.algorithm.sort(tempDirList).array;
    foreach(dir; tempDirList) {
        if (dir.canFind("__repeated_names")) continue;
        string catName = dir[vtspath.length .. $];
        if (catName.startsWith("__")) {
            if (!flagAutocategories) continue;
            if (catName.startsWith("__auto")) {
                if (catName == "__auto") continue;
                foreach(p; allRootAutoCatPaths) {
                    if (p == dir) continue;
                    if (dir.startsWith(p)) {
                        catName = dir[p.length .. $];
                        catList ~= catName;
                        dirList ~= dir;
                    }
                }
                continue;
            }
        }
        catList ~= catName;
        dirList ~= dir;
    }
    foreach(i; 0 .. catList.length) {
        string dir = dirList[i];
        string category = catList[i];
        string subcategory = category.baseName;
        if (!flagHidden && subcategory.startsWith(".")) continue;
        string outputLine = `"`~category~`"`;
        if (dir.isSymlink) {
            auto origPath = readLink(dir);
            auto l = getVitisPath().length;
            auto origCategory = origPath[l .. $].strip('/');
            outputLine ~= " -> " ~ `"`~origCategory~`"`;
            /*
            if (readLink(dir).baseName != category) {
                outputLine ~= " -> " ~ `"`~readLink(dir).baseName~`"`;
            }*/
        }
        "simple".tprintln(outputLine);
    }
}


void showFilesByExpression(string[] expression,
                           MainOptions mainOptions,
                           ShowingOptions opts) {
    string[string] files;
    files = getFileListByExpression(expression, Yes.fullLinkPath);
    showFilesByFileList(files, mainOptions, opts);
}


void showFilesByFileList(string[string] fileList,
                         MainOptions mainOptions,
                         ShowingOptions opts) {
    string[] categoriesOfFile;
    auto typeOfOutput = opts.noColors ? "simple" : "category_details";
    void subfnPrintCategories() {
        auto lastIndex = categoriesOfFile.length-1;
        foreach(i, c; categoriesOfFile) {
            typeOfOutput.tprint(`"`~c~`"`);
            if (lastIndex != i) {
                typeOfOutput.tprint(" ");
            } else {
                "simple".tprint("\n");
            }
        }
    }

    setColorStdout(!opts.noColors);
    setColorStderr(!opts.noColors);

    string[string] tempFiles = fileList.dup;
    string[string] files;
    foreach(f, l; tempFiles) {
        if (!mainOptions.hidden && l.baseName.startsWith(".")) continue;
        files[f] = l;
    }
    size_t[string] numbersOfRepeatedNames = groupByFilePathWithNumbers(files);
    string[] filepaths, linkpaths;
    sortBySortEnum(mainOptions.sortEnum, files, filepaths, linkpaths);
    string[] printableLinkPaths, printableFilePaths;
    ssize_t maxNumber;
    if (!mainOptions.numbers.empty) {
        printableLinkPaths = getRarefiedArrayByNumbers(linkpaths,
                                                       mainOptions.numbers);
        printableFilePaths = getRarefiedArrayByNumbers(filepaths,
                                                       mainOptions.numbers);
        foreach(i, l; printableLinkPaths) if ("" != l) maxNumber = i+1;
    } else {
        maxNumber = linkpaths.length;
        printableLinkPaths = linkpaths;
        printableFilePaths = filepaths;
    }

    for (ssize_t index; index < printableLinkPaths.length; index++) {
        ssize_t i;
        if (!mainOptions.reverse) {
            i = index;
        } else {
            i = printableLinkPaths.length - 1 - index;
        }
        string linkPath = printableLinkPaths[i];
        if (linkPath == "") continue;
        auto linkName = linkPath.baseName;
        string filePath = printableFilePaths[i];
        auto noNumbers = opts.noNumbers;
        auto machine = opts.machineOutput;

        if (opts.categories) {
            categoriesOfFile = getCategoriesOfFile(filePath);
        }

        auto numberOfRepeating = numbersOfRepeatedNames[linkPath];
        printLinkNameLine(linkName, i+1, maxNumber, noNumbers,
                          machine, numberOfRepeating);
        if (opts.paths)        printFilePath(filePath, machine);
        if (opts.ipaths)       printNetPath(filePath);
        if (opts.details)      printDetails(filePath, opts.noColors);
        if (opts.fragmentInfo) printFragmentInfo(filePath);
        if (opts.categories)   subfnPrintCategories();
    }
}


void openFilesByExpression(string[] expression,
                           MainOptions mainOptions,
                           OpeningOptions openingOptions) {
    string[string] files = getFileListByExpression(expression);
    if (files.length == 0) return;
    openFilesByFileList(files, mainOptions, openingOptions);
}
void openFilesByFileList(string[string] fileList,
                         MainOptions mainOptions,
                         OpeningOptions openingOptions) {
    if (openingOptions.app.empty) {
        openingOptions.app = VitisConf.getOpener();
    }
    string[string] tempFiles = fileList.dup;
    string[string] files;
    foreach(f, l; tempFiles) {
        if (!mainOptions.hidden && l.startsWith(".")) continue;
        files[f] = l;
    }
    string[] filepaths, linkpaths;
    sortBySortEnum(mainOptions.sortEnum, files, filepaths, linkpaths);
    string[] resultFilePaths;
    if (!mainOptions.numbers.empty) {
        resultFilePaths = getRarefiedArrayByNumbers(filepaths,
                                                    mainOptions.numbers);
    } else {
        resultFilePaths = filepaths;
    }
    if (mainOptions.reverse) {
        std.algorithm.reverse(resultFilePaths);
    }
    if (mainOptions.mix) {
        import std.random : randomShuffle;
        randomShuffle(resultFilePaths);
    }

    string[] command = [openingOptions.app];
    foreach(f; resultFilePaths) {
        if ("" == f) continue;
        if (f.isApplication) {
            auto msg = f ~ s_(": this file is an application. "
                    ~ "Perhaps it is not intended to be opened.");
            "warning".tprintln(msg);
        }
        if (f.endsWith(".desktop")) {
            if (!openingOptions.savedPage) {
                f = DesktopEntry.getURL(f);
            } else {
                auto page = dirName(f) ~ f.baseName.stripExtension ~ ".html";
                if (!page.exists) {
                    auto msg = page ~ ": saved page is not found."._s;
                    "warning".tprintln(msg, " ", "Skipped."._s);
                    continue;
                }
                f = page;
            }
        }
        command ~= f;
    }
    //execute(command);
    string strCmd = command[0];
    foreach(el; command[1 .. $]) {
        el = el.replace(`"`, `\"`);
        strCmd ~= " " ~ `"`~el~`"`;
    }
    c_system(strCmd);
}


void runFilesByExpression(string[] expression,
                          MainOptions mainOptions,
                          RunningOptions runningOptions) {
    string[string] files = getFileListByExpression(expression);
    runFilesByFileList(files, mainOptions, runningOptions);
}
void runFilesByFileList(string[string] fileList,
                        MainOptions mainOptions,
                        RunningOptions runningOptions) {
    executeShell("xhost si:localuser:root"); //important if Wayland
    bool sudo = runningOptions.sudo;
    string user = runningOptions.user;

    string[] precmd;
    if (sudo && !user.empty) {
        precmd = ["sudo", "user="~user];
    } else if (sudo) {
        precmd = ["sudo"];
    } else if (!user.empty) {
        precmd = ["su", user, "-c"];
    }

    string[string] tempFiles = fileList.dup;
    string[string] files;
    foreach(f, l; tempFiles) {
        if (!mainOptions.hidden && l.startsWith(".")) continue;
        files[f] = l;
    }
    string[] filepaths, linkpaths;
    sortBySortEnum(mainOptions.sortEnum, files, filepaths, linkpaths);
    string[] resultFilePaths;
    if (!mainOptions.numbers.empty) {
        resultFilePaths = getRarefiedArrayByNumbers(filepaths,
                                                    mainOptions.numbers);
    } else {
        resultFilePaths = filepaths;
    }
    if (mainOptions.reverse) {
        std.algorithm.reverse(resultFilePaths);
    }

    string[] command = precmd;
    foreach(app; resultFilePaths) {
        if (app.empty) continue;
        command ~= app;
        execute(command);
    }
}


void showVitisFile(string vitisFile,
                   MainOptions mainOptions,
                   ShowingOptions showingOptions,
                   size_t number) {
    auto res = findFileInVitis(vitisFile, number);
    string[string] files = [res[0]:res[1]];
    showFilesByFileList(files, mainOptions, showingOptions);
}


void openVitisFile(string vitisFile,
                   MainOptions mainOptions,
                   OpeningOptions openingOptions,
                   size_t number) {
    auto res = findFileInVitis(vitisFile, number);
    string[string] files = [res[0]:res[1]];
    openFilesByFileList(files, mainOptions, openingOptions);
}


void runVitisFile(string vitisFile,
                  MainOptions mainOptions,
                  RunningOptions runningOptions,
                  size_t number) {
    auto res = findFileInVitis(vitisFile, number);
    string[string] files = [res[0]:res[1]];
    runFilesByFileList(files, mainOptions, runningOptions);
}


private size_t calcNumberOfDigits(long x) {
    long p = 10;
    for (int i = 1; i < 19; ++i) {
        if (x < p) {
        	return i;
        }
        p *= 10;
    }
    return 19;
}


private void printLinkNameLine(string linkName,
                               ssize_t currNumber,
                               ssize_t maxNumber,
                               bool noNumbers,
                               bool machineOutput,
                               size_t numberOfRepeating) {
    if (machineOutput) {
        noNumbers = true;
        linkName = `"`~linkName~`"`;
    }
    auto numberOfDigits = calcNumberOfDigits(maxNumber);
    if (!noNumbers) {
        auto printableNumber = "[" ~ currNumber.to!string ~ "]";
        auto numberField = rightJustify(printableNumber, numberOfDigits+2);
        "simple".tprint(numberField, " ");
    }
    if (numberOfRepeating) {
        "simple".tprint(numberOfRepeating, "/");
    }
    "filename".tprintln(linkName);
}


private bool isApplication(string filepath) {
    string[] cmd = ["xdg-mime", "query", "filetype", filepath];
    auto res = execute(cmd);
    return res.output == "application/x-executable";
}


private void printFilePath(string filePath, bool machineOutput) {
    if (machineOutput) filePath = `"`~filePath~`"`;
    "filepath".tprintln(filePath);
}


private void printNetPath(string filepath) {
    if (filepath.endsWith(".desktop")) {
        "simple".tprint("URL: ");
        "netpath".tprintln(DesktopEntry.getURL(filepath));
    }
}


private void printDetails(string filepath, bool noColors) {
    FileStat fstat = amalthea.fs.FileStat(filepath);
    string lastModification = getTimeString(fstat.mtime);
    string lastAccess = getTimeString(fstat.atime);

    auto st = FileStat(filepath);
    string permissions = amalthea.fs.makeUnixFileModeLine(st);
    auto size = std.file.getSize(filepath);
    string normSize = makeHumanOrientedByteSize(size);
    string typeOfOutputForProperty, typeOfOutputForValue;
    if (noColors) {
        typeOfOutputForProperty = typeOfOutputForValue = "simple";
    } else {
        typeOfOutputForProperty = "details_property";
        typeOfOutputForValue = "details_value";
    }
    typeOfOutputForProperty.tprint("Modified: "._s);
    typeOfOutputForValue.tprint(lastModification, "\t");
    typeOfOutputForProperty.tprint("Permissions: "._s);
    typeOfOutputForValue.tprintln(permissions[1 .. $]);
    typeOfOutputForProperty.tprint("Accessed: "._s);
    typeOfOutputForValue.tprint(lastAccess, "\t");
    typeOfOutputForProperty.tprint("Size: "._s);
    typeOfOutputForValue.tprintln(normSize," (",size," ","B"._s,")");
}


private void printFragmentInfo(string filepath) {
    if (!filepath.endsWith(".fragpointer")) return;
    string typeOfOutputForProperty = "details_property";
    string typeOfOutputForValue = "details_value";
    auto info = getFragmentData(filepath);
    if (info.start.empty) info.start = "-";
    if (info.finish.empty) info.finish = "-";
    typeOfOutputForProperty.tprint("Type:     "._s);
    typeOfOutputForValue.tprintln(info.type);
    typeOfOutputForProperty.tprint("Start:    "._s);
    typeOfOutputForValue.tprintln(info.start);
    typeOfOutputForProperty.tprint("Finish:   "._s);
    typeOfOutputForValue.tprintln(info.finish);
    typeOfOutputForProperty.tprint("Original: "._s);
    typeOfOutputForValue.tprintln(info.original);
}


private auto getFragmentData(string filepath) {
    string content = readText(filepath);
    string[] lines = content.split("\n").array;
    string mediaType, original, start, finish;
    mediaType = lines[0].split("Type: ")[1].strip(' ');
    original  = lines[1].split("Path: ")[1].strip(' ');
    start     = lines[2].split("Start: ")[1].strip(' ');
    finish    = lines[3].split("Finish: ")[1].strip(' ');
    Tuple!(string, "type",  string, "original",
           string, "start", string, "finish") dataSet;
    dataSet = tuple(mediaType, original, start, finish);
    return dataSet;
}


string makeHumanOrientedByteSize(ulong bytes) {
    long getExponent(ulong x) {
        if (x == 0) return 0;
        long exp = -1;
        while(x != 0) { x = x >> 10; ++exp; }
        return exp;
    }
    real size;
    string unit;
    auto exponent = getExponent(bytes);
    if (exponent > 5) exponent = 5;
    switch(exponent) {
        case 0: unit = "B"._s;   break;
        case 1: unit = "KiB"._s; break;
        case 2: unit = "MiB"._s; break;
        case 3: unit = "GiB"._s; break;
        case 4: unit = "TiB"._s; break;
        case 5: unit = "PiB"._s; break;
        default: break;
    }
    size = to!real(bytes)/(1024^^exponent);

    string strSize;

    if (exponent == 0) {
        strSize = to!string(size);
    } else {
        auto writer = appender!string();
        writer.formattedWrite("%.2f", size);
        strSize = writer.data;
    }

    return strSize ~ " " ~ unit;
}
unittest {
    assert(makeHumanOrientedByteSize(0) == "0 B");
    assert(makeHumanOrientedByteSize(1023) == "1023 B");
    assert(makeHumanOrientedByteSize(1024) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1025) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1024*1024*512) == "512.00 MiB");
    assert(makeHumanOrientedByteSize(1024L^^3*5/4) == "1.25 GiB");
    assert(makeHumanOrientedByteSize(1024L^^3*7/4) == "1.75 GiB");
}


private string[] getCategoriesOfFile(string file) {
    string[] linkedCategories;
    string[] autoCategories;

    if (!file.exists) {
        "simple".tprint(file, ": file not found."._s, "\n");
        return linkedCategories;
    }

    if (file.isSymlink) file = file.readLink;
    else if (!file.startsWith('/')) file = pwd ~ "/" ~ file;

    string[string] allLinks = getAllLinksFromCache();

    foreach(l, lf; allLinks) {
        if (lf != file) continue;
        string cat = l.dirName[getVitisPath.length .. $];
        if (cat.startsWith("__auto")) {
            // "__auto/N/NetworkBookmarsk" -> "NetworkBookmarks"
            import amalthea.dataprocessing : getIndex;
            auto indexOf1stSlash = getIndex(cat, '/');
            cat = cat[indexOf1stSlash+1 .. $];
            indexOf1stSlash = getIndex(cat, '/');
            cat = cat[indexOf1stSlash+1 .. $];
            if (cat.canFind("/__repeated_names/")) {
                cat = cat.split("/__repeated_names/")[0];
            }
            autoCategories ~= cat;
            continue;
        }
        if (cat.canFind("/__repeated_names/")) {
            cat = cat.split("/__repeated_names/")[0];
        }
        linkedCategories ~= cat;
    }
    linkedCategories = std.algorithm.sort(linkedCategories).array;
    autoCategories = std.algorithm.sort(autoCategories).array;
    return linkedCategories ~ autoCategories;
}


enum SortEnum {
    extension, name, none, size, time, atime
}


void sortBySortEnum(SortEnum sortEnum,
                    in  string[string] files,
                    out string[] filepaths,
                    out string[] linknames) {
    if (sortEnum != SortEnum.none) {
        //values of "files" are link names, keys are file paths
        orderAssociativeArrayByValues(files, filepaths, linknames);
    }
    final switch(sortEnum) {
        case SortEnum.extension:
            alias getExt = std.path.extension;
            filepaths.sort!((a,b) => getExt(a) < getExt(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.name:
            filepaths.sort!((a,b) => files[a].baseName < files[b].baseName);
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.none: break;
        case SortEnum.size:
            filepaths.sort!((a,b) => std.file.getSize(a) < std.file.getSize(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.time:
            filepaths.sort!((a, b) => lastModified(a) < lastModified(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.atime:
            filepaths.sort!((a, b) => lastAccessed(a) < lastAccessed(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
    }
}


SysTime lastAccessed(string name) {
    SysTime accessTime, modTime;
    std.file.getTimes(name, accessTime, modTime);
    return accessTime;
}


SysTime lastModified(string name) {
    SysTime accessTime, modTime;
    std.file.getTimes(name, accessTime, modTime);
    return modTime;
}

