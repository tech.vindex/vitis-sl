/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module module_conf;
public alias moduleConf = module_conf;

import amalthea.dataprocessing, amalthea.langlocal;
import module_vitis_base: ValidException;

import std.algorithm, std.array, std.process, std.string;

import std.algorithm.mutation : stripRight;
import std.file : exists, isDir, isSymlink;

/*******************************************************************************
 * The class for configuration file management
 */
final class VitisConf {
    static string vitisConfFile;

    static this() {
        auto confDir = environment["HOME"] ~ "/.config/vitis-sl/";
        mkdirRecurse(confDir);
        vitisConfFile = confDir ~ "vitis-sl.conf";
    }

    static void setVitisConfFile(string filepath) {
        vitisConfFile = filepath;
    }

    static string getConfPath() {
        string path = vitisConfFile;
        if (!exists(path)) {
            createConfFile(path);
        }
        return path;
    }

    deprecated alias fileConfPath = getConfPath;

    static void createConfFile(string path) {
        string vtsDir = environment["HOME"] ~ "/Vitis";
        auto initialTextOfConfPath
            = "[Main settings]\n"
            ~ "path=" ~ vtsDir ~ "\n"
            ~ "language=auto\n"
            ~ "filespaces=\n"
            ~ "autosave=no\n"
            ~ "opener=ufo\n"
            ~ "casesensitivity=no\n"
            ~ "autocategorization=format;extension\n";
        std.file.write(path, initialTextOfConfPath);
    }

    static void setDefault() {
        createConfFile(vitisConfFile);
    }

    private static string ConfGetter(string parameter) {
        parameter ~= "=";
        string confText = readText(getConfPath);
        auto index1 = confText.indexOf(parameter) + parameter.length;
        auto index2 = index1 + confText[index1 .. $].indexOf("\n");
        return confText[index1 .. index2].dup;
    }

    static void ConfSetter(string parameter, string value) {
        string confText = readText(getConfPath);
        confText = confText.replace(parameter ~ "=" ~ ConfGetter(parameter),
                                    parameter ~ "=" ~ value);
        std.file.write(getConfPath, confText);
    }

    static string getPath()     { return ConfGetter("path").stripRight('/'); }
    static string getLanguage() { return ConfGetter("language"); }
    static string getAutosave() { return ConfGetter("autosave"); }
    static string getOpener()   { return ConfGetter("opener"); }
    static string getCS()       { return ConfGetter("casesensitivity"); }
    static string getAC()       { return ConfGetter("autocategorization"); }

    static void setPath    (string path)  { ConfSetter("path",     path); }
    static void setLanguage(string lang)  { ConfSetter("language", lang); }
    static void setOpener  (string opener){ ConfSetter("opener", opener); }


    static void setAutosave(string autosave) {
        enforce(
            autosave.among("yes", "no"),
            new ValidException(
                "Invalid value for configuration parameter."._s)
        );
        ConfSetter("autosave", autosave);
    }

    static void setCaseSensitivity(string casesensitivity) {
        enforce(
            casesensitivity.among("yes", "no"),
            new ValidException(
                "Invalid value for configuration parameter."._s)
        );
        ConfSetter("casesensitivity", casesensitivity);
    }

    static void setAutocategorization(string ac) {
        enforce(
            ac.among("format", "none"),
            new ValidException(
                "Invalid value for configuration parameter."._s)
        );
        ConfSetter("autocategorization", ac);
    }

    alias getCaseSensitivity = getCS;
    alias setCS = setCaseSensitivity;

    alias getAutocategorization = getAC;
    alias setAC = setAutocategorization;

    static string[] getFileSpaces() {
        return ConfGetter("filespaces").split(";");
    }

    static void resetFileSpaces() {
        ConfSetter("filespaces", "");
    }

    static void addFileSpace(string filespace) {
        filespace = filespace.stripRight('/');
        string currSpaces = getFileSpaces().join(";");
        if (!currSpaces.empty) currSpaces = currSpaces ~ ";" ~ filespace;
        else currSpaces = filespace;
        ConfSetter("filespaces", currSpaces);
    }

    static void prioritizeFileSpace(string filespace) {
        filespace = filespace.stripRight('/');
        string confText = readText(getConfPath);
        if (!getFileSpaces.canFind(filespace)) {
            throw new ValidException("There is no file space."._s);
        }
        string[] currentFileSpaces = getFileSpaces();
        confText = confText.replace("filespaces=" ~ currentFileSpaces.join(";"),
                                    "filespaces=");
        currentFileSpaces = std.algorithm.remove(
            currentFileSpaces, currentFileSpaces.getIndex(filespace)
        );
        currentFileSpaces = filespace ~ currentFileSpaces;
        confText = confText.replace("filespaces=",
                                    "filespaces=" ~ currentFileSpaces.join(";"));
        std.file.write(getConfPath, confText);
    }

    static void disregardFileSpace(string filespace) {
        filespace = filespace.stripRight('/');
        string confText = readText(getConfPath);
        if (!getFileSpaces.canFind(filespace)) {
            throw new ValidException("There is no file space."._s);
        }
        string[] currentFileSpaces = getFileSpaces();
        confText = confText.replace("filespaces=" ~ currentFileSpaces.join(";"),
                                    "filespaces=");
        currentFileSpaces = std.algorithm.remove(
            currentFileSpaces, currentFileSpaces.getIndex(filespace)
        );
        confText = confText.replace("filespaces=",
                                    "filespaces=" ~ currentFileSpaces.join(";"));
        std.file.write(getConfPath, confText);
    }
}

