/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

module module_copy;

import module_vitis_base,
       module_help,
       module_RPN;
public alias moduleCopy = module_copy;

// import std.algorithm.mutation : stripRight;
import std.file : exists;

enum ConflictResolutionMode {
    interactive,
    replacement,
    skip
}

void mainFn(string[] args) {
    string err = "Wrong using of command 'copy'."._s;
    checkCondition!err(args.length >= 2);
    if (args[1].among("--help", "-h")) {
        checkCondition!err(args.length == 2);
        fnHelp("copy");
        return;
    }
    if (!args[1].among("-e", "-d")) {
        args = args[0] ~ ["-e"] ~ args[1 .. $];
    }

    string[] expression = args.extractOptionRange("-e");
    string directory = args.extractOptionValue("-d");

    bool replacementMode, skipMode;
    args.getopt(config.passThrough, "yes", &replacementMode);
    args.getopt(config.passThrough, "no", &skipMode);
    ConflictResolutionMode conflictResolution;
    if (skipMode) {
        conflictResolution = ConflictResolutionMode.skip;
    } else if (replacementMode) {
        conflictResolution = ConflictResolutionMode.replacement;
    } else {
        conflictResolution = ConflictResolutionMode.interactive;
    }

    checkCondition!err(args.length == 1);

    copyFilesByExpression(expression, directory, conflictResolution);
}


void copyFilesByExpression(string[] expression,
                           string directory,
                           ConflictResolutionMode conflictResolutionMode) {
    if (!directory.exists) mkdirRecurse(directory);
    string[] files = getFileListByExpression(expression).keys;
    foreach(filepath; files) {
        string filename = baseName(filepath);
        string destination = directory ~ "/" ~ filename;
        if (!exists(destination)) {
            cp(filepath, destination);
            continue;
        }
        final switch(conflictResolutionMode) {
        case(ConflictResolutionMode.interactive):
            "warning".tprintln(
                destination, ": file with this name already exists."._s,
            );
            string question;
            question = "Replace the file? y/n >: ";
            if (getAnswerToTheQuestion(question._s)) {
                rm(destination);
                cp(filepath, destination);
            }
            continue;
        case(ConflictResolutionMode.replacement):
            rm(destination);
            cp(filepath, destination);
            continue;
        case(ConflictResolutionMode.skip):
            continue;
        }

    }
}
