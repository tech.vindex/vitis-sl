/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021, 2025 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module module_service;
public alias moduleService = module_service;

import module_vitis_base;
import module_help;

import amalthea.fileformats;

import std.algorithm.mutation : stripRight;
import std.file : exists, isDir, isSymlink;

import amalthea.dataprocessing : mixAssociativeArrays;

static import std.net.curl;

/*******************************************************************************
 * Service management
 */
void mainFn(string[] args) {
    enforce(args.length > 1,
            new ValidException("Wrong using of command 'service'."._s));

    immutable subcommand = args[1];

    if (subcommand.among("-h", "--help")) {
        fnHelp("service");
        return;
    }

    alias Conf = VitisConf;
    switch(subcommand) {
    case "get":
        switch(args[2]) {
        case "path":               getVitisPath.writeln;            break;
        case "lang":               Conf.getLanguage.writeln;        break;
        case "filespaces":         Conf.getFileSpaces.each!writeln; break;
        case "autosave":           Conf.getAutosave.writeln;        break;
        case "opener":             Conf.getOpener.writeln;          break;
        case "casesensitivity":    Conf.getCaseSensitivity.writeln; break;
        case "autocategorization": Conf.getAC.writeln;              break;
        default:
            string err = "Wrong using of command 'service'."._s;
            if ("filespace" == args[2])
                err ~= "\n" ~ "Maybe did you mean 'filespaces'?"._s;
            throw new ValidException(err);
        }
        break;
    case "set":
        string value = args[3];
        switch(args[2]) {
        case "default":            Conf.setDefault();              break;
        case "path":               Conf.setPath(value);            break;
        case "lang":               Conf.setLanguage(value);        break;
        case "autosave":           Conf.setAutosave(value);        break;
        case "opener":             Conf.setOpener(value);          break;
        case "casesensitivity":    Conf.setCaseSensitivity(value); break;
        case "autocategorization": Conf.setAC(value);              break;
        default:
            throw new ValidException("Wrong using of command 'service'."._s);
        }
        break;
    case "reset":
        switch(args[2]) {
            case "filespaces": VitisConf.resetFileSpaces(); break;
            default:
                string err = "Wrong using of command 'service'."._s;
                if ("filespace" == args[2])
                    err ~= "\n" ~ "Maybe did you mean 'filespaces'?"._s;
                throw new ValidException(err);
        }
        break;
    case "add":
        switch(args[2]) {
            case "filespace": VitisConf.addFileSpace(args[3]); break;
            default:
                string err = "Wrong using of command 'service'."._s;
                if ("filespaces" == args[2])
                    err ~= "\n" ~ "Maybe did you mean 'filespace'?"._s;
                throw new ValidException(err);
        }
        break;
    case "prioritize":
        switch(args[2]) {
            case "filespace": VitisConf.prioritizeFileSpace(args[3]); break;
            default:
                string err = "Wrong using of command 'service'."._s;
                if ("filespaces" == args[2])
                    err ~= "\n" ~ "Maybe did you mean 'filespace'?"._s;
                throw new ValidException(err);
        }
        break;
    case "disregard":
        switch(args[2]) {
            case "filespace": VitisConf.disregardFileSpace(args[3]); break;
            default:
                string err = "Wrong using of command 'service'."._s;
                if ("filespaces" == args[2])
                    err ~= "\n" ~ "Maybe did you mean 'filespace'?"._s;
                throw new ValidException(err);
        }
        break;
    case "check":
        enforce(args.length > 2,
                new ValidException("Wrong using of command 'service'."._s));
        check(args[2 .. $]);
        break;
    default:
        throw new ValidException("Wrong using of command 'service'."._s);
    }
}


void check(string[] checkOptions) {
    auto object = checkOptions[0];
    bool flagFix, flagInteractive, flagFresh;
    string category;
    checkOptions.getopt("c",           &category,
                        "fix",         &flagFix,
                        "fresh",       &flagFresh,
                        "interactive", &flagInteractive);
    if (flagInteractive) flagFix = false;

    string[] categories;
    if (object == "network" && category == "") {
        categories ~= getVitisPath ~ "NetworkBookmarks";
    } else if (category != "") {
        string categoryPath = getCatDirPath(category);
        if (!(exists(categoryPath) && isDir(categoryPath))) {
            throw new FileWarning(category~": this category doesn't exist."._s);
        }
        categories ~= categoryPath;
    } else {
        categories = getTopLevelCategories();
        foreach(ref cat; categories) {
            cat = getVitisPath ~ cat;
        }
    }

    switch(object) {
    case "broken-links":
        checkBrokenLinks(categories, flagInteractive, flagFix);
        break;
    case "unnecessary-links":
        checkUnnecessaryLinks(categories, flagInteractive, flagFix);
        break;
    case "saved-pages":
        checkSavedPages(categories, flagInteractive, flagFix, flagFresh);
        break;
    case "fileformats":
        checkFileFormats(category, flagInteractive, flagFix);
        break;
    default:
        throw new ValidException("Wrong using of command 'service'."._s);
    }
}


void checkBrokenLinks(string[] dirs, bool flagInteractive, bool flagFix) {
    string[string] links;
    foreach(dir; dirs) { //filling of links
        string[string] tempLinks = getSymlinksInfo(dir, true, true);
        foreach(k, v; tempLinks) links[k] = v;
    }
    long counterOfBrokenLinks;
    foreach(link, filepath; links) {
        if (exists(filepath)) {
            if (std.file.getSize(filepath) > 0) {
                continue;
            }
            "simple".tprint(
                filepath, " : 0 " ~ "B"._s, "\n",
                "Be careful: the linked file has a size of zero."._s, "\n"
            );
        }
        //else
        ++counterOfBrokenLinks;
        "simple".tprint(
            getBorder,
            `"`~link~`"`, " is broken link."._s,
            " ", "The linked file does not exist."._s, "\n",
            "-> ", "This link points to "._s, `"`~filepath~`"`, "\n"
        );
        if (flagInteractive) {
            tprint("-> ", "Do you want to remove this link? y/n >: "._s);
            string ans = readln;
            ans = std.string.strip(ans).toLower;
            if (ans.among("y"._s, "yes"._s, "y", "yes")) {
                rm(link);
            } else {
                "simple".tprint("-> ", "Skipped."._s, "\n");
            }
        } else if (flagFix) {
            tprint("-> ", "This link is removing now."._s, "\n");
            rm(link);
        }
    }
    if (counterOfBrokenLinks == 0) return;
    "simple".tprint(
        "You can edit links using the 'link-editor' from the OS-18 Project."._s,
        "\n"
    );
}


void checkUnnecessaryLinks(string[] dirs, bool flagInteractive, bool flagFix) {
    foreach(dir; dirs) {
        string[string] links = getSymlinksInfo(dir, true, false, false);
        string[] subdirs = getDirList(dir, false, true);
        string[string] sublinks;
        foreach(subdir; subdirs) {
            auto tempSublinks = getSymlinksInfo(subdir, true, false, true);
            sublinks = mixAssociativeArrays(sublinks, tempSublinks);
        }
        foreach(link, filepath; links) foreach(sublink, subfilepath; sublinks) {
            if (filepath != subfilepath) continue;
            //tprint(getBorder);
            tprint(link);
            tprint(s_(
                ": this link is the same as the link in the subcategory."
            ));
            "simple".tprint("\n");
            tprint(link, " points to "._s, filepath, "\n");
            tprint(sublink, " also points to "._s, subfilepath, "\n");
            if (flagInteractive) {
                tprint(
                    "-> ",
                    "Do you want to remove unnecessary link? y/n >: "._s
                );
                bool yes = getAnswerToTheQuestion();
                if (yes) {
                    rm(link);
                } else {
                    "simple".tprint("-> ", "Skipped."._s, "\n");
                }
            } else if (flagFix) {
                tprint("-> ", "This link is removing now."._s, "\n");
                rm(link);
            }
        }
    }
}


void checkSavedPages(string[] dirs,
                     bool flagInteractive,
                     bool flagFix,
                     bool flagFresh) {
    foreach(dir; dirs) {
        string[] desktopFiles = getFileList(getHTMLLinkEntriesPath())
            .filter!(a => a.endsWith(".desktop"))
            .array;
        string URL, savedPagePath;
        foreach(desktopFile; desktopFiles) {
            URL = DesktopEntry.getURL(desktopFile);
            savedPagePath = desktopFile.stripExtension ~ ".html";

            if (exists(savedPagePath)) {
                if (flagFresh && !flagInteractive) {
                    saveHTMLPageByEntry(desktopFile);
                } else if (flagFresh && flagInteractive) {
                    string info = format("%s %s", savedPagePath.baseName,
                                 ": saved page is not found"._s);
                    tprintln(info);
                    FileStat st = FileStat(savedPagePath);
                    auto modTime = amalthea.sys.getTimeString(st.mtime);
                    tprintln("Time of saving the page: "._s, modTime);
                    auto q = "Do you want to update a local copy? y/n >: "._s;
                    if (getAnswerToTheQuestion(q)) {
                        saveHTMLPageByEntry(desktopFile);
                    } else {
                        "simple".tprint("-> ", "Skipped."._s, "\n");
                    }
                }
                return;
            }
            // else if (!exists(savedPagePath))
            tprint(getBorder);
            string info = format("%s %s", savedPagePath.baseName,
                                 ": saved page is not found"._s);
            tprintln(info);
            if (flagInteractive) {
                auto msg = "-> "~"Do you want to save a local copy? y/n >: "._s;
                flagFix = getAnswerToTheQuestion(msg);
            }
            if (flagFix) {
                saveHTMLPageByEntry(desktopFile);
            } else {
                "simple".tprint("-> ", "Skipped."._s, "\n");
            }
        }
    }
}





void checkFileFormats(string category = "",
                      bool flagInteractive = false,
                      bool flagFix = false) {

    auto automodes = VitisConf.getAutocategorization().split(';').array;
    if (!canFind(automodes, "format")) {
        tprintln("Automatic categorization by file format is disabled."._s);
        tprintln("Nothing to check."._s);
        return;
    }

    string[string] allLinks;
    if (category.empty) {
        allLinks = getAllNoAutoLinks();
    } else {
        string categoryPath = getCatDirPath(category);
        auto tempLinks = getSymlinksInfo(categoryPath);
        foreach(k, v; tempLinks) allLinks[k] = v;
    }
    string topFormatDir = getFormatCategoryPath();
    string topTypeDir = getTypeCategoryPath();

    FileFormat currFileFormat;

    void checkCorrectnessOfFormatAutocategory(string lp, //linkpath
                                              string fp, //filepath
                                              string autocategorization) {
        string catPath;
        string topCatPath;
        string category;
        if (autocategorization == "format") {
            category = formatCategory() ~ "/" ~ currFileFormat.format;
            topCatPath = topFormatDir;
            catPath = topFormatDir ~ currFileFormat.format ~ "/";
        } else if (autocategorization == "type") {
            if (currFileFormat.group.empty) {
                return;
            }
            category = typeCategory() ~ "/" ~ currFileFormat.group;
            topCatPath = topTypeDir;
            catPath = topTypeDir ~ currFileFormat.group ~ "/";
        }

        if (exists(catPath)) {
            foreach(lpath, fpath; getSymlinksInfo(catPath)) {
                if (fp == fpath) {
                    return;  // it's OK
                }
            }
        }

        string msg = "doesn't have correct format category."._s;
        msg = msg.replace("format", autocategorization);
        msg = format("%s %s", lp, msg._s);
        "warning".tprintln(msg);

        bool yes = false;
        if (flagInteractive && !flagFix) {
            yes = getAnswerToTheQuestion("Rectify the situation? y/n >: "._s);
        }
        if (!yes && !flagFix) {
            return;
        }

        if (!catPath.exists) {
            mkdirRecurse(catPath);
        }

        //we remove current link in old format (or type) categories
        foreach(lpath, fpath; getSymlinksInfo(topCatPath)) {
            if (fpath == fp) {
                amalthea.fs.rm(lpath);
            }
        }
        //we create new link in new category
        if (!exists(catPath ~ lp.baseName)) {
            symlink(fp, catPath ~ lp.baseName);
        }

        auto w = ": this file is assigned to new category"._s;
        msg = format("%s%s (%s).", lp, w, category);
        "warning".tprintln(msg);
    }

    foreach(linkpath, filepath; allLinks) {
        currFileFormat = amalthea.fileformats.getFileFormat(filepath);
        checkCorrectnessOfFormatAutocategory(linkpath, filepath, "format");
        checkCorrectnessOfFormatAutocategory(linkpath, filepath, "type");
    }
}



private void saveHTMLPageByEntry(string entryPath) {
    auto URL = DesktopEntry.getURL(entryPath);
    try {
        auto destDir = std.path.dirName(entryPath);
        auto htmlName = entryPath.baseName.stripExtension ~ ".html";
        savePage(URL, destDir); //make index.html
        rename(destDir~"/index.html", destDir~"/"~htmlName);
    }
    catch(std.net.curl.CurlException e) e.msg.writeln;
}
