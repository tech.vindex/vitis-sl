/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module module_output;
public alias moduleOutput = module_output;

import module_vitis_base;
import amalthea.terminal;

///whether to use color in the terminal
static colorStdout = true;
static colorStderr = true;
public bool getColorStdout() {
    if (!stdout.isTTY) colorStdout = false;
    return colorStdout;
}
public void setColorStdout(bool flag) { colorStdout = flag; }
public bool getColorStderr() {
    if (!stderr.isTTY) colorStderr = false;
    return colorStderr;
}
public void setColorStderr(bool flag) { colorStderr = flag; }
/*******************************************************************************
 * Main function for vitis output
 */
void terminalOutput(S...)(S args) {
    string message;

    if (args.length == 0) {
        return;
    } else if (args.length == 1) {
        std.stdio.write(args);
        return;
    }
    foreach(arg; args[1 .. $]) message ~= to!string(arg);
    const string msgType = to!string(args[0]);
    alias fg = FGColor;
    auto foreground = fg.empty;
    File outputFile = msgType.among("warning", "error") ? stderr : stdout;
    switch (msgType) {
        case "warning":  foreground = fg.yellow;     break;
        case "error":    foreground = fg.light_red;  break;
        case "category": foreground = fg.light_blue; break;
        case "slash":    foreground = fg.dark_gray;  break;
        case "filename": foreground = fg.light_cyan; break;
        case "filepath": foreground = fg.cyan;       break;
        case "netpath":  foreground = fg.magenta;    break;
        case "simple":   foreground = fg.empty;      break;
        case "category_details": foreground = fg.yellow;     break;
        case "details_property": foreground = fg.light_blue; break;
        case "details_value":    foreground = fg.green;      break;
        default: message = msgType ~ message; break;
    }

    if ((stdout == outputFile && getColorStdout)
     || (stderr == outputFile && getColorStderr)) {
        outputFile.cwrite(foreground, message);
    } else {
        outputFile.write(message);
    }
}
void terminalOutputLn(S...)(S args) {
    terminalOutput(args, "\n");
}
alias tprint = terminalOutput;
alias tprintln = terminalOutputLn;
