/* This file is part of vitis-sl.
 *
 * Copyright (C) 2018-2021, 2025 Eugene 'Vindex' Stulin
 *
 * vitis-sl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import
    module_vitis_base,
    module_help,
    module_RPN,
    module_create;

import amalthea.fileformats;
import std.getopt;

public alias moduleAssign = module_assign;

alias vpath = getVitisPath;

import std.algorithm.mutation : stripRight;
import std.file : exists, isDir, isSymlink;
static import std.net.curl;

/*******************************************************************************
 * Main function of the module
 */
void mainFn(string[] args) {
    auto err = "Wrong using of command 'assign'."._s;
    if (2 == args.length && args[1].among("--help", "-h")) {
        moduleHelp.mainFn("assign");
        return;
    }

    if (!args[1].isOption) {
        args = args[0] ~ ["-c"] ~ args[1 .. $];
    }

    string[] categories = args.extractOptionRange("-c");
    bool userConfirmation, userDenial;
    getopt(args, config.passThrough, "yes", &userConfirmation,
                                     "no",  &userDenial);
    if (userConfirmation && userDenial) { //incorrect situation
        userConfirmation = userDenial = false;
    }

    string newName = args.extractOptionValue("-n");

    //vitis-sl assign [-c] <category> -a <alias>
    string[] aliasNames = args.extractOptionRange("-a");
    if (!aliasNames.empty) {
        checkCondition!err(categories.length == 1);
        string category = categories[0];
        checkCondition!err(args.length == 1);
        assignAliases(category, aliasNames, userConfirmation, userDenial);
        return;
    }

    const bool confOptionSave = (VitisConf.getAutosave == "yes");
    string destFileSpace;
    bool cliOptionSave = false;
    getopt(args, config.passThrough, "save", &cliOptionSave,
                                     "to", &destFileSpace);
    if (!destFileSpace.empty) {
        cliOptionSave = true;
    }
    bool save = confOptionSave || cliOptionSave;

    //vitis-sl assign [[-c] <category>] -f <file list>
    string[] files = args.extractOptionRange("-f");
    if (!files.empty && newName.empty) {
        const autocategorization = VitisConf.getAutocategorization != "none";
        const positiveCondition1 = categories.empty && autocategorization;
        const positiveCondition2 = !categories.empty;
        checkCondition!err(positiveCondition1 || positiveCondition2);
        foreach(f; files) {
            if (std.file.exists(f)) checkCondition!err(!std.file.isDir(f));
        }
        string fragpointerName = args.extractOptionValue("--fragname");
        if (fragpointerName.empty) {
            checkCondition!err(args.length == 1);
            assignCategoriesToFiles(categories,
                                    files,
                                    userConfirmation,
                                    userDenial,
                                    save,
                                    destFileSpace);
        } else {
            string start = args.extractOptionValue("--start");
            string finish = args.extractOptionValue("--finish");
            checkCondition!err(files.length == 1);
            checkCondition!err(args.length == 1);
            assignCategoriesToFragment(categories,
                                       files[0],
                                       userConfirmation,
                                       userDenial,
                                       save,
                                       destFileSpace,
                                       start,
                                       finish,
                                       fragpointerName);
        }
        return;
    }

    //vitis-sl assign [[-c] <category>] -d <directory>
    string dir = args.extractOptionValue("-d");
    if (!dir.empty) {
        string category = args.extractOptionValue("--as");
        checkCondition!err(args.length == 1);
        checkCondition!err(std.file.isDir(dir));
        mirrorDirectoryToVitis(dir, category, categories, save, destFileSpace,
                               userConfirmation, userDenial);
        return;
    }

    //vitis-sl assign [-c] <category> -e <expression>
    string[] expression = args.extractOptionRange("-e");
    if (!expression.empty) {
        checkCondition!err(categories.length >= 1);
        checkCondition!err(args.length == 1);
        assignCategoriesByExpression(categories, expression,
                                     userConfirmation, userDenial);
        return;
    }

    string vitisFile = args.extractOptionValue("-v");
    if (!vitisFile.empty)
        checkCondition!err(!dirName(vitisFile).empty);
    const strNumber = args.extractOptionValue("--number");
    size_t number;
    try {
        number = strNumber.empty ? 0 : strNumber.to!size_t;
    } catch (ConvException e) {
        checkCondition!err(false);
    }

    //vitis-sl assign [-c] <category> -v <[category/]file>
    if (!vitisFile.empty && !categories.empty) {
        string fragpointerName = args.extractOptionValue("--fragname");
        string start, finish;
        if (!fragpointerName.empty) {
            start = args.extractOptionValue("--start");
            finish = args.extractOptionValue("--finish");
        }
        checkCondition!err(args.length == 1);
        //checkCondition!err(!save);
        assignCategoriesToVitisFile(categories,
                                    vitisFile,
                                    number,
                                    userConfirmation,
                                    userDenial,
                                    start,
                                    finish,
                                    fragpointerName);
        return;
    }

    //vitis-sl assign [-c] <category> -i <net-links>
    string[] networkLinks = args.extractOptionRange("-i");
    if (!networkLinks.empty) {
        checkCondition!err(args.length == 1);
        assignCategoriesToNetLinks(categories,
                                   networkLinks,
                                   cliOptionSave,
                                   userConfirmation,
                                   userDenial);
        return;
    }

    if (!newName.empty) {
        //vitis-sl assign [-c] <category> -n <new name>
        if (categories.length == 1) {
            checkCondition!err(args.length == 1);
            assignNewNameToCategory(categories[0], newName);
            return;
        }
        //vitis-sl assign -v <[category/]file> -n <new_name>

        checkCondition!err(!newName.canFind('/'));
        if (!vitisFile.empty) {
            checkCondition!err(args.length == 1);
            assignNewNameToFileFromVitis(vitisFile, newName, number);
            return;
        }
        //vitis-sl assign -f <file_path> -n <new_name>
        if (!files.empty) {
            checkCondition!err(args.length == 1);
            checkCondition!err(files.length == 1);
            assignNewNameByFile(files[0], newName);
            return;
        }
    }

    //vitis-sl assign [-c] <category> -s <subcategory>
    string subcategory = args.extractOptionValue("-s");
    if (!subcategory.empty) {
        checkCondition!err(categories.length == 1);
        bool flagGlobal = true;
        bool flagLocal;
        getopt(args, config.passThrough, "local",  &flagLocal,
                                         "global", &flagGlobal);
        if (flagLocal) flagGlobal = false;
        checkCondition!err(args.length == 1);
        makeSubcategories(categories[0],
                          [subcategory],
                          flagGlobal,
                          userConfirmation,
                          userDenial);
        return;
    }

    throw new ValidException("Wrong using of command 'assign'."._s);
}


/*******************************************************************************
 * Command to create aliases for required category.
 */
void assignAliases(string category,
                   string[] pseudonyms,
                   bool userConfirmation,
                   bool userDenial) {
    if (!createNonExistentCategory(category,userConfirmation,userDenial,true))
        return;
    foreach(pseudonym; pseudonyms) {
        if (pseudonym.isAutocategoryName) {
            "error".tprintln(pseudonym,
                ": creation of this alias is not available."._s,
                " ", "Skipped."._s);
            continue;
        }

        auto pseudonymPath = vpath ~ pseudonym;
        if (!std.file.exists(pseudonymPath)) {
            string catDir = std.algorithm.mutation.stripRight(
                category.getCatDirPath, '/'
            );
            symlink(catDir, vpath ~ pseudonym);
            continue;
        }
        "warning".tprintln(pseudonym ~ ": this category already exists."._s);
        if (std.file.isSymlink(pseudonymPath)) {
            "warning".tprintln(pseudonymPath, " -> ", pseudonymPath.readLink);
            "warning".tprintln("The request is skipped."._s);
            return;
        }
        bool answer = false;
        if (!userConfirmation && !userDenial) {
            auto q = "Do you want to combine the categories? y/n >: "._s;
            answer = getAnswerToTheQuestion(q);
        }
        if (!userDenial && (answer || userConfirmation)) {
            "simple".tprintln("The categories are combining."._s);
            combineCategories(category, pseudonym);
            string catDir = std.algorithm.mutation.stripRight(
                category.getCatDirPath, '/'
            );
            symlink(catDir, vpath ~ pseudonym);
        } else {
            "warning".tprintln("The request is skipped."._s);
        }
    }
}

/**
 */
void combineCategories(string mainCategory, string oldCategory) {
    string pathToOldCategory = getCatDirPath(oldCategory);
    string[] catDirList = getDirListRecurse(pathToOldCategory);
    catDirList = pathToOldCategory ~ catDirList;
    foreach(d; catDirList) {
        const currSubcategory = d[pathToOldCategory.length .. $];
        string oldSubcategory = oldCategory ~ "/" ~ currSubcategory;
        string newSubcategory = mainCategory ~ "/" ~ currSubcategory;
        createNonExistentCategory(newSubcategory, true, false);
        string pathToNewSubcategory;
        pathToNewSubcategory = newSubcategory.getCatDirPath.stripRight('/');
        if (oldSubcategory.isGlobalSubCategory) {
            auto globPath = getCatDirPath(oldSubcategory.baseName);
            globPath = globPath.stripRight('/');
            module_vitis_base.changeSymlink(globPath, pathToNewSubcategory);
        }
        string pathToOldSubcategory = getCatDirPath(oldSubcategory);
        string[] fileList = getFileList(pathToOldSubcategory);
        foreach(f; fileList) {
            auto newLink = pathToNewSubcategory ~ "/" ~ f.baseName;
            if (newLink.exists) {
                if (newLink.readLink == f.readLink) continue;
                writeDuplicateLink(f.readLink, pathToNewSubcategory);
            } else {
                symlink(f.readLink, newLink);
            }
        }
    }
    std.file.rmdirRecurse(pathToOldCategory);
}


/*******************************************************************************
 * The function assigns categories to files.
 */
void assignCategoriesToFiles(string[] categories,
                             string[] files,
                             bool userConfirmation,
                             bool userDenial,
                             bool flagSave = (VitisConf.getAutosave == "true"),
                             string destFileSpace = "") {
    string[] filesFromFS;
    foreach(f; files) {
        if (exists(f)) {
            filesFromFS ~= amalthea.fs.getAbsolutePath(f);
            continue;
        }
        "error".tprintln(f, ": file or directory not found."._s, " ",
                         "The request is skipped."._s);
    }
    if (filesFromFS.empty) {
        return;
    }

    if (flagSave) foreach(ref f; filesFromFS) {
        f = addFileInFileSpace(f, destFileSpace, userConfirmation, userDenial);
    }

    categories = createNonExistentCategories(categories,
                                             userConfirmation,
                                             userDenial);
    createNewSymlinks(categories, filesFromFS);
}


/*******************************************************************************
 * The function assigns categories to file fragment.
 */
void assignCategoriesToFragment(string[] categories,
                                string file,
                                bool userConfirmation,
                                bool userDenial,
                                bool flagSave,
                                string destFileSpace,
                                string start,
                                string finish,
                                string fragpointerName) {
    if (!file.exists) {
        throw new FileWarning(file ~ ": file or directory not found."._s);
    }
    auto f = amalthea.fs.getAbsolutePath(file);
    if (flagSave) {
        f = addFileInFileSpace(f, destFileSpace, userConfirmation, userDenial);
    }
    string fpPath = createFragPointer(f, start, finish, fragpointerName);
    categories = createNonExistentCategories(categories,
                                             userConfirmation,
                                             userDenial);
    categories ~= fragPointersCategory;
    createNewSymlinks(categories, [fpPath], No.useAuto);
}


/*******************************************************************************
 * The function assigns categories to directory.
 */
void mirrorDirectoryToVitis(string dir,
                            string category,
                            string[] categories,
                            bool save,
                            string fileSpace,
                            bool userConfirmation,
                            bool userDenial) {
    if (!exists(dir)) {
        auto msg = dir ~ ": file or directory not found."._s ~
                   " " ~ "The request is skipped."._s;
        throw new FileWarning(msg);
    }

    if (category.empty) category = dir.baseName;
    if (category.isAutocategoryName) {
        throw new Exception(
            category, ": creation of this category is not available."._s
        );
    }

    if (!dir.startsWith('/')) {
        dir = amalthea.fs.getAbsolutePath(dir);
    }

    if (save) {
        dir = addFileInFileSpace(dir, fileSpace, userConfirmation, userDenial);
    }

    categories = createNonExistentCategories(categories,
                                             userConfirmation,
                                             userDenial);

    void subfn(string nextDir, string nextCategory) {
        moduleCreate.createCategories([nextCategory]);
        auto fileList = getFileList(nextDir);
        auto catList = nextCategory ~ categories;
        assignCategoriesToFiles(catList, fileList, true, false, false);
        auto dirList = getDirList(nextDir);
        foreach(d; dirList) {
            subfn(d, nextCategory ~ "/" ~ d.baseName);
        }
    }
    subfn(dir, category);
}


/*******************************************************************************
 * The function assigns categories to files by expression.
 */
void assignCategoriesByExpression(string[] categories,
                                  string[] expression,
                                  bool userConfirmation,
                                  bool userDenial) {
    module_RPN.validateExpression(expression);
    string[string] files = module_RPN.calculateExpression(expression);
    categories = createNonExistentCategories(categories,
                                             userConfirmation,
                                             userDenial);
    createNewSymlinks(categories, files.keys); //files.keys are file paths
}


/*******************************************************************************
 * The function assigns categories to file from Vitis.
 */
void assignCategoriesToVitisFile(string[] categories,
                                 string vitisFile,
                                 size_t number,
                                 bool userConfirmation,
                                 bool userDenial,
                                 string start,
                                 string finish,
                                 string fragpointerName) {
    string filepath = moduleBase.findFileInVitis(vitisFile, number)[0];
    if (fragpointerName.empty) {
        assignCategoriesToFiles(categories,
                                [filepath],
                                userConfirmation,
                                userDenial,
                                false,
                                "");
    } else {
        assignCategoriesToFragment(categories,
                                   filepath,
                                   userConfirmation,
                                   userDenial,
                                   false,
                                   "",
                                   start,
                                   finish,
                                   fragpointerName);
    }

}


/*******************************************************************************
 * The function assigns categories to Internet links.
 */
void assignCategoriesToNetLinks(string[] categories,
                                string[] netlinks,
                                bool flagSave,
                                bool userConfirmation,
                                bool userDenial) {
    string[] networkLinks = netlinks.dup;

    categories = createNonExistentCategories(categories,
                                             userConfirmation,
                                             userDenial);
    categories ~= networkBookmarksCategory;

    foreach(url; networkLinks) {
        if (!url.canFind("://")) {
            url = "http://" ~ url;
        }

        string entryPath = saveLinkEntry(url);

        createNewSymlinks(categories, [entryPath], No.useAuto);

        if (flagSave) {
            string destDir = std.path.dirName(entryPath);
            try savePage(url, destDir, true);
            catch(std.net.curl.CurlException e) e.msg.writeln;
            catch(amalthea.net.AmaltheaNetException e) {
                "error".tprintln(url, ": ", "Saving page canceled"._s,
                                 " ", e.msg._s);
                continue;
            }
        }
    }
}


/*******************************************************************************
 * The function assigns new name for the category.
 */
void assignNewNameToCategory(string category, string newName) {
    category = category.stripRight('/');
    newName = newName.stripRight('/');
    string msg;
    if (getCatDirPath(category).empty) {
        msg = category ~ ": this category doesn't exist."._s;
    }
    if (!getCatDirPath(newName).empty) {
        msg = newName ~ ": this category already exists."._s;
    }
    if (category.isAutocategoryName) {
        msg = category ~ ": renaming this category is not available."._s;
    }
    if (newName.isAutocategoryName) {
        msg = newName ~ ": this renaming is not allowed."._s;
    }

    auto partsOfCategoryPath = newName.split('/').array;
    foreach(part; partsOfCategoryPath) {
        if (part.startsWith("__"))
            msg = newName ~ ": creation of this category is not available."._s;
    }

    if (!msg.empty) throw new FileWarning(msg);

    string categoryPath = getCatDirPath(category).stripRight('/');
    string superCategory = std.path.dirName(vpath ~ newName);
    if (!superCategory.exists) {
        std.file.mkdirRecurse(superCategory);
    }
    std.file.rename(categoryPath, vpath ~ newName);
    categoryPath = vpath ~ newName;

    if (newName.canFind('/')) {
        import module_service : checkUnnecessaryLinks;
        auto topCat = newName.split('/')[0];
        auto topDir = getCatDirPath(topCat);
        checkUnnecessaryLinks([topDir], false, true);
    }

    if (categoryPath.isSymlink) return;
    string[] allAliases = getAliases(Yes.fullPath);
    foreach(aliasPath; allAliases) {
        rm(aliasPath);
        symlink(categoryPath, aliasPath);
    }
}


/*******************************************************************************
 * The function assigns new name for the file.
 */
void assignNewNameByFile(string file, string newName) {
    if (!file.exists) {
        throw new FileWarning(file ~ ": file not found."._s);
    }
    if (!file.startsWith("/")) {
        file = amalthea.fs.getAbsolutePath(file);
    }
    string[string] allLinks = moduleBase.getAllLinks();
    size_t counter;
    foreach(l, f; allLinks) {
        if (file != f) continue;
        auto dir = dirName(l);
        if (dir.canFind("/__repeated_names/")) {
            auto index = dir.indexOf("/__repeated_names/");
            dir = dir[0 .. index];
        }
        auto newLink = dir.stripRight('/') ~ "/" ~ newName;
        if (newLink.exists) {
            if (newLink.readLink == f) continue;
            rm(l);
            writeDuplicateLink(f, dir, newName);
            /*
            "error".tprintln(newLink,
                             ": file with this name already exists."._s);
            "error".tprintln("The request is skipped."._s);
            */
        } else {
            rm(l);
            symlink(file, newLink);
        }
        ++counter;
    }
    if (0 == counter) {
        throw new FileWarning(file ~ ": file in Vitis is not found."._s);
    }
}


/*******************************************************************************
 * The function assigns new name for the file by Vitis-location and Vitis-name.
 */
void assignNewNameToFileFromVitis(string vitisFile,
                                  string newName,
                                  size_t number) {
    string filepath = moduleBase.findFileInVitis(vitisFile, number)[0];
    assignNewNameByFile(filepath, newName);
}


/*******************************************************************************
 * The function creates subcategories.
 */
void makeSubcategories(string category,
                       string[] subcategories_,
                       bool flagGlobal,
                       bool userConfirmation,
                       bool userDenial) {
    auto subcategories = subcategories_.dup;
    if (!createNonExistentCategory(category, userConfirmation, userDenial))
        return;
    foreach(subcategory; subcategories) {
        string finCat = category ~ "/" ~ subcategory;
        if ("" != getCatDirPath(finCat)) {
            auto msg = finCat ~ ": this subcategory already exists."._s
                     ~ " " ~ "The request is skipped."._s ~ "\n";
            "warning".tprint(msg);
            continue;
        }
        if (!flagGlobal) { //--local
            moduleCreate.createCategories([finCat], Yes.silentMode);
            continue;
        }
        //else --global
        string globalCat = subcategory;
        string globalCatPath = getCatDirPath(globalCat).stripRight('/');
        if (!globalCatPath.empty && !globalCatPath.isSymlink) {
            mv(globalCatPath, vpath ~ category ~ "/" ~ subcategory);
        } else {
            moduleCreate.createCategories([finCat], Yes.silentMode);
        }
        assignAliases(finCat, [subcategory], userConfirmation, userDenial);
    }
}


private void createNewSymlinks(string[] categories,
                               string[] filesFromFS,
                               Flag!"useAuto" useAuto = Yes.useAuto) {
    foreach(f; filesFromFS) {
        string[] categoriesPaths = getCategoriesPaths(categories);
        if (useAuto) categoriesPaths ~= defineAutoCategoriesPaths(f);
        foreach(i, p; categoriesPaths) {
            string link = p ~ f.baseName;
            if (!link.exists) {
                symlink(f, link);
            } else if (link.exists && link.isSymlink) {
                if (link.readLink == f) {
                    continue;
                } else { //repeated name
                    writeDuplicateLink(f, p);
                }
            } else if (link.exists) {
                "warning".tprintln(link, " ",
                    ": this file prevents the creation of a symbolic link."._s
                );
                "warning".tprintln("The request is skipped."._s);
            }

            //checking unnecessary links is not for autocategories
            if (i+1 > categories.length || categories.empty) continue;

            import module_service : checkUnnecessaryLinks;
            auto category = categories[i];
            auto topCat = category.split('/')[0];
            auto topDir = getCatDirPath(topCat).stripRight('/');
            while (topDir.isSymlink) {
                topCat = getCategoryByAlias(topCat).split('/')[0];
                topDir = getCatDirPath(topCat).stripRight('/');
            }
            checkUnnecessaryLinks([topDir], false, true);
        }
    }
}


private string[] getCategoriesPaths(string[] categories) {
    string[] categoriesPaths;
    foreach(category; categories) {
        categoriesPaths ~= getCatDirPath(category);
    }
    return categoriesPaths;
}


/**
 */
bool desktopFileIsLink(string filepath) {
    bool result;
    try {
        result = DesktopEntry.getField(filepath, "Type") == "Link";
    } catch(Exception e) {
        return false;
    }
    return result;
}


private string[] defineAutoCategoriesPaths(string filepath) {
    string[] paths;

    if (filepath.endsWith(".desktop") && desktopFileIsLink(filepath)) {
        paths ~= getNetworkBookmarksCategoryPath();
    } else if (filepath.endsWith(".fragpointer")) {
        paths ~= getFragPointersCategoryPath();
    }

    string[] modes = VitisConf.getAutocategorization().split(";").array;
    if (modes[0] == "none") {
        return paths;
    }

    if (modes.canFind("format")) {
        auto fileType = amalthea.fileformats.getFileFormat(filepath);
        paths ~= getFormatCategoryPath() ~ fileType.format ~ "/";
        if (!fileType.group.empty)
            paths ~= getTypeCategoryPath() ~ fileType.group ~ "/";
    }
    if (modes.canFind("extension")) {
        string ext = filepath.extension;
        if (!ext.empty && ext.length < 12) {
            ext = ext.toLower[1 .. $]; //without '.'
            paths ~= getExtensionCategoryPath() ~ ext ~ "/";
        }
    }

    foreach(p; paths) {
        mkdirRecurse(p);
    }
    return paths;
}


private string createFragPointer(string file,
                                 string start,
                                 string finish,
                                 string fragpointerName) {
    if (getAppPath("mediafragmenter") == "") {
        string appNotFound = "'mediafragmenter' not found."._s ~ "\n"
                           ~ s_("The 'mediafragmenter' package must be "
                           ~ "installed to work with fragments.");
        throw new AppNotFound(appNotFound);
    }
    string[] cmd = ["mediafragmenter", file];
    if (!start.empty) {
        cmd ~= ["-s", start];
    }
    if (!finish.empty) {
        cmd ~= ["-f", finish];
    }
    auto tmp = createTempFile();
    scope(exit) rm(tmp);
    cmd ~= ["-o", tmp];
    auto result = execute(cmd);
    enforce(result.status == 0, new ValidException(result.output));
    auto fpDir = getFragPointersPath();
    auto fpName = fragpointerName ~ ".fragpointer";
    auto fpPath = fpDir ~ fpName;
    if (fpPath.exists) {
        fpPath = writeDuplicateFile(tmp, fpDir, fpName);
    } else {
        cp(tmp, fpPath);
    }
    return fpPath;
}


private bool areFilesIdentical(string file1, string file2) {
    if (file1.getSize != file2.getSize) {
        return false;
    }
    auto f1 = File(file1, "r");
    auto f2 = File(file2, "r");
    byte[1024] buffer1;
    byte[1024] buffer2;
    byte[] slice1, slice2;
    do {
        slice1 = f1.rawRead(buffer1);
        slice2 = f2.rawRead(buffer2);
        if (slice1 != slice2) {
            return false;
        }
    } while(!slice1.empty);
    return true;
}


private string addFileInFileSpace(string f,
                                  string destFileSpace,
                                  bool userConfirmation,
                                  bool userDenial) {
    string[] allFileSpaces = VitisConf.getFileSpaces();
    if (allFileSpaces.empty) {
        throw new FileSpaceException("File spaces are not configured."._s);
    }

    if (destFileSpace == "") {
        destFileSpace = allFileSpaces[0];
    } else if (!canFind(allFileSpaces, destFileSpace)) {
        auto err = destFileSpace ~ ": " ~ "There is no file space."._s;
        throw new FileSpaceException(err);
    }

    if (f.startsWith(destFileSpace)) {
        return f; //save is not required
    }

    if (!destFileSpace.exists) {
        mkdirRecurse(destFileSpace);
    }

    string newLocation = destFileSpace.stripRight('/') ~ "/" ~ f.baseName;
    if (!newLocation.exists) {
        if (f.isDir) {
            cp(f, destFileSpace);
        } else {
            cp(f, newLocation);
        }
    } else if ((f.isFile && !newLocation.isFile)
             || (f.isDir && !newLocation.isDir)) {
        "warning".tprintln(
            newLocation, ": ",
            s_("Something is preventing the file from being added" ~
               " to the file space.")
        );
        "warning".tprintln("The request is skipped."._s);
    } else if (f.isFile && newLocation.isFile) {
        if (!areFilesIdentical(f, newLocation)) {
            return writeDuplicateFile(f, destFileSpace);
        }
        if (userDenial) {  // no rewrite (--no)
            return newLocation;
        }
        if (userConfirmation) {  // autoanswer with --yes
            return writeDuplicateFile(f, destFileSpace);
        }
        auto w = "Exactly the same file already exists in the file space."._s;
        "warning".tprintln(newLocation, ": ", w);

        auto question = "Do you want to write a duplicate file? y/n >: "._s;
        if (getAnswerToTheQuestion(question)) {
            return writeDuplicateFile(f, destFileSpace);
        }
        return newLocation;
    } else if (f.isDir && newLocation.isDir) {
        return writeDuplicateFile(f, destFileSpace);
    }
    return newLocation;
}


private string writeDuplicate(string file,
                              string dir,
                              string newFileName,
                              bool createLink) {
    if (newFileName.empty) {
        newFileName = baseName(file);
    }
    const repeatedBaseDir = dir.stripRight('/') ~ "/__repeated_names/";
    string fileLocation;
    for(size_t i = 2; ; i++) {
        string repDir = repeatedBaseDir ~ i.to!string ~ "/";
        fileLocation = repDir ~ newFileName;
        if (exists(fileLocation)) {
            if (fileLocation.isSymlink) {
                if (fileLocation.readLink == file)
                    return fileLocation;
            }
            continue;
        }
        mkdirRecurse(repDir);
        if (createLink) {
            symlink(file, fileLocation);
        } else {
            if (!file.isDir) {
                cp(file, fileLocation);
            } else {
                cp(file, repDir);
            }
        }
        break;
    }
    return fileLocation;
}
private string writeDuplicateFile(string file,
                                  string dir,
                                  string newFileName = "") {
    return writeDuplicate(file, dir, newFileName, false);
}
private string writeDuplicateLink(string file,
                                  string dir,
                                  string newFileName = "") {
    return writeDuplicate(file, dir, newFileName, true);
}




/*******************************************************************************
 * The function creates category with the default response
 * to create new category if not exists
 */
private bool createNonExistentCategory(string category,
                                       bool userConfirmation,
                                       bool userDenial,
                                       bool skipAutocategory = false) {
    if (!skipAutocategory && category.isAutocategoryName) {
        "error".tprintln(
            category, ": this category is not available for this use."._s,
            " ", "Skipped."._s
        );
        return false;
    }
    if (getCatDirPath(category) != "") {
        return true;
    }
    if (userConfirmation) {
        createCategories([category], Yes.silentMode);
        return true;
    }

    "warning".tprint(category, ": this category doesn't exist."._s, "\n");
    if (userDenial) return false;
    write("Do you want to create this category? y/n >: "._s);
    string answer = readln;
    answer = std.string.strip(answer).toLower;
    if (answer.among("y"._s, "yes"._s, "y", "yes", "")) {
        createCategories([category], Yes.silentMode);
        return true;
    } else {
        "warning".tprint("Action canceled."._s, "\n");
        return false;
    }
}


private string[] createNonExistentCategories(string[] categories,
                                             bool userConfirmation,
                                             bool userDenial) {
    string[] finalCategories;
    foreach(c; categories) {
        if (createNonExistentCategory(c, userConfirmation, userDenial)) {
            finalCategories ~= c;
        }
    }
    return finalCategories;
}


private string saveLinkEntry(string url) {
    const bool HTML = amalthea.net.isLinkToHTML(url);
    string linkEntriesDir = HTML ? getHTMLLinkEntriesPath()
                                 : getNonHTMLLinkEntriesPath();
    string tempEntryPath;  // path to desktop-file
    tempEntryPath = DesktopEntry.createNetworkLink(url, std.file.tempDir);
    scope(exit) rm(tempEntryPath);

    // comments in entry is not used now
    if (HTML) {
        DesktopEntry.setField(tempEntryPath, "Comment", "html");
    } else {
        DesktopEntry.setField(tempEntryPath, "Comment", "non-html");
    }

    string entryPath = linkEntriesDir ~ tempEntryPath.baseName;

    if (entryPath.exists) {
        entryPath = writeDuplicateFile(tempEntryPath, linkEntriesDir);
    } else {
        cp(tempEntryPath, linkEntriesDir);
    }
    return entryPath;
}

